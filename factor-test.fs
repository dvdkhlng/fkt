\ -*-forth-*-
\ Test integer factorization code
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./tester2.fs

T{ require ./factor.fs -> }T

T{ 12. (factor) -> 6. 2 }T
T{ 7. (factor) -> 7. 1 }T
T{ 19. (factor) -> 19. 1 }T
T{ 2309. (factor) -> 2309. 1 }T
T{ 10019686343. (factor) -> 333667. 30029 }T
T{ 826446446281. (factor) -> 909091. 909091 }T
T{ 303333666697. (factor) -> 909091. 333667 }T
 
0 VALUE factors{

T{ 7. factor TO factors{ -> 1 }T
T{ factors{ 2@ -> 7. }T
T{ 6. factor TO factors{ -> 2 }T
T{ factors{ 2@+ 2@ -> 2. 3. }T
T{ 12. factor TO factors{  -> 3 }T
T{ factors{ 2@+ 2@+ 2@ -> 2. 2. 3. }T
T{ 123456789. factor TO factors{  -> 4 }T
T{ factors{ 2@+ 2@+ 2@+ 2@ -> 3. 3. 3607. 3803. }T
T{ 877670618263531. factor TO factors{  -> 2 }T
T{ factors{  2@+ 2@ -> 7552031. 116216501. }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth-fast factor-test.fs -e bye"
   End:
[THEN]
