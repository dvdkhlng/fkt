\ -*-forth-*-
\ Load, configure and extend Gforth's ttester.fs
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Nov 2012

require test/ttester.fs
require string.fs
require ./misc.fs

variable #errors
variable #total

: }T  ( ... -- )  \ overload to count number of tests performed
   }T   1 #total +! ;
: .error  ( c-addr u -- )  \ print detailed error info
   CR sourcefilename TYPE ." :" sourceline# 0 .R ." :" TYPE ;
: test-error  ( c-addr u -- ) \ called on errors, message on stack
   ['] .error stderr outfile-execute
   1 #errors +! ;
: .===  ( -- )  CR 78 0 DO '= EMIT LOOP ;
: .summary  ( -- ) \ print test summmary
   CR ."  ("
   #total @ #errors @ - 0 .R  ." /" #total @ . ." test-expressions passed"
   #errors @ ?DUP-IF
      ." , " . ." tests FAILED!"
   THEN
   ." )" CR ;
: exit-test  ( -- )  \ quit gforth, exit-code indicating number of errors
   .summary 
   #errors @ IF ABORT ELSE BYE THEN ;

' test-error ERROR-XT !