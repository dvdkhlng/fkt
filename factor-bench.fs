\ -*-forth-*-
\ Benchmark integer factorization code
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require test/ttester.fs
T{ require ./factor.fs -> }T

0 VALUE factors{

CELL 8 = [IF]
   \ This code only works on 64-bit Forth systems.
   \ Takes 50s on a Athlon II, 1.8 GHz.
   T{ 8095082638060894293961419810869773. factor TO factors{ -> 3 }T
   T{ factors{  2@+ 2@+ 2@ -> 7552031. 116216501. 9223372036854775783. }T
[ELSE]
   \ On 32-bit forth systems, this benchmark has about the same complexity:
   T{ 9223372036854775783. factor TO factors{ -> 1 }T
   T{ factors{ 2@ -> 9223372036854775783. }T
[THEN]

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth-fast factor-bench.fs -e bye"
   End:
[THEN]
