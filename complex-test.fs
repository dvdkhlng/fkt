\ -*-forth-*-
\ Complex number modular arithmetic test case
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

3 assert-level !

require ./tester2.fs
T{ require ./complex.fs -> }T

2VARIABLE a

7 modulus !
T{ 1 3  6 4  z+  -> 0 0 }T
T{ 1 3  6 4  z-  -> 2 6 }T
T{ 1 3 a 2!  6 4 a z+!  a 2@ -> 0 0 }T
T{ 1 3 a 2!  6 4 a z-!  a 2@ -> 2 6 }T
T{ 5 0  1 0  z*  -> 5 0 }T
T{ 5 0  0 1  z*  -> 0 5 }T
T{ 0 2  0 2  z*  -> 4 rnegate 0 }T
T{ 1 0  zinv  -> 1 0 }T
T{ 0 1  zinv  -> 0 1 rnegate }T
T{ 3 5  zinv  3 5 z* -> 1 0 }T
T{ 2 2  znormsq -> 1 }T

2 modulus !
T{ 0 0  z0= -> true }T
T{ 0 0  z0<> -> false }T
T{ 1 0  z0= -> false }T
T{ 1 0  z0<> -> true }T
T{ 1 1  z0= -> true }T
T{ 1 1  z0<> -> false }T

T{ 1 0  zsqrt -> 1 0 }T
T{ 4 0  zsqrt -> 2 0 }T
T{ 81 0  zsqrt -> 9 0 }T
T{ 7 24  zsqrt -> 4 3 }T

max-modulus modulus !

T{ -1   0  zsqrt -> 0 1 }T
T{ 456 123 2DUP z* zsqrt -> 456 123 }T
T{ 123 456 2DUP z* z>signed zsqrt -> 123 456 }T


: mod-series  ( n1 n2 n -- )
   >R 2DUP R> 0 ?DO
      2DUP z.   2OVER z*  
   LOOP
   2DROP 2DROP ;
3 modulus !  1 2 16 mod-series
7 modulus !  1 2 49 mod-series
   
exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth complex-test.fs -e bye"
   End:
[THEN]
