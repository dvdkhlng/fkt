\ -*-forth-*-
\ Dark integers a.k.a. bignum arithmetic unit test.
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: May 2021

3 assert-level !

require ./tester2.fs
T{ require ./darkint.fs -> }T

T{ bn-depth -> 0 }T

T{ bn0 bn-depth bn-drop -> #darkint cell+ }T
T{ bn0 bn0= -> TRUE }T
T{ bn0 bn0  bn-drop bn-drop bn-depth -> 0 }T
T{ bn0 bn0  bn= bn-depth -> TRUE 0 }T

T{ 0 u>bn  bn0  bn=  bn-depth -> TRUE 0 }T
T{ 1234 u>bn  bn>u  bn-depth -> 1234 0 }T
T{ 0 u>bn  0 u>bn  bn=  bn-depth -> TRUE 0 }T
T{ 0 u>bn  1 u>bn  bn=  bn-depth -> FALSE 0 }T

T{ 5 u>bn  1 u>bn  bn+  bn-depth bn-drop -> #darkint cell+ }T
T{ 5 u>bn  1 u>bn  bn+  6 u>bn bn=  bn-depth -> TRUE 0 }T
T{ $123456789abcdef034567890abcdef12. ud>bn
$34567890abcdef12 $123456789abcdef0 2 k>bn  bn=  bn-depth  -> TRUE 0 }T

T{ $FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF
4 k>bn   1 u>bn bn+  
0 0 0 0 1 5 k>bn bn-.s bn=  bn-depth -> TRUE 0 }T

T{ $FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF
4 k>bn   1 bn+u  
0 0 0 0 1 5 k>bn bn-.s bn=  bn-depth -> TRUE 0 }T

CR .( bn-)
T{ $1234 $0 $0 $8000000000000000 $6789 5 k>bn
$1235 u>bn  bn-
$FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF $FFFFFFFFFFFFFFFF $7FFFFFFFFFFFFFFF $6789
5 k>bn bn-.s bn=  bn-depth -> TRUE 0 }T

T{ 1 2 3 4 4 k>bn   1 2 3 4 4 k>bn   bn-compare  bn-depth -> 0  0 }T
T{ 1 2 3 5 4 k>bn   1 2 3 4 4 k>bn   bn-compare  bn-depth -> 1  0 }T
T{ 5 2 3 4 4 k>bn   1 2 3 4 4 k>bn   bn-compare  bn-depth -> 4  0 }T
T{ 1 2 3 4 4 k>bn   2 2 5 4 4 k>bn   bn-compare  bn-depth -> -2 0 }T

T{ $f00 $a00 $000 $700 4 k>bn   $8000000000000000   bn*u
   $000 $780 $500 $000 $380 5 k>bn  bn-.s bn=  bn-depth  -> TRUE 0 }T

T{ $ffffffffffffffff $0a00 $000 $700 4 k>bn   bn2*
   $fffffffffffffffe $1401 $000 $e00 4 k>bn  bn-.s bn=  bn-depth  -> TRUE 0 }T

T{ 256 u>bn  .bn  bn-depth -> 0 }T

T{ 1 u>bn bn#msb bn-drop -> 0 }T
T{ bn0 bn#msb bn-drop -> 0 }T
T{ 1 u>bn bn-negate   bn#msb bn-drop -> #darkint 8 * 1- }T
T{ $f00 $a00 $000 $700 4 k>bn   bn#msb  bn-drop -> #bits/cell 3 * 10 + }T

T{ $ffffffffffffffff $0a00 $000 $700 4 k>bn  8 (bn-lshift)
   $ffffffffffffff00 $0a00ff $000 $70000 4 k>bn  bn-.s bn=  bn-depth  -> TRUE 0 }T

T{ $ffffffffffffffff $0a00 $000 $700 4 k>bn  0 (bn-lshift)
   $ffffffffffffffff $0a00 $000 $700 4 k>bn  bn-.s bn=  bn-depth  -> TRUE 0 }T

T{ $ffffffffffffffff $0a00 $000 $700 4 k>bn  0 bn-lshift
   $ffffffffffffffff $0a00 $000 $700 4 k>bn  bn-.s bn=  bn-depth  -> TRUE 0 }T

CR .( rshift)
T{ $ffffffffffffffff $0a59 $000 $700 4 k>bn  8 (bn-rshift)
   $59ffffffffffffff $000a $000 $007 4 k>bn  bn-.s bn=  bn-depth  -> TRUE 0 }T

CR .( lshift limbs )
T{ $ffffffffffffffff $0a00 $000 $700 4 k>bn  5 bn-lshift-limbs
   0 0 0 0 0 $ffffffffffffffff $0a00 $000 $700 9 k>bn  bn-.s bn=  bn-depth
   -> TRUE 0 }T

T{ bn0 bn-invert 5 bn-lshift-limbs
   bn0 bn-invert 5 CELLS 8 * 0 [DO] bn2* [LOOP]
   bn-.s bn=  bn-depth  -> TRUE 0
}T

CR .( rshift limbs )
T{ 0 0 0 0 0 $ffffffffffffffff $0a00 $000 $700 9 k>bn  5 bn-rshift-limbs
   $ffffffffffffffff $0a00 $000 $700  4 k>bn  
   bn-.s bn=  bn-depth  -> TRUE 0
}T

T{ bn0 bn-invert 5 bn-rshift-limbs
   bn0 bn-invert 5 CELLS 0 [DO] 8 (bn-rshift) [LOOP]  
   bn=  bn-depth  -> TRUE 0 }T

T{ bn0  100 bn-bitset   $0 $1000000000 ud>bn bn-.s  bn=   bn-depth -> TRUE 0 }T

T{  $1234 u>bn $1000 u>bn  (bn/mod)  bn>u  bn-depth  -> 0  $234  0 }T
T{  $1234 u>bn $100 u>bn  (bn/mod)  bn>u  bn-depth  -> 4  $234  0 }T
T{  $1234 u>bn $130 u>bn  (bn/mod)  bn>u  bn-depth  -> 3  $8B4  0 }T
T{  $1234 u>bn $130 u>bn  (bn/mod)  bn>u  bn-depth  -> 3  $8B4  0 }T
T{  $1234 u>bn $1235 u>bn  (bn/mod)  bn>u  bn-depth  -> -1  $1234  0 }T   
T{  $1234 u>bn $1234 u>bn  (bn/mod)  bn>u  bn-depth  -> 0  0  0 }T
T{  $1234 u>bn $1233 u>bn  (bn/mod)  bn>u  bn-depth  -> 0  1  0 }T

T{  $1234 u>bn $10 u>bn  bn/mod  bn>u bn>u SWAP  bn-depth  -> 4  $123  0 }T
T{  $1234 u>bn $1 u>bn  bn/mod  bn>u bn>u SWAP  bn-depth  -> 0  $1234  0 }T
T{  $1 u>bn $1 u>bn  bn/mod  bn>u bn>u SWAP  bn-depth  -> 0  $1  0 }T
T{  $1 u>bn 0 u>bn  ' bn/mod CATCH  bn-2drop bn-depth  -> -2  0 }T         
   
   
exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth darkint-test.fs -e bye"
   End:
[THEN]
