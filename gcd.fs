\ -*-forth-*-
\ Routines related to greatest common divisor and euclid's algorithm
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./misc.fs

\ Note: code below assumes that "*" is equivalent to "M* DROP" WRT overflow.

: gcd  ( n1 n2 -- n3 )  \ greatest common divisior, via euclid's algorithm
   \ n2 must be nonzero
   ABS SWAP ABS
   BEGIN
      ( 2DUP < IF SWAP THEN)
      TUCK MOD DUP 0=	\ if n2 divides n1, we're done, return n2
   UNTIL DROP ;		\ else continue with n2, n1-n*n2 (i.e. remainder)
: (dgcd)  ( d1 d2 -- d3 )
   \ compute binary GCD, if at least one of d1, d3 is odd
   BEGIN
      2OVER 2OVER D= IF 2DROP EXIT THEN
      2OVER 2OVER D< IF 2SWAP THEN
      2TUCK D-
      BEGIN OVER 1 AND 0= WHILE D2/ REPEAT
   AGAIN ;
: dgcd  ( d1 d2 -- d3 )
   \ greatest common divisor in double precision via binary GCD algorithm
   DABS 2SWAP DABS
   #bits/cell 2* 0 DO
      OVER 1 AND >R 2SWAP OVER 1 AND R> OR IF
	 (dgcd) I 0 ?DO D2* LOOP UNLOOP EXIT  \ <- could do this faster
      THEN
      D2/ 2SWAP D2/
   LOOP   
   2DROP ;		\ no one-bits found, just return zero

: bezout  ( n1 n2 -- n3 n4 )
   \ return n3,n4 such that gcd(n1,n2) = n1*n3+n2*n4
   \ This is based on pseudo-code from
   \ https://en.wikipedia.org/w/index.php?title=Extended_Euclidean_algorithm&oldid=471779453#Recursive_method_2
   ?DUP-IF
      TUCK /modf >R RECURSE
      TUCK R> * -
      EXIT
   THEN
   DROP 1 0 ;
: bezout-gcd  ( n1 n2 -- n3 n4 n5 )
   \ like bezout, but also return GCD on top.  
   2DUP bezout 2DUP 2>R
   ROT * -ROT *  +  2R> ROT ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth gcd-test.fs -e bye"
   End:
[THEN]
