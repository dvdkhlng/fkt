\ -*-forth-*-
\ det test
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./rdet.fs -> }T

CR
31 modulus !

3 3 INTEGER MMATRIX tt{{

3 3 tt{{ }}ifill:
2 0 0
0 3 0
0 0 5
T{ 3 tt{{ }}rdet -> 30 }T

3 3 tt{{ }}ifill:
2 0 0
0 0 5
0 3 0
T{ 3 tt{{ }}rdet rnegate -> 30 }T

2 2 tt{{ }}ifill:
   2   8  
   4   4  
T{ 2 tt{{ }}rdet rnegate -> 24 }T

2 2 tt{{ }}ifill:
   1   1  
   2   1  
T{ 2 tt{{ }}rdet rnegate -> 1 }T

2 2 tt{{ }}ifill:
   1   2  
   2   4  
T{ 2 tt{{ }}rdet -> 0 }T

127 modulus !
3 3 tt{{ }}ifill:
   2   8   0
   4   4   2
   1   4   5
T{ 3 tt{{ }}rdet rnegate -> 120 }T


exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth rdet-test.fs -e bye"
   End:
[THEN]
