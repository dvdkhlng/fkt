\ -*-forth-*-
\ Test fibonacci numbers, fibonacci encoding
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./tester2.fs
require ./misc.fs

T{ require ./fibonacci.fs -> }T

print-fibonacci{}

T{ fibonacci{ 5 } 2@ -> 5. }T

0 VALUE fc{

T{ 5. fibonacci-encode TO fc{ -> 5 }T
T{ fc{ C@+ C@+ C@+ C@+ C@  ->  0 0 0 1 1 }T
T{ 65. fibonacci-encode TO fc{ -> 10 }T
T{ fc{ C@+ C@+ C@+ C@+ C@+ C@+ C@+ C@+ C@+ C@ -> 0 1 0 0 1 0 0 0 1 1 }T

T{ fc{ fibonacci-decode -> 65. }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth fibonacci-test.fs -e bye"
   End:
[THEN]

