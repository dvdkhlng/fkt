\ -*-forth-*-
\ Matrix Pfaffian (operating on pseudo-complex galois field GF(p^2) )
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Nov 2012

require ./module.fs
require ./imatrix.fs
require ./complex.fs
require ./zemt.fs

module zpfaffian

<private>

: }}rc-exchange { n M{{ j k -- }  \ simultaneous row+column exchange
   n M{{ j k }}2swap-cols
   n M{{ j k }}swap-rows ;

: }}rc-scaleadd { d: a n M{{ j c k -- }
   \ Add multiple of partial row&column j to partial row&column k.
   \ Only touch elements c..c+n-1 on each row/column.
   a n M{{ j c k (}}zrowadd)
   a n M{{ c j k (}}zcoladd)
   ( Cr n n M{{ }}iprint) ;

<public>

: }}zpfaffian { n M{{ -- result }
   \ note: destroys the contents of M{{ during computation
   n 1 AND IF       \ abort early on odd dimension, code below can't cope
      0 0 EXIT      \ in that case pfaffian is 0 anyways
   THEN
   1 0 { D: result }
   n 1 ?DO		\ iterating over odd I: 1, 3,.., n-1
      \ search pivot (i.e. swap columns to ensure M(i-1,i)>0)
      n I ?DO
	 M{{ J 1- I }} 2@ z0<> IF
	    I J <> IF
	       n M{{ I J }}rc-exchange
	       result znegate TO result
	    THEN
	    LEAVE
	 THEN
      LOOP
      
      \ read pivot, adjust result, precompute inverse
      M{{ I 1- I }} 2@                             ( s: pivot)
      2DUP result z*  TO result                    ( s: pivot)
      2DUP z0= IF                  \ zero pivot -> return pfaffian=0
	 2DROP 0 0   UNLOOP EXIT
      THEN   
      zinv { D: p^-1 }

      \ zero column I-1 in rows below and row I-1 in colums right.  Note how
      \ zeroing even rows /and/ colums is sufficient: there will be no
      \ permutation (think: matching) containing any of the odd rows/columns
      \ apart from the subdiagonal.  For this reason row/column update will
      \ also ignore even rows/colums at indices <I-1
      n I 1+ ?DO
	 M{{ J 1- I }} 2@ 2DUP z0<> IF
	    p^-1 z* znegate n J 1- - M{{ J  J 1-  I }}rc-scaleadd
	    ELSE 2DROP THEN
      LOOP
   2 +LOOP			\ iterate over odd I
   result ;

\ Customize Emacs highlighting and indentation for this file
0 [IF]
   Local Variables:
   compile-command: "gforth ./zpfaffian-test.fs -e bye"
   End:
[THEN]

