\ -*-forth-*-
\ (pseudo) Complex number modular arithmetic routines ( GF(p^2) )
\
\ Copyright (C) 2011-2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

\ Note that for this modular arithmetic to be properly defined on complex
\ numbers, i^2 = -1 <=> x^2 = -1 <=> x^2+1 = 0.
\
\ I.e. we compute in the Galois Field modular the polynomial x^2+1.  Note that
\ this field is only well defined if x^2+1 is irreducable under MODULUS ,
\ i.e. -1 having no square root.  This is easily guaranteed by chosing MODULUS
\ so that mod(MODULUS,4)=3 (see
\   https://en.wikipedia.org/wiki/Legendre_symbol
\   https://en.wikipedia.org/wiki/Quadratic_residue#Prime_power_modulus
\ )
\

require ./mod.fs
require ./misc.fs

: zok?  ( n1 n2 -- flag )  rok? SWAP rok? AND ;
: zn  ( n1 n2 -- n3 n4 )  rn SWAP rn SWAP ;
: z+  ( n1 n2 n3 n4 -- n5 n6 )
   ROT r+ >R r+ R> ;
: z-  ( n1 n2 n3 n4 -- n5 n6 )
   ROT SWAP r- >R r- R> ;
: z+!  ( n1 n2 a-addr -- )  dup >R 2@ z+ R> 2! ;
: z-!  ( n1 n2 a-addr -- )  dup >R 2@ 2SWAP z- R> 2! ;
: znegate  ( n1 n2 -- n3 n4 )
   rnegate SWAP rnegate SWAP ;
: zconj  ( n1 n2 -- n3 n4 )
   rnegate ;
: z*  ( n1 n2 n3 n4 -- n5 n6 )
   2OVER 2OVER
   -ROT r* -ROT r* r+  >R  ( imag  )
   ROT r* >R r* R> r-      ( real )
   R> ;
: znormsq  ( n1 n2 -- n3 )
   rsq SWAP rsq r+ ;
: zinv  ( n1 n2 -- n3 n4 )
   zconj  2DUP znormsq rinv >R
   R@ r* SWAP r> r* SWAP ;
: z.  ( n1 n2 -- )
   SWAP 0 .R ?DUP IF ." +" 0 .R ." i " ELSE SPACE THEN ;
: z0=  ( n1 n2 -- flag )
   \ note that some elements are zero, although components are not.  take for
   \ example (1+1i mod 2), which is clearly zero when squared and does not
   \ have an inverse.  This may be unneccessary, see comments about galois
   \ fields above.
   znormsq 0= ;
: z0<>  ( n1 n2 -- flag )
   znormsq 0<> ;

: z>signed  ( n1 n2 -- n3 n4 ) \ be careful, see r>signed
   r>signed SWAP r>signed SWAP ;

\ this is not modular (but may be needed anyways).  put it into misc.fs?
: zsqrt ( n1 n2 -- n3 )
   \ see www.mathpropress.com/stan/bibliography/complexSquareRoot.pdf
   \ real part
   2DUP 2DUP DUP M* ROT DUP M* D+ disqrt >R assert( DUP 0= ) DROP
   DROP R@ + assert( DUP 1 AND 0= ) 2/ isqrt  assert( OVER 0= ) NIP
   \ imaginary part
   -ROT
   R> ROT -  assert( DUP 1 AND 0= ) 2/ isqrt  assert( OVER 0= ) NIP
   SWAP >sign+- * ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth complex-test.fs -e bye"
   End:
[THEN]


   
