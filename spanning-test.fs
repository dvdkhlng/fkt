\ -*-forth-*-
\ test spanning tree
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./spanning.fs -> }T

\ define the planar graph from wiki/Dual_graph
5 CONSTANT #nodes
7 CONSTANT #edges
DOUBLE ,  HERE  0 1 2,  1 2 2,  2 3 2,  1 3 2,  3 4 2,  4 0 2,  1 4 2,  
CONSTANT edges{  

T{ #nodes #edges edges{ }edges>adjmap  CONSTANT adjmap{{ -> }T

T{ adjmap{{ #nodes spanning-tree  CONSTANT #sedges CONSTANT sedges{ -> }T

CR #sedges sedges{ }xyprint
T{ #sedges -> 4 }T
T{ sedges{ 0 } 2@ -> 0 1 }T
T{ sedges{ 1 } 2@ -> 1 2 }T
T{ sedges{ 2 } 2@ -> 2 3 }T
T{ sedges{ 3 } 2@ -> 3 4 }T

5 CONSTANT #idxs
INTEGER ,  HERE  2 , 0 , 1 ,  5 , 4 ,  CONSTANT idxs{

T{ #idxs idxs{ edges{ }spanning-path CONSTANT path{ -> }T
CR #idxs path{ }iprint
T{ path{ 0 } @ -> 2 }T
T{ path{ 1 } @ -> 4 }T
T{ path{ 2 } @ -> 5 }T
T{ path{ 3 } @ -> 0 }T
T{ path{ 4 } @ -> 1 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth spanning-test.fs -e bye"
   End:
[THEN]
