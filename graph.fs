\ -*-forth-*-
\ code for handling graphs
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./matrix.fs
require ./xy.fs
require ./misc.fs
require ./sort.fs

: on-edge?  ( node1 node2 node3 -- flag )
   \ node1 part of edge (node2,node3)?
   ROT TUCK = -ROT = OR ;

: other-node  ( node1 node2 node3 -- node2|node3 )
   \ return the other edge node of edge (node2,node3) that's not node1
   ROT OVER = IF DROP
   ELSE NIP THEN ;

\ Encoding direction of edges and side of nodes on edge via sign.  using one's
\ complement (invert) so we can have a "sign" on 0.  The following routines
\ implement one's complement support (where gforth's two's complement doesn't
\ suffice).  Using one's complement also allows us to store nodes in compact
\ storage (16bit etc.) with sign-extend.

' INVERT ALIAS 1negate  ( x1 -- x2 )
: 1abs  ( x1 -- x2 ) DUP 0< - ABS ;
: 1sign ( x1 -- +1|-1 ) \ return -1 for negative numbers, +1 for 0 and positive
   0< 2* 1+ ;

: other-node'  ( node1 node2 node3 -- -node2|node3 )
   \ return the other edge node of edge (node2,node3) that's not node1
   \ encode in sign-bit whether returned node is the start (+) or end (-) of
   \ the edge
   ROT OVER = IF DROP
   ELSE NIP 1negate THEN ;

: start-node?  ( node1 node2 node3 -- flag )
   \ return whether node1 is the start-node of edge node2,node3
   ROT TUCK = IF 2DROP FALSE EXIT THEN
   assert( 2DUP = )
   2DROP TRUE ;

: }+-@node  ( edges{ idx -- node )
   \ return node of indexed edge, either start (+) or end (-) of edge,
   \ depending on sign-bit of idx
   TUCK 1abs } 2@ ROT 0< IF NIP ELSE DROP THEN ;
: }+-@node'  ( edges{ idx -- node )
   \ return node of indexed edge, either start (+) or end (-) of edge,
   \ depending on sign-bit of idx.  Copy (inverse) sign into output:
   \ Neighbours on incoming nodes are going to be negative.
   TUCK 1abs } 2@ ROT 0< IF NIP ELSE DROP 1negate THEN ;
: }+-@edge  ( edges{ idx -- startnode endnode )
   \ load nodes of edge from edges{}, for negative idx, return edge in
   \ stored order, else invert edge direction.
   TUCK 1abs } 2@  ROT 0>= IF SWAP THEN ;


: }list-neighbours  { node #edges edges{ -- neighbours{ nneighbours }
   \ allot array of neighbours of 'node' in format understood by '}' 
   \ (make sure you ALIGN first)
   INTEGER , HERE  0 		( s: neighbours{ nneighbours )
   #edges 0 ?DO
      node edges{ I } 2@ on-edge? IF
	 node edges{ I } 2@ other-node ,
	 1+
      THEN
   LOOP	;

: }list-neighbours'  { node #edges edges{ -- neighbours{ nneighbours }
   \ like }list-neighbours, but encodes the direction of the edge in the
   \ sign of neighbours.  sign-bit set: outgoing edge, else: incoming edge
   INTEGER , HERE  0 		( s: neighbours{ nneighbours )
   #edges 0 ?DO
      node edges{ I } 2@ on-edge? IF
	 node edges{ I } 2@ other-node' ,
	 1+
      THEN
   LOOP	;

: }list-edges'  { node #edges edges{ -- idxs{ #idxs }
   \ list indices of edges that node lies on.  For outgoing edges, sign bit
   \ of indices will be set (one's complement of idx)
   INTEGER , HERE  0 		( s: idxs{ #idxs )
   #edges 0 ?DO
      node edges{ I } 2@ on-edge? IF
	 I
	 node edges{ I } 2@ start-node? IF 1negate THEN
	 ,
	 1+
      THEN
   LOOP ;

: }list-edge-xys  { #idxs idxs{ xy{ edges{ -- edge-xy{ }
   \ list node-coordinates of indexed edges.  use end-nodes for edge-ids
   \ with sign-bit set, else start-nodes
   DOUBLE , HERE 		( s: xy{ )
   #idxs 0 ?DO
      xy{ edges{ idxs{ I } @ }+-@node } 2@ 2,
   LOOP ;

: outer-node { node1 node2 node3 node4 -- node1|node2 }
   \ for adjacent edges (node1,node2) (node3,node4) that have any direction,
   \ return the outer node on the first edge, i.e. the one that doesn't lie
   \ on both edges.  assertion checks that edges are adjacent
   node1 node3 node4 on-edge? IF
      node2 EXIT 
   THEN
   assert( node2 node3 node4 on-edge? )
   node1 ;
      
: inner-node { node1 node2 node3 node4 -- node1|node2 }
   \ for adjacent edges (node1,node2) (node3,node4) that have any direction,
   \ return the inner node, i.e. the one that lies on both edges.  assertion
   \ checks that edges are adjacent
   node1 node3 node4 on-edge? IF
      node1 EXIT 
   THEN
   assert( node2 node3 node4 on-edge? )
   node2 ;

: }list-path-xys  { #idxs idxs{ xy{ edges{ -- edge-xy{ }
   \ List node-coordinates of indexed edges that lie on a path.  Ignore
   \ sign-bit (1abs) and choses start/nodes as matching for path traversal
   \ return vector with #idxs+1 xy-pairs.  note that for cycles one node will
   \ be listed twice, so only use the first #idx entries.
   DOUBLE , HERE 		( s: xy{ )
   edges{ idxs{ 0 } @ 1abs } 2@
   edges{ idxs{ 1 #idxs MOD } @ 1abs } 2@
   outer-node			( s: xy{ first-node )
   #idxs 0 ?DO
      xy{ OVER } 2@ 2,
      edges{ idxs{ I } @ 1abs } 2@ other-node
   LOOP				( s: xy{ last-node )
   xy{ SWAP } 2@ 2, ;

: }list-edge-vs  { #idxs idxs{ xy{ edges{ -- edge-vs{ }
   \ list edge-vectors of indexed edges.  for edge-ids with sign-bit set
   \ the vector is endnode-startnode, else it is startnode-endnode
   \ (so for edges listed via }list-edges' or or edgemaps, vector always
   \ points towards the neighbour)
   DOUBLE , HERE 		( s: xy{ )
   #idxs 0 ?DO
      idxs{ I } @   { idx }
      xy{ edges{ idx }+-@node } 2@ 
      xy{ edges{ idx 1negate }+-@node } 2@ xy- 2,
   LOOP ;

: }find-edge  ( node1 node2 #edges edges{ -- eidx )
   \ find edge containing both nodes.  nodes don't have to have correct order
   \ todo: factor out edge comparison
   { edges{ }
   0 ?DO
      2DUP 2DUP edges{ I } 2@ 2TUCK D= >R
      SWAP D= R> OR IF
	 2DROP I UNLOOP EXIT
      THEN
   LOOP
   2DROP  -1 ;

: (}edges>map)   { #nodes #edges edges{ sign -- map{{ }
   \ generate a 2-d twice-indexed array: map{{.  every entry maps a node to a
   \ two-cell entry 'addr{ n', that refers to another array with as many
   \ (unfilled) entries as the node has neighbours.
   \ For sign < 0, count incoming, for sign > 0 outgoing, and for sign=0
   \ both kinds of edges, already filling the first part of the map{}
   \ array.
   DOUBLE , HERE DUP #nodes 2* CELLS DUP ALLOT ERASE	{ map{{ }
   #edges 0 ?DO
      edges{ I } 2@
      sign 0<= IF map{{ SWAP } 1 SWAP +! ELSE DROP THEN
      sign 0>= IF map{{ SWAP } 1 SWAP +! ELSE DROP THEN
   LOOP
   
   \ allot the per-node arrays, writing pointers to adjmap{} 
   #nodes 0 ?DO
      INTEGER , HERE 
      map{{ I } DUP @ CELLS ALLOT
      CELL+ !
   LOOP
   map{{ ;
: <fill-map  ( #nodes adjmap{{ -- >adjmap{ )
   \ copy pointers to per-node arrays into temporary array.  used for holding
   \ the incremented insertion pointers when inserting items into the arrays.
   \ then use >adj{} for your insertion operation
   INTEGER , HERE >R 
   DUP ROT }  SWAP CELL+  ?DO  I @ ,   DOUBLE +LOOP
   R> ;
: fill-map>  ( >adjmap{ -- )  \ free memory allocated above
   }unallot ;

\ todo: code below might be greatly compacted by more factorization and/or
\ CREATE..DOES>

: }edges>adjmap  { #nodes #edges edges{ -- adjmap{{ )
   \ Output:
   \ 2-d twice-indexed array: adjmap{{.  every entry maps a node to
   \ a two-cell entry 'addr{ n', that refers to another array listing the
   \ neighbours of the node.  Incoming and outgoing nodes are listed, with
   \ sign-bit set for neighbours on incoming nodes.
   \ You can free the memory via }unallot (NOT }}unalot !)
   #nodes #edges edges{ 0 (}edges>map)			{ adjmap{{ }
   #nodes adjmap{{ <fill-map				{ >adjmap{ }
   #edges 0 ?DO
      edges{ I } 2@
      2DUP SWAP >adjmap{ SWAP } @!+
      SWAP 1negate SWAP >adjmap{ SWAP } @!+
   LOOP
   >adjmap{ fill-map>
   adjmap{{ ;
: }edges>outmap  { #nodes #edges edges{ -- adjmap{{ )
   \ Output:
   \ 2-d twice-indexed array: adjmap{{.  every entry maps a node to
   \ a two-cell entry 'addr{ n', that refers to another array listing the
   \ neighbours on *outgoing* edges of the node.  
   \ You can free the memory via }unallot (NOT }}unalot !)
   #nodes #edges edges{ 1 (}edges>map)			{ adjmap{{ }
   #nodes adjmap{{ <fill-map				{ >adjmap{ }
   #edges 0 ?DO
      edges{ I } 2@ >adjmap{ ROT } @!+
   LOOP
   >adjmap{ fill-map>
   adjmap{{ ;
: }edges>inmap  { #nodes #edges edges{ -- adjmap{{ )
   \ Output:
   \ 2-d twice-indexed array: adjmap{{.  every entry maps a node to
   \ a two-cell entry 'addr{ n', that refers to another array listing the
   \ neighbours on *incoming* edges of the node.  
   \ You can free the memory via }unallot (NOT }}unalot !)
   #nodes #edges edges{ -1 (}edges>map)			{ adjmap{{ }
   #nodes adjmap{{ <fill-map				{ >adjmap{ }
   #edges 0 ?DO
      edges{ I } 2@ >adjmap{ SWAP } @!+
   LOOP
   >adjmap{ fill-map>
   adjmap{{ ;
: }edges>edgemap  { #nodes #edges edges{ -- edgemap{{ )
   \ Output:
   \ 2-d twice-indexed array: edgemap{{.  every entry maps a node to a
   \ two-cell entry 'addr{ n', that refers to another array listing the
   \ indices of edges connected to the node.  Incoming and outgoing edges are
   \ listed, with sign-bit set on outgoing edges.
   #nodes #edges edges{ 0 (}edges>map)			{ edgemap{{ }
   #nodes edgemap{{ <fill-map				{ >edgemap{ }
   #edges 0 ?DO
      edges{ I } 2@
      I >edgemap{ ROT } @!+
      I 1negate >edgemap{ ROT } @!+
   LOOP
   >edgemap{ fill-map>
   edgemap{{ ;
: }edges>outedgemap  { #nodes #edges edges{ -- edgemap{{ )
   \ Output: 2-d twice-indexed array: edgemap{{.  every entry maps a node to a
   \ two-cell entry 'addr{ n', that refers to another array listing the
   \ indices of *outgoing* edges connected to the node.
   #nodes #edges edges{ 1 (}edges>map)			{ edgemap{{ }
   #nodes edgemap{{ <fill-map				{ >edgemap{ }
   #edges 0 ?DO
      edges{ I } 2@ DROP  I >edgemap{ ROT } @!+
   LOOP
   >edgemap{ fill-map>
   edgemap{{ ;
: }edges>inedgemap  { #nodes #edges edges{ -- edgemap{{ )
   \ Output: 2-d twice-indexed array: edgemap{{.  every entry maps a node to a
   \ two-cell entry 'addr{ n', that refers to another array listing the
   \ indices of *outgoing* edges connected to the node.
   #nodes #edges edges{ -1 (}edges>map)			{ edgemap{{ }
   #nodes edgemap{{ <fill-map				{ >edgemap{ }
   #edges 0 ?DO
      edges{ I } 2@ NIP  I >edgemap{ ROT } @!+
   LOOP
   >edgemap{ fill-map>
   edgemap{{ ;
: }}edgemap>adjmap  ( #nodes edges{ edgemap{{ -- )
   \ Convert edgemap to adjmap
   ROT }bounds ?DO		( s: edges{ )
      I 2@ }bounds ?DO
	 DUP I @ }+-@node' I !
      CELL +LOOP
   DOUBLE +LOOP
   DROP ;
: }}edgemap>adjmap'  ( #nodes orient{ edges{ edgemap{{ -- )
   \ Convert edgemap to adjmap, using char-array to override edge orientation
   >R ROT R> SWAP }bounds ?DO		( s: orient{ edges{ )
      I 2@ }bounds ?DO
	 I @ 2DUP }+-@node'  SWAP 2>R	( s: orient{ edges{ r: node edge )
	 OVER R> 1abs } C@ 0<>  R> XOR	\ apply orientation override
	 I !
      CELL +LOOP
   DOUBLE +LOOP
   2DROP ;
: }}sort-map<  ( #nodes adjmap{{  -- )
   \ sort neighbours in adjmap/edgemap/in/outmap into ascending order
   SWAP }bounds ?DO  I 2@ SWAP }sort< DOUBLE +LOOP ;
: }}sort-map>  ( #nodes adjmap{{  -- )
   \ sort neighbours in adjmap/edgemap/in/outmap into descending order
   SWAP }bounds ?DO  I 2@ SWAP }sort> DOUBLE +LOOP ;

: }}has-edge?  { node1 node2 adjmap{{ -- flag }
   \ return whether graph contains edge (node1,node2).  as adjmap{{ pass
   \ the result from }edges>adjmap
   adjmap{{ node2 } 2@ }bounds ?DO
      I @ 1abs node1 = IF   TRUE UNLOOP EXIT THEN
   CELL +LOOP
   FALSE ;   

: }}has-edge?'  { node1 node2 edges{ edgemap{{ -- flag }
   \ return whether graph contains edge (node1,node2).  same functionality
   \ as }}has-edge?, just with an edgemap + edge{} array used on input.
   edgemap{{ node1 } 2@ }bounds ?DO
      edges{ I @ }+-@node node2 = IF   TRUE UNLOOP EXIT THEN
   CELL +LOOP
   FALSE ;   

: }}sort-edgemap { #nodes xy{ edges{ edgemap{{ -- }
   \ sort all edge-lists in the edgemap so that edges are listed in
   \ counter-clockwise order
   edgemap{{ #nodes }bounds ?DO
      I 2@					{ eidxs{ #aedges  }
      #aedges eidxs{ xy{ edges{ }list-edge-vs	{ ev{ }
      INTEGER , HERE  #aedges 0 DO  I , LOOP	{ sortidxs{ }
      #aedges sortidxs{ ev{ }xysort-ccw'	\ new order in sortidxs{
      HERE					( s: eidxs2{ )
      sortidxs{ #aedges }bounds ?DO		\ create sorted eidxs2{
	 eidxs{ I @ } @ ,
      CELL +LOOP
      eidxs{ #aedges CELLS MOVE			\ copy eidxs{ to eidxs{
      ev{ }unallot				\ free memory
   DOUBLE +LOOP ;

: }prev-edge  ( eidx1 eidxs{ #neighbours -- eidx2 )
   \ Return the edge that precedes edge eidx1 in the edge array.  Use with
   \ edge-lists in the edgemap{{ (probably after sorting).  Operates cyclic:
   \ eidx{0} is next edge from eidxs{n-1}.  Edge sign ignored on edge search,
   \ but returned unchanged in eidxs
   2DUP 1- } @ -ROT     ( s: eidx1 previdx eidxs{ #neighbours )
   }bounds ?DO
      OVER 1abs I @ 1abs = IF	\ found eidx1?  return previdx from stack.
	 NIP UNLOOP EXIT
      THEN
      DROP I @			\ record previous
   CELL +LOOP
   assert0( false ) ;

: }}adjmap>seidelmatrix  { #nodes adjmap{{ matrix{{ -- }
   \ set matrix{ to contain the +1,-1,0 seidel matrix of the graph
   #nodes 0 DO
      \ initialize entries to 1 (n.c.), diagonal to 0
      #nodes 0 DO   1 matrix{{ J I }} !  LOOP
      0 matrix{{ I I }} !
      adjmap{{ I } 2@ }bounds ?DO
	 \ override to -1 on outgoing (positive) edges
	 I @ DUP 1sign NEGATE SWAP matrix{{ J ROT 1abs }} !
      CELL +LOOP
   LOOP ;

: (}}adjmap>+-0matrix)  { #nodes adjmap{{ matrix{{ +one -one -- }
   \ set matrix{ to contain the -ONE,+ONE,0 adjacency matrix of the graph in
   \ case that matrix{{ is a complex matrix, note that the result will be a
   \ -ONE*i,+ONE*i,0 matrix, due to word order implemented by Forth.
   matrix{{ 0 0 }}  matrix{{ #nodes 0 }}  OVER - ERASE
   #nodes 0 DO
      \ initialize entries to 0 (n.c.)
      #nodes 0 DO   0 matrix{{ J I }} !  LOOP
      0 matrix{{ I I }} !
      adjmap{{ I } 2@ }bounds ?DO
	 \ -1/+1 depending on direction
	 I @ DUP 1sign 0< IF -one ELSE +one THEN
	 SWAP matrix{{ J ROT 1abs }} !
      CELL +LOOP
   LOOP ;

: }}adjmap>+-0matrix  ( #nodes adjmap{{ matrix{{ -- )
   \ set matrix{ to contain the -1,+1,0 adjacency matrix of the graph in case
   \ that matrix{{ is a complex matrix, note that the result will be a -i,+i,0
   \ matrix, due to word order implemented by Forth.
   1 -1 (}}adjmap>+-0matrix) ;

: make-square-lattice { #n #m -- #nodes #edges xy{ edges{ }
   \ Create a #n x #m square lattice graph.  You can free the memory allotted
   \ via xy{ }unallot.
   DOUBLE , HERE   { xy{ }
   #n 0 DO  #m 0 DO  J I 2,  LOOP  LOOP
   DOUBLE , HERE   { edges{ } 
   #n 0 DO
      #m 0 DO
	 J #m * I +  { node }
	 I 1+ #m < IF  node node 1+ 2, THEN
	 J 1+ #n < IF  node node #m + 2, THEN
      LOOP
   LOOP
   #n #m *			( #nodes)
   #m 1- #n *  #n 1- #m * +	( #edges)
   xy{ edges{ ;


\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth graph-test.fs -e bye"
   End:
[THEN]
