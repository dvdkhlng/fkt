\ -*-forth-*-
\ Holographic Algorithm test-case
\
\ Copyright (C) 2012,2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Jul 2012

require ./tester2.fs
require ./imatrix.fs
require ./fkt.fs
require ./drawing.fs
T{ require ./ha.fs -> }T

T{ 5 -> 5 }T

7 MODULUS !

DOUBLE , HERE  1 0 2,  0 0 2,  CONSTANT b0n{
DOUBLE , HERE  0 0 2,  1 0 2,  CONSTANT b0p{

DOUBLE , HERE  1 0 znegate 2,  1 0 2,  CONSTANT b1n{
DOUBLE , HERE  1 0 2,  0 0 2,  CONSTANT b1p{

DOUBLE , HERE  1 0 2,  1 0 2,  CONSTANT b2n{
DOUBLE , HERE  1 0 2,  1 0 znegate 2,  CONSTANT b2p{

4 DOUBLE MARRAY tp{

T{ 2 b1n{ 2 b1p{ tp{ }ztensp -> }T
T{ tp{ 0 } 2@+ 2@+ 2@+ 2@+ DROP 
-> 1 0 znegate   0 0   1 0   0 0 }T

DOUBLE , HERE 0 0 2,   0 0 2,   1 0 2,   0 0 2,  CONSTANT test{
8 DOUBLE MARRAY tp2{
T{ 4 test{ 2 b0n{ tp2{ }ztensp -> }T
T{ tp2{ 0 } 2@+ 2@+ 2@+ 2@+ 2@+ 2@+ 2@+ 2@+ DROP
-> 0 0 0 0 0 0 0 0
   1 0 0 0 0 0 0 0 }T

16 16 DOUBLE MMATRIX m0{{
T{ 4 2 b0n{ b0p{ m0{{ }}zbasetrafo -> }T
CR .( M0: ) CR
16 16 m0{{ }}zprint

16 16 DOUBLE MMATRIX m1{{
T{ 4 2 b1n{ b1p{ m1{{ }}zbasetrafo -> }T
CR .( M1: ) CR
16 16 m1{{ }}zprint

16 16 DOUBLE MMATRIX m2{{
T{ 4 2 b2n{ b2p{ m2{{ }}zbasetrafo -> }T
CR .( M2: ) CR
16 16 m2{{ }}zprint

32 32 DOUBLE MMATRIX m2-5{{
T{ 5 2 b2n{ b2p{ m2-5{{ }}zbasetrafo -> }T
CR .( M2: ) CR
32 32 m2-5{{ }}zprint


0 VALUE #points
0 VALUE #lines
0 VALUE xy{
0 VALUE lines{
0 VALUE colors{

draw
    0 0 point: a
    4 0 point: b
    4 2 point: c
    0 2 point: d
    2 0 point: (e)
    2 2 point: (f)
    a (e) line  (e) b line  b c line
    c (f) line  (f) d line  d a line
    (e) (f) -1 cline
end-draw  TO colors{ TO lines{ TO xy{ TO #lines TO #points

0 VALUE am{{
T{ #points #lines xy{ lines{ colors{ false fkt-wedges>matrix TO am{{ -> }T
modulus @ .
CR .( am{{}} : ) CR
#points DUP am{{ }}iprint

CR .( w{} : ) CR
#lines colors{ }iprint

16 INTEGER MARRAY sig{
T{ 4 #points am{{ sig{ (}}signature) -> }T

CR .( sig: ) 16 sig{ }iprint

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth ha-test.fs -e bye"
   End:
[THEN]


   
