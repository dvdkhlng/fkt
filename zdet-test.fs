\ -*-forth-*-
\ zdet test
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./zdet.fs -> }T

CR
31 modulus !

0 VALUE tt{{

3 , DOUBLE , HERE TO tt{{
2 0 2,  0 0 2,  0 0 2,
0 0 2,  0 3 2,  0 0 2,
0 0 2,  0 0 2,  5 0 2,
T{ 3 tt{{ }}zdet -> 0 30 }T

3 , DOUBLE , HERE TO tt{{
2 0 2,  0 0 2,  0 0 2,
0 0 2,  0 0 2,  5 0 2,
0 0 2,  1 3 2,  0 0 2,

T{ 3 tt{{ }}zdet znegate -> 10 30 }T

127 modulus !
3 , DOUBLE , HERE TO tt{{
2 0 2,  8 0 2,  0 0 2,
4 1 2,  4 0 2,  2 0 2,
1 0 2,  4 1 2,  5 0 2,

T{ 3 tt{{ }}zdet znegate -> 120 44 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth zdet-test.fs -e bye"
   End:
[THEN]
