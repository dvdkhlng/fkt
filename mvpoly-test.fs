\ -*-forth-*-
\ Multi-variate polynomial arithmetic unit-test
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA posteo TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: July 2021
3 assert-level !

require ./tester2.fs
require ./fsl-util.fs
T{ require ./mvpoly.fs -> }T

T{ /mvp-term   -> 256 CELL+ }T
T{ x0 .mvp   mvp-depth -> 0 }T
T{ x4 .mvp   mvp-depth -> 0 }T
T{ 10 123 mvp-x_n^k .mvp  mvp-depth -> 0 }T
T{ x4  mvp-compress .mvp   mvp-depth -> 0 }T

CR .( .mvp :)
T{ mvp0 ' .mvp $tmp 2DUP TYPE  S" ( 0)" str=
mvp-depth -> true 0 }T
T{ mvp1 ' .mvp $tmp 2DUP .( <) TYPE .( >) S" ( +1 )" str=
mvp-depth -> true 0 }T

CR .( mvp+ :)
T{ x1 x3 mvp+   ' .mvp $tmp 2DUP TYPE  S" ( +1 x1 +1 x3)" str=
mvp-depth -> true 0 }T

T{ x2 x2 mvp+   ' .mvp $tmp 2DUP TYPE  S" ( +2 x2)" str=
mvp-depth -> true 0 }T

CR .( mvp-sort:)
T{ x1 x3   mvp+ x10 mvp+  x5 x5 mvp* mvp+ mvp-sort  ' .mvp $tmp 2DUP TYPE
S" ( +1 x1 +1 x3 +1 x10 +1 x5^2)" str=
mvp-depth -> true 0 }T

CR .( mvp*n :)
T{ x3 x1 mvp+  -123 mvp*n  ' .mvp $tmp 2DUP TYPE  S" ( -123 x1 -123 x3)" str=
mvp-depth -> true 0 }T
T{ x3 x1 mvp*  -123 mvp*n  ' .mvp $tmp 2DUP TYPE  S" ( -123 x1x3)" str=
mvp-depth -> true 0 }T

CR .( mvp compress :)
T{ x1 x3 mvp+    x3 -1 mvp*n  mvp+ ' .mvp $tmp 2DUP TYPE  S" ( +1 x1)" str=
mvp-depth -> true 0 }T

CR .( mvp* :)
T{ mvp1 x1 mvp*   ' .mvp $tmp 2DUP TYPE  S" ( +1 x1)" str=
mvp-depth -> true 0 }T
T{ x1 mvp1 mvp*   ' .mvp $tmp 2DUP TYPE  S" ( +1 x1)" str=
mvp-depth -> true 0 }T

T{ x4 x1 mvp*   ' .mvp $tmp 2DUP TYPE  S" ( +1 x1x4)" str=
mvp-depth -> true 0 }T
CR
T{ x4 x1 mvp+  x3 mvp*   ' .mvp $tmp 2DUP TYPE  S" ( +1 x1x3 +1 x3x4)" str=
mvp-depth -> true 0 }T
CR
T{ x3   x4 x1 mvp+  mvp*   ' .mvp $tmp 2DUP TYPE  S" ( +1 x1x3 +1 x3x4)" str=
mvp-depth -> true 0 }T
CR
T{ x4 x1 mvp+  mvp-dup  mvp*   ' .mvp $tmp 2DUP TYPE
S" ( +1 x1^2 +2 x1x4 +1 x4^2)" str=
mvp-depth -> true 0 }T

T{ x4 x1 mvp+  mvp-dup  mvp*  mvp1 mvp*  ' .mvp $tmp 2DUP TYPE
S" ( +1 x1^2 +2 x1x4 +1 x4^2)" str=
mvp-depth -> true 0 }T
T{ mvp1   x4 x1 mvp+  mvp-dup  mvp*  mvp*  ' .mvp $tmp 2DUP TYPE
S" ( +1 x1^2 +2 x1x4 +1 x4^2)" str=
mvp-depth -> true 0 }T

CR .( mvp** :)
T{ x4 0 mvp**   ' .mvp $tmp 2DUP TYPE  S" ( +1 )" str=
mvp-depth -> true 0 }T
T{ x4 1 mvp**   ' .mvp $tmp 2DUP TYPE  S" ( +1 x4)" str=
mvp-depth -> true 0 }T
T{ x4 2 mvp**   ' .mvp $tmp 2DUP TYPE  S" ( +1 x4^2)" str=
mvp-depth -> true 0 }T

CR .( Binomial)
T{ x1 x4 mvp+ 2 mvp**  ' .mvp $tmp 2DUP TYPE
S" ( +1 x1^2 +2 x1x4 +1 x4^2)" str=
mvp-depth -> true 0 }T

CR
T{ x1 x4 mvp+ 4 mvp**  ' .mvp $tmp 2DUP TYPE
S" ( +1 x1^4 +4 x1^3x4 +6 x1^2x4^2 +4 x1x4^3 +1 x4^4)" str=
mvp-depth -> true 0 }T

CR .( Maxidx)
T{ x1 x4 mvp*  x10 x5 mvp* mvp+  x7 mvp+   mvp-maxidx mvp-depth -> 10 0 }T

CR .( Eval)
ALIGN INTEGER ,  HERE
123 ,
567 ,
890 ,
0 ,
CONSTANT values{
T{ values{ x0 mvp-eval mvp-depth -> 123 0 }T
T{ values{ x1 mvp-eval mvp-depth -> 567 0 }T
T{ values{ x2 mvp-eval mvp-depth -> 890 0 }T
T{ values{ x3 mvp-eval mvp-depth -> 0 0 }T
T{ values{ x1 x2 mvp*  x0 mvp+ mvp-eval mvp-depth -> 567 890 * 123 +   0 }T
T{ values{ x0 x3 mvp+ 3 mvp** mvp-eval ->  123 3 n**  }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth -m 1G mvpoly-test.fs -e bye"
   End:
[THEN]

