\ -*-forth-*-
\ define modules (simple namespaces)
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

VARIABLE current-module  \ holds WID of current module or 0 

: module  ( "name" --  )
   \ like VOCABULARY, but rembers current module, and already adds wordlist to
   \ search-order at definition time.
   CREATE    WORDLIST DUP ,  DUP current-module ! >order
  DOES>
   \ run-time action only defined to make module-internal stuff visible from
   \ other modules and for debugging.  works like VOCABULARY.
   @ >order ;		

: end-module  ( -- )  \ remove private list from search order
   PREVIOUS 0 current-module ! ;

\ fsl-util.4th defines private: and public:, so we use <private> instead

: <private>  ( -- )  \ compile into modules' wordlist
   current-module @ SET-CURRENT ;
: <public>  ( -- )  \ compile into default wordlist
   PREVIOUS DEFINITIONS
   current-module @ >order ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth module-test.fs -e bye"
   End:
[THEN]
