\ -*-forth-*-
\ Gaussion matrix inversion of pseudo-complex matrix
\
\ Copyright (C) 2012,2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Jul 2012

require ./complex.fs
require ./matrix.fs
require ./zemt.fs
require ./module.fs

module zgauss
<private>

: }}find-pivot  { k n m{{ -- j ) 
   \ search pivot for row k in nxn matrix.  Return pivot row J.  Caller should
   \ then perform row-swapping of K and J
   n k ?DO
      M{{ I k }} 2@ z0<> IF
	 I UNLOOP EXIT
      THEN
   LOOP
   -1 ;  \ no pivot found (matrix not invertible)

<public>

: }}zinv { n m{{ -- det }
   \ in-place inversion of nxn matrix using gauss-jordan algorithm.  Also
   \ return determinant of original matrix m{{.
   INTEGER , HERE { pivotidx{ } \ array holding our pivots (filled via , )
   1.  { d: det }
   n 0 ?DO
      I n m{{ }}find-pivot DUP 0< ABORT" }}zinv: singular matrix"
      DUP ,  n m{{ ROT I }}swap-rows
      m{{ I I }} 2@			\ fetch diagonal element
      2DUP det z* TO det		\  .. use it to update determinant
      zinv { d: pivot } 		\  .. invert it and remember as PIVOT
      n 0 ?DO
	 I J <> IF  \ for all other rows/columns
	    \ row-multiply-and-add transform 
	    M{{ I J }} DUP 2@ znegate pivot z*   ( s: addr-ij zfactor )
	    2DUP n M{{ J I }}zrowadd  \ row transform (needlessly includes I,J)
	    ROT 2!		      \ store factor as result overwriting I,J
	 THEN
      LOOP

      \ now multiply row with (inverted) pivot (old value of row used above)
      pivot n M{{ I }}zrowmul
      pivot M{{ I I }} 2!		\ ovewrite pivot with inverse
   LOOP
   
   \ for every row-permutation performed on M we now have to do the
   \ corresponding column-permutation on the inverse.  Also fix sign of det
   \ here.
   n 0 ?DO
      I pivotidx{ I } @ 2DUP <> IF
	 n M{{ 2SWAP  }}2swap-cols
	 det znegate TO det
      ELSE 2DROP THEN
   LOOP
   pivotidx{ }unallot
   det ;

end-module   

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth zgauss-test.fs -e bye"
   End:
[THEN]
