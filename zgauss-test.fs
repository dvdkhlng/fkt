\ -*-forth-*-
\ Gaussion matrix inversion test-case
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Jul 2012

require ./tester2.fs
require ./imatrix.fs
T{ require ./zgauss.fs -> }T

20 maxdepth-.s !
7 MODULUS !

0 VALUE tt{{

3 , DOUBLE , HERE TO tt{{
2 0 2,  0 0 2,  0 0 2,
0 0 2,  0 3 2,  0 0 2,
0 0 2,  0 0 2,  5 0 2,
T{ 3 tt{{ }}zinv -> 2 0  0 3 z*  5 0 z* }T

T{ tt{{ 1 1 }} 2@ -> 0 3 zinv }T
T{ tt{{ 2 2 }} 2@ -> 5 0 zinv }T

127 modulus !
3 , DOUBLE , HERE TO tt{{
0 0 2,  8 0 2,  0 0 2,
4 1 2,  4 0 2,  2 0 2,
1 0 2,  4 1 2,  5 0 2,
T{ 3 tt{{ }}zinv -> 110 87 }T
CR 3 3 tt{{ }}zprint
T{ 3 tt{{ }}zinv -> 110 87 zinv }T
T{ tt{{ 0 0 }} 2@+ 2@+ 2@+  2@+ 2@+ 2@+  2@+ 2@+ 2@ ->
0 0  8 0  0 0   4 1  4 0  2 0   1 0  4 1  5 0 }T
CR 3 3 tt{{ }}zprint

3 , DOUBLE , HERE TO tt{{
0 0 2,  0 0 2,  1 0 2,
0 0 2,  2 0 2,  0 0 2,
3 0 2,  0 0 2,  0 0 2,
T{ 3 tt{{ }}zinv -> 2 3 r* rnegate 0 }T
T{ 3 tt{{ }}zinv -> 2 3 r* rnegate rinv 0 }T
T{ tt{{ 0 0 }} 2@+ 2@+ 2@+  2@+ 2@+ 2@+  2@+ 2@+ 2@
-> 0 0  0 0  1 0  0 0  2 0  0 0  3 0  0 0  0 0 }T
CR 3 3 tt{{ }}zprint

\ todo: add some more tests on larger matrices.
\ todo: verify matrix outputs (also pin intermediate results?)

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth zgauss-test.fs -e bye"
   End:
[THEN]
