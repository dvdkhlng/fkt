\ -*-forth-*-
\ Sorting algorithms
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./matrix.fs

: }sorter:  ( xt "name" -- )  CREATE ,
  DOES> @ { n vector{ xt -- }
   \ bring elements into order for which xt returns true on consecutive
   \ elements (uses bubble sort)
   n 2 < IF  EXIT THEN
   BEGIN
      TRUE { done }
      n 1- 0 ?DO
	 vector{ I } DUP 2@ 2DUP xt EXECUTE IF   \ 2@ loads swapped, so no 0=
	    SWAP ROT 2!  FALSE TO done
	 ELSE DROP 2DROP THEN
      LOOP
   done UNTIL ;

' < }sorter: }sort<
' > }sorter: }sort>

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth sort-test.fs -e bye"
   End:
[THEN]
