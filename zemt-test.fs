\ -*-forth-*-
\ Elementary matrix transform tests
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014

require ./tester2.fs
T{ require ./zemt.fs -> }T


3 CONSTANT m
4 CONSTANT n
0 VALUE m{{
3 , DOUBLE , HERE TO m{{
2 0 2,  0 0 2,  2 0 2, 6 0 2,
9 0 2,  3 0 2,  1 0 2, 1 0 2,
0 0 2,  0 0 2,  5 0 2, 7 0 2,

m n DOUBLE MMATRIX mcopy{{
: copym  m{{ mcopy{{ m n * 2* CELLS MOVE ;

T{ copym 2 0  2 mcopy{{ 0 1 1 (}}zrowadd) -> }T
T{ mcopy{{ 1 0 }} 2@+ 2@+ 2@+ 2@ -> 9 0 3 0 5 0 1 0 }T

T{ copym 2 0  2 mcopy{{ 0 1 2 (}}zcoladd) -> }T
T{ mcopy{{ 0 2 }} 2@ mcopy{{ 1 2 }} 2@ mcopy{{ 2 2 }} 2@  -> 2 0 7 0 5 0 }T

T{ copym  2 3  2 mcopy{{ 1 1 (}}zrowmul) -> }T
T{ mcopy{{ 1 0 }} 2@+ 2@+ 2@+ 2@ -> 9 0  6 9  2 3  1 0  }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth zemt-test.fs -e bye"
   End:
[THEN]
