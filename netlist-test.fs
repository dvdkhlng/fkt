\ -*-forth-*-
\ Netlist test-case
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./netlist.fs -> }T
T{ require ./graph.fs -> }T

0 VALUE #gates
0 VALUE #vars
0 VALUE #wires
0 VALUE wires{
0 VALUE gates{
0 VALUE adjmap{{

T{ 
netlist
-1 -1 1 2gate CONSTANT gatea
0 -1 1 2gate  CONSTANT gateb
0 1 1 2gate CONSTANT gatec
end-netlist TO gates{ TO wires{ TO #wires TO #vars TO #gates
-> }T
T{ #gates -> 3 }T
T{ #wires -> 6 }T
T{ #vars -> 3 }T
T{ #gates #vars + #wires wires{ }edges>adjmap TO adjmap{{ -> }T
T{ adjmap{{ 3 } @ ->  1 }T
T{ adjmap{{ 4 } @ ->  1 }T
T{ adjmap{{ 5 } @ ->  1 }T
T{ adjmap{{ 0 } @ ->  4 }T
T{ adjmap{{ 1 } @ ->  3 }T
T{ adjmap{{ 2 } @ ->  2 }T

3 INTEGER MARRAY values{
: >values  0 2 DO  values{ #gates I + } ! -1 +LOOP ;
: donet ( n1 n2 n3 -- n1 n2 n3 )
   >values
   #gates #vars #wires wires{ gates{ values{ netlist-eval
   #gates 0 ?DO values{ I } @ LOOP ;
   
T{ 0 0 0 donet -> 1 1 0 }T
T{ 0 0 1 donet -> 1 0 1 }T
T{ 0 1 0 donet -> 1 1 0 }T
T{ 1 1 0 donet -> 0 1 1 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth netlist-test.fs  -e bye"
   End:
[THEN]
