\ -*-forth-*-
\ Test-cases for drawing.fs
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./drawing.fs -> }T

0 VALUE #points
0 VALUE #lines
0 VALUE xy{
0 VALUE lines{
0 VALUE zcolors{
0 VALUE colors{

T{
draw
0 0 point: a
2 0 2 clineto: b
2 2 lineto: c
1 3 point: d
1 1 walk
-1 1 point: e

( a b line  b c line)  c d line  d e 2 cline  e a -1 cline
end-zdraw  TO zcolors{ TO lines{ TO xy{ TO #lines TO #points
-> }T

T{ #points -> 5 }T
T{ #lines -> 5 }T
T{ xy{ 3 } 2@ -> 1 3 }T
T{ xy{ 4 } 2@ -> 0 2 }T
T{ zcolors{ 3 } 2@ DROP -> 2 }T
T{ zcolors{ 4 } 2@ DROP -> -1 }T

T{ s" drawing-ztest.ps" w/o create-file THROW CONSTANT fid -> }T
T{ #points #lines xy{ lines{ zcolors{ ' drawing>ps fid outfile-execute -> }T
T{ fid close-file THROW -> }T

T{ end-draw printto drawing-test.ps -> }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth drawing-test.fs  -e bye"
   End:
[THEN]


