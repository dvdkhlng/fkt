\ 4HDL object stack test case
\
\ Copyright (C) 2010,2021 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPL3 or later, NO WARRANTY
\
\ Created: Sep 2010

require ./tester2.fs
T{ require ./ostack2.fs -> }T

T{  odepth -> 0  }T
T{  10 oalloc constant o-addr ->  }T
T{  odepth -> 10 aligned cell+ }T
T{  oaddr -> o-addr }T
T{  osize -> 10 }T
T{  otos -> o-addr 10 }T
T{  s" test1" opush -> }T
T{  osize -> 5  }T
T{  onos -> o-addr 10  }T
T{  otos s" test1" str= -> true  }T
T{  odrop  osize -> 10 }T
T{  odrop  odepth -> 0 }T

T{  s" test2abcdef" opush  osize -> 11 }T
T{  3 oresize  osize -> 3 }T
T{  otos s" tes" str= -> true }T
T{  11 oresize  osize -> 11 }T
T{  otos 3 min s" tes" str= -> true }T
T{  odrop  odepth -> 0 }T

T{  s" test2abcdef" opush  osize -> 11 }T
T{  s" abc" opush  osize -> 3 }T
T{  oover osize -> 11 }T
T{  otos s" test2abcdef" str= -> true }T
T{  odrop odrop odrop odepth -> 0 }T

T{  s" test2abcdef" opush  osize -> 11 }T
T{  s" abc" opush  osize -> 3 }T
T{  onip  osize -> 3 }T
T{  otos s" abc" str= -> true }T
T{  odrop odepth -> 0 }T

T{  s" test2abcdef" opush  osize -> 11 }T
T{  s" abc" opush  osize -> 3 }T
T{  oswap  osize -> 11 }T
T{  otos s" test2abcdef" str= -> true }T
T{  onos s" abc" str= -> true }T
T{  odrop otos s" abc" str= -> true }T
T{  odrop odepth -> 0 }T

T{  s" test2abcdef" opush  osize -> 11 }T
T{  s" ABC" opush  osize -> 3 }T
T{  oconcat o.s  osize -> 14 }T
T{  otos s" test2abcdefABC" str= -> true }T
T{  odrop depth -> 0 }T

T{  s" abc" opush  0 oresize osize odrop odepth -> 0 0 }T
T{  0 oalloc drop  0 oresize osize odrop odepth -> 0 0 }T

T{  0 oalloc drop  osize  odepth odrop 0<> -> 0 true }T
T{  0 oalloc drop  oaddr ~~ drop s" test" opush oconcat otos s" test" str= -> true }T
T{  odrop  odepth -> 0 }T

T{  : testolit  [ s" abc" opush ] oliteral ;  odepth  -> 0 }T
T{  testolit  osize odrop odepth -> 3 0 }T

\ T{  s" abcdef" opush oconstant oconst   odepth  -> 0 }T
\ T{  oconst  otos s" abcdef" str= -> true }T
\ T{  odrop  odepth -> 0 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth ./test-ostack.fs -e bye"
   End:
[THEN]
