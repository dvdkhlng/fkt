\ -*-forth-*-
\ Matrix multiplication
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014

require ./mod.fs
require ./matrix.fs

: }}rdot-rc  { n a{{ aj ai b{{ bj bi -- val }
   \ row-column dot product of vectors length n: row-vector starting at
   \ A(aj,ai), column-vector at B(bj,bi).
   a{{ aj ai }}
   b{{ bj bi }}
   0 ( s: row-addr col-addr accu )
   a{{ }eltsize b{{ }}rowsize { rstride cstride }
   n 0 ?DO
      >R OVER @ OVER @ r* R> r+
      ROT rstride + ROT cstride + ROT
   LOOP
   NIP NIP ;

: (}}rmatmul)  { m n kk a{{ aj ai b{{ bj bi c{{ cj ci -- }
   \ Matrix multiplication using sub-matrices A, B starting from A(aj,ai) and
   \ B(bj,bi) respectively.  
   \ C := A * B  ;  C : m x n   A : m x k   B: k x n
   m 0 ?DO
      n 0 ?DO 
	 kk a{{ J aj + ai b{{ bj I bi + }}rdot-rc  c{{ J cj + I ci + }} !
      LOOP
   LOOP ;
: }}rmatmul  ( m n kk a{{ b{{ c{{ -- )
   \ matrix multiplication
   \ C := A * B  ;  C : m x n   A : m x k   B: k x n
   0 0 2SWAP 0 0 ROT 0 0 (}}rmatmul) ;
: (}}rmatmadd)  { m n kk alpha a{{ aj ai b{{ bj bi c{{ cj ci -- }
   \ matrix multiplication and add using sub-matrices 
   \ C := C + alpha * A * B  ;  C : m x n   A : m x k   B: k x n
   m 0 ?DO
      n 0 ?DO 
	 kk a{{ J aj + ai b{{ bj I bi + }}rdot-rc alpha r*
	 c{{ J cj + I ci + }} r+!
      LOOP
   LOOP ;
: }}rmatmadd  ( m n kk alpha a{{ b{{ c{{ -- )
   \ matrix multiplication and add:
   \ C := C + alpha * A * B  ;  C : m x n   A : m x k   B: k x n
   0 0 2SWAP 0 0 ROT 0 0 (}}rmatmadd) ;

: }}rmatpow  { n a{{ u -- }
   \ Rise nxn matrix A to the u'th power, replacing A with the result
   \ Temporarily use 2*n*n CELLS of dictionary memory.
   n , INTEGER , HERE  n n * CELLS ALLOT  { sq{{ }   \ work matrix 
   a{{ sq{{ n n * CELLS MOVE
   n , INTEGER , HERE  n n * CELLS ALLOT  { prod{{ }  \ second work matrix
   u 1 AND 0= IF   \ special case for u bit 0
      n a{{ }}I!
   THEN
   u BEGIN
      1 RSHIFT ?DUP \ loop for bits 1..MSB
   WHILE   
	 n n n sq{{ sq{{ prod{{ }}rmatmul   \ repeated squaring..
	 sq{{ prod{{ TO sq{{ TO prod{{      \ ..with swapping buffers 
	 DUP 1 AND IF		 	    \ multiply into result
	    a{{ prod{{ n n * CELLS MOVE	      \ this time no buffer swap, copy
	    n n n sq{{ prod{{ a{{ }}rmatmul   
	 THEN
   REPEAT
   sq{{ prod{{ MIN }}unallot ;   \ correctly free even when matrices swapped!
	    

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth rmatmul-test.fs -e bye"
   End:
[THEN]

