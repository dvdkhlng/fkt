\ -*-forth-*-
\ test modules definition code
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./module.fs -> }T

\ define variables outside modle, so we can test how module-internal variables
\ shadow these variables
123 CONSTANT test-var1
123 CONSTANT test-var2
123 CONSTANT test-var3
123 CONSTANT test-var4

\ define module, overriding all these variables
T{ module test -> }T
345 CONSTANT test-var1
<public>
345 CONSTANT test-var2
<private>
345 CONSTANT test-var3
<public>
345 CONSTANT test-var4

\ test visibility inside module
T{ test-var1 -> 345 }T
T{ test-var2 -> 345 }T
T{ test-var3 -> 345 }T
T{ test-var4 -> 345 }T
T{ end-module -> }T

\ test visibility outside module
T{ test-var1 -> 345 }T
T{ test-var2 -> 345 }T
T{ test-var3 -> 123 }T
T{ test-var4 -> 345 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth module-test.fs -e bye"
   End:
[THEN]
