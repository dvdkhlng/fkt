\ -*-forth-*-
\ Elementary matrix transforms for matrices with elments from finite field
\ of dimension p²
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014

require ./matrix.fs
require ./complex.fs

: (}}zrowadd) { d: zalpha n m{{ r1 c r2 -- }
   \ In matrix M add ALPHA times the values in columns c..c+n-1 of row r1 to
   \ row r2 
   m{{ r1 c }} 
   m{{ r2 c }} 
   n 0 ?DO 			( s: addr1 addr2 )
      2DUP DUP >R 2@ ROT 2@  zalpha z* z+  R> 2!
      DOUBLE + SWAP DOUBLE + SWAP
   LOOP
   2DROP ;
: }}zrowadd  ( zalpha n m{{ r1 r2 -- )
   0 SWAP (}}zrowadd) ;

: (}}zcoladd) { d: zalpha n m{{ r c1 c2 -- }
   assert3( zalpha zok? )
   \ In matrix M add ALPHA times the values in rows r..r+n-1 of column c1 to
   \ column c2
   m{{ r c1 }}   
   m{{ r c2 }}
   m{{ }}rowsize		{ stride }
   n 0 ?DO			( s: addr1 addr2 )
      2DUP DUP >R 2@ ROT 2@
      zalpha z* z+  R> 2!
      stride + SWAP stride + SWAP
   LOOP
   2DROP ;
: }}zcoladd  ( zalpha n m{{ c1 c2 -- )
   0 -ROT (}}zcoladd) ;

: (}}zrowmul) ( zalpha n m{{ r c -- )
   \ In matrix M multiply elements in row R from column C..C+N-1 by ALPHA
   }} SWAP CELLS 2* bounds ?DO  ( s: alpha )
      2DUP I 2@ z* I 2!
   DOUBLE +LOOP
   2DROP ;
: }}zrowmul ( zalpha n m{{ r -- )
   0 (}}zrowmul) ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth zemt-test.fs -e bye"
   End:
[THEN]
