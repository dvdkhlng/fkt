\ -*-forth-*-
\ test gcd.fs routines
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./tester2.fs
T{ require ./gcd.fs -> }T

T{ 13 5 gcd -> 1 }T
T{ 15 5 gcd -> 5 }T
T{ 15 1 gcd -> 1 }T
T{ 15 15 gcd -> 15 }T
T{ 21 14 gcd -> 7 }T
T{ 99 66 gcd -> 33 }T
T{ 99 55 gcd -> 11 }T
T{ 127337 600303 gcd -> 18191 }T

T{ 15. 15. dgcd -> 15. }T
T{ 99. 66. dgcd -> 33. }T
T{ 34. 51. dgcd  -> 17. }T
T{ 11111. 22222. dgcd -> 11111. }T
T{ 127337. 600303. dgcd -> 18191. }T
T{ 70573097979979. 123457773838. dgcd -> 63835457. }T

T{ 120 23 bezout -> -9 47 }T
T{ 3 7 bezout -> -2 1 }T
T{ 127337 600303 bezout 600303 * SWAP 127337 * + -> 18191 }T
T{ 127337 600303 bezout-gcd NIP NIP -> 18191 }T
T{ 127337 -600303 bezout-gcd NIP NIP -> -18191 }T
T{ -127337 600303 bezout-gcd NIP NIP -> 18191 }T
T{ -127337 -600303 bezout-gcd NIP NIP -> -18191 }T

cell 8 = [IF]

   \ tests only working with 64 bit cells
   T{ 9223372036854775783 123 gcd -> 1 }T
   T{ 9223372036854775783 123 bezout-gcd NIP NIP -> 1 }T
   
   \ 2174616171290963396
   
[THEN]

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth gcd-test.fs -e bye"
   End:
[THEN]

