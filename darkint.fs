\ -*-forth-*-
\ Dark integers a.k.a. bignum arithmetic.
\ This implements simple, slow, fixed-size big unsigned integers.
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA posteo TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: May 2021

\ Note: little endian storage
require ./ostack2.fs
require ./misc.fs

1 CELLS 4 = [IF] 2 [ELSE] 1 CELLS 8 = [IF] 3 [ELSE] ABORT [THEN] [THEN]
CONSTANT cellshift

160 CONSTANT #darkint  \ bignum size in bytes
                        \ needs to be multiple of CELL size!
#darkint cellshift RSHIFT CONSTANT cells/darkint

#darkint 16 *  make-ostack: bn-stack

bn-stack odepth: bn-depth
: bn-alloc  ( -- a-addr )
   #darkint  bn-stack (oalloc)   DUP #darkint ERASE ;
bn-stack osize: bn-size
bn-stack oaddr: bn-addr
bn-stack oresize: bn-resize
bn-stack otos: bn-tos
bn-stack onos: bn-nos
bn-stack odrop: bn-drop
bn-stack o2drop: bn-2drop
bn-stack o.s: bn-.s
bn-stack opush: bn-push
bn-stack opop: bn!
: bn@  ( a-addr --   o: -- u )  #darkint bn-push ;
bn-stack odup: bn-dup
bn-stack oover: bn-over
bn-stack onip: bn-nip
bn-stack oswap: bn-swap
bn-stack oconcat: bn-concat
bn-stack oliteral: bn-literal

: bn-tuck  bn-swap bn-over ;

#darkint CELL+ NEGATE CONSTANT #bn-tos>nos
#darkint CELL+ CONSTANT #bn-nos>tos

: bn0  ( o: 0 )  bn-alloc DROP ;
: u>bn  ( u --  o: -- u )  \ move single-cell unsigned from stack to bn-stack
   bn-alloc ! ;
: bn>u  ( -- u  o: u -- )  \ convert darkint to single-cell unsigned
   bn-tos drop @  bn-drop ;
: ud>bn  ( du -- o: -- u )  \ move double-cell unsigned to bn-stack
   \ note how 2! operates big-endian, whereas darkint stack is little endian.
   SWAP bn-alloc 2! ;
: k>bn  ( x1 .. xk k --  o: -- u )
   bn-alloc  -1 ROT 1- -DO    ( s: a-addr )
      TUCK I CELLS + !  \ Bit order from TOS -> NOS is MSB -> LSB!
   1 -LOOP DROP ;
: (bn0=)  ( o: u1 --   s: flag )
   bn-tos bounds DO
      I @ IF FALSE UNLOOP EXIT THEN
   CELL +LOOP
   TRUE ;
: bn0=  ( o: u1 --   s: flag )
   (bn0=) bn-drop ;
: bn=  ( o:  u1 u2 -- S: -- flag )
   bn-tos bounds DO
      I @  I #bn-tos>nos + @ <> IF  bn-2drop FALSE UNLOOP EXIT THEN
   CELL +LOOP
   bn-2drop TRUE ; 
: (bn+)  ( o:  u1 u2 -- u1+u2 u2 )
   bn-nos bounds   0 -ROT  DO   ( s: n-carry1 )
      I @ 0 ROT  M+       ( s: ud-tos+carry)
      I #bn-nos>tos + @ 0 D+   ( s: ud-sum )
      SWAP I !  ( s: n-carry2 )
   CELL +LOOP
   DROP ;
: bn+  ( o:  u1 u2 -- u3 )  (bn+) bn-drop ;
: bn-  ( o:  u1 u2 -- u3 )
   bn-nos bounds   0 -ROT  DO   ( s: n-borrow1 )
      I @ 0 ROT  M+       ( s: ud-tos+borrow )
      I #bn-nos>tos + @ 0 D-   ( s: ud-sum )
      SWAP I !  ( s: n-borrow2 )
   CELL +LOOP
   DROP bn-drop ;
: bn+u  ( o: u1 -- u3   s: u2 ) \ add single cell to darkint
   bn-tos bounds  DO
      0  I @ 0  D+  SWAP I !
      ?DUP-0=-IF  UNLOOP EXIT THEN
   CELL +LOOP  DROP ;
: bn*u  ( o: u1 -- u3   s: u2 ) \ multiply darkint with single-cell integer
   0  bn-tos bounds  DO   ( s: u2 carry  )
      OVER 0 SWAP  I @ um*  D+  SWAP I !  
   CELL +LOOP  2DROP ;
: bn2*  ( o: u1 -- o2 ) \ multiply darkint with 2
   0  bn-tos bounds  DO   ( s: carry  )
      I @ 0 d2* -ROT OR I !
   CELL +LOOP  DROP ;
: (bn-lshift)  ( o: u1 -- u2 o2 s: u3 )
   \ shift limited to single-cell shift amount, shift by 0 allowed.
   ?DUP-0=-IF  EXIT THEN  \ special case for shift by 0.
   0  bn-tos bounds  DO   ( s: amount carry  )
      OVER I @ SWAP mlshift -ROT OR I !
   CELL +LOOP  2DROP ;
: (bn-rshift)  ( o: u1 -- u2 o2  s: u3 )
   \ Shift limited to single-cell shift amount.  Shift by 0 allowed.
   ?DUP-0=-IF  EXIT THEN  \ special case for shift by 0.   
   0  bn-tos -bounds  -DO   ( s: amount carry  )
      OVER 0 I @ ROT drshift  ROT OR I !
   CELL -LOOP  2DROP ;
: bn-lshift-limbs  ( o: u1 -- u2   s: u3 )
   \ shift left coarsly by a number of cells
   CELLS  #darkint CELLS MIN >R
   bn-tos OVER SWAP R@ /STRING MOVE    \ move limbs
   bn-tos DROP  R> ERASE ;             \ set low limbs zero
: bn-rshift-limbs  ( o: u1 -- u2   s: u3 )
   \ shift right coarsly by a number of cells
   CELLS  #darkint CELLS MIN >R
   bn-tos OVER SWAP R@ /STRING  >R SWAP R> MOVE         \ move limbs
   bn-tos +   0  R> NEGATE /STRING ERASE ;              \ set high limbs zero
: bn-lshift  ( o: u1 -- u2  s: u3)
   #bits/cell /MOD bn-lshift-limbs  (bn-lshift) ;
: bn-bitset ( o: u1 -- u2   s: u3 ) \ set bit #u3 in u1
   bn-tos  1 CELLS -   ROT #bits/cell /MOD   ( s: a-addr #max #bit #cell )
   1 ROT LSHIFT >R   CELLS MIN +   R> SWAP  OR! ;
   
: bn-invert  ( o: u1 -- u2 )
   bn-tos bounds  DO   I @ INVERT I !  CELL +LOOP ;
: bn-negate  ( o: u1 -- u2 )  bn-invert 1 bn+u ;
: (bn-compare)  ( o:  u1 u2 -- u1 u2 S: -- -n | 0 | +n )
   bn-tos -bounds -DO
      I #bn-tos>nos + @  I @ - ?DUP-IF  UNLOOP EXIT THEN
   CELL -LOOP
   0 ;
: bn-compare  ( o:  u1 u2 -- S: -- -n | 0 | +n )
   (bn-compare)  bn-2drop ;
: bn>  ( o: u1 u2 -- s: -- flag )  bn-compare 0> ;
: bn<  ( o: u1 u2 -- s: -- flag )  bn-compare 0< ;
\ : bn=  ( o: u1 u2 -- s: -- flag )  bn-compare 0= ;  \ fwd iter above faster.

: bn#msb ( o: u1 --  u1  s:  -- u2 )  \ return index of most significant bit
   #darkint 8 *                  ( s: u2 )
   bn-tos -bounds -DO   
      #bits/cell - 
      I @ ?DUP-IF
         #msb + UNLOOP EXIT
      THEN
   CELL -LOOP
   ( naturally returns zero when input is zero) ;

: (bn/mod)  ( o: u1 u2 -- u3   s: n )
   \ single division step:  return u3 = u1 - 2^n * u2   and N
   \ for maximum N that does not cause subtraction underflow.
   \ When u2>u1 return n=-1 and  u3=u2
   bn-swap bn#msb bn-swap bn#msb   -  { deltamsb }
   deltamsb 0< IF                               \ U1<U2 -> N=-1
      bn-drop -1 EXIT
   THEN
   deltamsb bn-lshift (bn-compare) 0< 0= IF     \ u1 >= u2*2^delta -> N=delta
      bn-   deltamsb EXIT
   THEN   
   deltamsb 0> IF                               \ u2 >= u2*2^{delta-1}
      1 (bn-rshift) bn- deltamsb 1- EXIT
   THEN  
   bn-drop -1 ;                                 \ U1<U2 -> N=-1
: bn/mod  ( o: u1 u2 -- u3 u4 )
   \ darkint division: u4 is quotient, u3 is remainder
   \ ABORTs when division by zero is attempted.
   (bn0=) ABORT" darkint division by zero"
   -1  \ mark bottom of bag of quotient bits
   BEGIN bn-tuck (bn/mod) DUP 0< 0= WHILE  \ compute remainder
         bn-swap                           \ collect quotient bits on the stack
   REPEAT
   bn-nip DROP   \ compute quotient by assembling the bits
   bn0 BEGIN DUP 0< 0= WHILE bn-bitset REPEAT  DROP ;

: .bn  ( o: u -- )
   \ todo: this fails depending on endianess.  port this to using a
   \ loop of shifts!
   CR ." dark:[ "
   bn-tos DROP bn#msb 3 RSHIFT 1+ -cbounds -DO
      BASE @ HEX  I C@  0 <# # # #> TYPE SPACE  BASE !
   1 -LOOP
   ." ]"  bn-drop ;


\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth darkint-test.fs -e bye"
   End:
[THEN]
