\ -*-forth-*-
\ Test sorting algorithms
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./sort.fs }T
T{ require ./misc.fs }T

INTEGER , HERE 2 , 7 , 23 , 5 , 11 ,  CONSTANT v{

T{ 5 v{ }sort< -> }T
T{ v{ @+ @+ @+ @+ @+ DROP -> 2 5 7 11 23 }T
T{ 5 v{ }sort> -> }T
T{ v{ @+ @+ @+ @+ @+ DROP -> 23 11 7 5 2 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth sort-test.fs -e bye"
   End:
[THEN]
