\ -*-forth-*-
\ Gaussion matrix inversion for matrices with elments from finite field
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014


require ./complex.fs
require ./matrix.fs
require ./remt.fs
require ./module.fs

module rgauss
<private>

: }}find-pivot  { k n m{{ -- j ) 
   \ search pivot for row k in nxn matrix.  Return pivot row J.  Caller should
   \ then perform row-swapping of K and J
   n k ?DO
      M{{ I k }} @ IF
	 I UNLOOP EXIT
      THEN
   LOOP
   -1 ;  \ no pivot found (matrix not invertible)

<public>

: }}rinv { n m{{ -- det }
   \ In-place inversion of nxn matrix using gauss-jordan algorithm.  Also
   \ return determinant of original matrix m{{.
   INTEGER , HERE { pivotidx{ } \ array holding our pivots (filled via , )
   1 { det }
   n 0 ?DO
      I n m{{ }}find-pivot DUP 0< ABORT" }}rinv: singular matrix"
      DUP ,  n m{{ ROT I }}swap-rows
      m{{ I I }} @		\ fetch diagonal element
      DUP det r* TO det		\  .. use it to update det
      rinv { pivot }		\  .. invert and remember in PIVOT
      n 0 ?DO
	 I J <> IF  \ for all other rows/columns
	    \ row-multiply-and-add transform 
	    M{{ I J }} DUP @ rnegate pivot r*    ( s: addr-ij factor )
	    DUP n M{{ J I }}rrowadd   \ row transform (needlessly includes I,J)
	    SWAP !                    \ store factor as result overwriting I,J
	 THEN
      LOOP
      
      \ now multiply row with (inverted) pivot (old value of row used above)
      \ Pivot itself is needlessly written here but overwritten thereafter
      pivot n M{{ I }}rrowmul
      pivot M{{ I I }} !		\ ovewrite pivot with inverse
   LOOP
   
   \ for every row-permutation performed on M we now have to do the
   \ corresponding column-permutation on the inverse.  Also fix sign here.
   n 0 ?DO
      I pivotidx{ I } @ 2DUP <> IF
	 n M{{ 2SWAP }}swap-cols
	 det rnegate TO det
      ELSE 2DROP THEN
   LOOP

   pivotidx{ }unallot
   det ;

end-module   

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth rgauss-test.fs -e bye"
   End:
[THEN]

