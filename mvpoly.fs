\ -*-forth-*-
\ Multi-variate polynomial arithmetic.
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA posteo TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: July 2021

require ./ostack2.fs
require ./misc.fs
require ./misc.fs

128 CONSTANT #mvp-vars  \ number of variables

\ Layout of a polynomial term
\ (Polynomials are variable-length arrays of 0..n terms)
2 2 2CONSTANT int16%
struct
   cell%              field mvp-coefficient
   int16% #mvp-vars * field mvp-exponents
end-struct mvp-term%

int16% %size CONSTANT /mvp-exponent  \ number of bytes per exponent
mvp-term% %size  CONSTANT /mvp-term

/mvp-term 3145728 * make-ostack: mvp-stack

mvp-stack odepth: mvp-depth
mvp-stack oalloc: mvp-alloc
mvp-stack osize: mvp-size
mvp-stack oaddr: mvp-addr
mvp-stack oresize: mvp-resize
mvp-stack otos: mvp-tos
mvp-stack onos: mvp-nos
mvp-stack odrop: mvp-drop
mvp-stack o2drop: mvp-2drop
mvp-stack o.s: mvp-.s
mvp-stack opush: mvp-push
mvp-stack opop: mvp-pop
mvp-stack odup: mvp-dup
mvp-stack oover: mvp-over
mvp-stack onip: mvp-nip
mvp-stack oswap: mvp-swap
mvp-stack oconcat: mvp-concat
mvp-stack oliteral: mvp-literal


: #mvp-terms  ( -- +n  o: poly -- poly )  \ number of terms of TOS poly
   mvp-size /mvp-term / ;
: >mvp-term  ( n -- a-addr   o: poly -- poly )  \ get pointer to indexd term
   /mvp-term * mvp-addr + ;

: mvp-term*  { term1 term2 -- o: poly )
   \ compute product of two terms given by plain pointers and put result into
   \ single-term poly on top of poly stack.
   /mvp-term mvp-alloc  { product }
   term1 @  term2 @ M*  2DUP  OVER S>D D<> ABORT" coeff overflow"
   DROP product !
   #mvp-vars /mvp-exponent *  CELL+  CELL DO
      term1 I + sw@
      term2 I + sw@ +
      DUP -32768 32768 WITHIN 0= ABORT" exponent overflow"
      product I + w!
   /mvp-exponent +LOOP ;
: mvp-termdeg  ( term -- u )  \ degree of term
   0 SWAP
   /mvp-term  cell /STRING BOUNDS DO
      I sw@ +
   /mvp-exponent +LOOP ;
   
: mvp-term-cmp  { term1  term2 -- n }
   \ compare terms: return <0 if term1<term2, >0 if term1>term2, 0 if equal.
   \ compare by degree first (lesser degree means term comes first)
   \ then compare inverse alphabetically by exponents, i.e. highest exponent
   \ on same variable comes first, x1 comes before x2 etc.
   term1 mvp-termdeg  term2 mvp-termdeg - ?DUP-IF EXIT THEN
   #mvp-vars /mvp-exponent *  CELL+  CELL DO
      term2 I + sw@  
      term1 I + sw@  - ?DUP-IF UNLOOP EXIT THEN
   /mvp-exponent +LOOP
   0 ;
      
: mvp0  ( o:  -- 0 )   0 mvp-alloc DROP ;
: mvp1  ( o:  -- 1 ) 
   /mvp-term mvp-alloc DUP /mvp-term ERASE
   1 SWAP ! ;
   
: mvp-compress  ( o: poly1 -- poly2 ) \ remove zero terms
   mvp-tos BOUNDS   mvp0  ?DO  ( o: poly1 poly2 )
      I @ IF   \ todo: would be more efficient if could directly append
         I /mvp-term mvp-push mvp-concat
      THEN
   /mvp-term +LOOP
   mvp-nip ;

: mvp-sort  ( o: poly1 -- poly2 )
   \ sort polynomial terms in ascending order as defined by mvp-term-cmp
   \ This is sort of a quicksort algorithm.
   mvp-tos  { poly-addr poly-size }
   poly-size /mvp-term > 0= IF \ no need to sort if only 1 term or less
      EXIT
   THEN
   poly-addr poly-size /mvp-term /  2/ /mvp-term * +  { pivot }
   2 0 DO  \ I=0: select terms less than pivot and sort
           \    then append pivot
           \ I=1: select terms greater than pivot and sort
      mvp0 poly-addr poly-size BOUNDS ?DO
         I pivot mvp-term-cmp
         J IF NEGATE THEN   \ depending on iteration get lesser/higher terms
         0<  IF
            I /mvp-term mvp-push mvp-concat  \ select a term
         THEN
      /mvp-term +LOOP
      RECURSE
      I 0= IF  pivot /mvp-term mvp-push mvp-concat THEN
   LOOP
   mvp-concat  \ concat the two polynomials generated in the two iterations.
   mvp-nip ;   \ get rid of the input polynomial 
   
: mvp-term+  ( o: poly1 term -- poly2 )
   \ add single-term poly to NOS poly on poly stack
   mvp-size /mvp-term <> ABORT" not a single term"
   mvp-nos BOUNDS ?DO
      I /mvp-term CELL /STRING
      mvp-tos CELL /STRING  COMPARE 0= IF
         \ poly1 has term already, so just sum up the coefficients
         \ todo: gabage collect resulting zero coefficients!
         mvp-addr @ I +!
         mvp-drop
         UNLOOP EXIT
      THEN
   /mvp-term +LOOP
   \ no match: need to add new term
   mvp-concat ;
: mvp*n  ( n --  o: poly1 -- poly2 ) \ multiply polynomial with single-cell int
   mvp-tos BOUNDS ?DO
      DUP I @ *  I !
   /mvp-term +LOOP
   DROP ;
: mvp*  ( o: poly1 poly2 -- poly3 )
   \ multiply two polynomials
   mvp-nos mvp-tos  { d: nos  d: tos }
   mvp0  \ put the zero-polynomial on top to accumulate result
   nos BOUNDS ?DO
      tos BOUNDS ?DO
         I J mvp-term* mvp-term+ 
      /mvp-term +LOOP
   /mvp-term +LOOP
   mvp-nip mvp-nip mvp-compress ;   \ only keep TOS product.
: mvp-square  ( o: poly1 -- poly2 )   mvp-dup mvp* ;
: mvp**  ( exponent -- o: poly1 -- poly2 )
   mvp1 mvp-swap                 ( s: exponent   o: result  poly^{2^n} )
   BEGIN  DUP 0> WHILE
         DUP 1 AND IF
            mvp-swap   mvp-over  mvp*   mvp-swap
	 then
	 mvp-square 
	 2/
   REPEAT
   mvp-drop DROP ;
: mvp+ ( o: poly1 poly2 -- poly3 )
   mvp-nos BOUNDS ?DO
      I /mvp-term mvp-push   \ extract single term from NOS ..
      mvp-term+              \ and add to TOS
   /mvp-term +LOOP
   mvp-nip mvp-compress ;  \ only keep TOS poly
: mvp-compare  (  -- n  o: poly1 poly2 -- )
   mvp-sort  mvp-swap mvp-sort
   mvp-tos mvp-nos COMPARE 
   mvp-2drop ;
: mvp=  (  -- flag  o: poly1 poly2 -- )
   mvp-compare 0= ;
: mvp0<>  ( -- flag   o: poly -- )
   mvp-size 0<>  mvp-drop ;

: mvp-x_n^k  { n k -- o:  -- x_n }
   /mvp-term mvp-alloc  DUP  /mvp-term ERASE
   1 OVER !   \ coefficient=1
   k -32768 32767 WITHIN 0= ABORT"  exponent!"
   n #mvp-vars U< 0= ABORT" index!"
   n /mvp-exponent * + CELL+   k SWAP w! ; \ exponent=k ;
: mvp-x_n   1 mvp-x_n^k ;
: mvp-.coeff ( n -- ) \ print coefficient
   S>D 
   SWAP OVER DABS 
   <#   #S   ROT IF [char] - ELSE [char] + THEN HOLD #>  
   TYPE  SPACE ;
: mvp-.term  ( s: a-addr )  \ print single term
   DUP @ mvp-.coeff
   CELL+  #mvp-vars 0 DO    ( S: exponents )
      DUP I /mvp-exponent * + sw@ ?DUP-IF
         ." x"  I 0 .R
         DUP 1 > IF  ." ^" 0 .R ELSE DROP THEN
      THEN
   LOOP DROP ;
: (.mvp)  ( o: poly -- poly )
   mvp-size 0= IF  ." ( 0)" EXIT THEN
   ." ("
   mvp-tos BOUNDS ?DO
      SPACE I mvp-.term
   /mvp-term +LOOP
   ." )";
: .mvp   ( o: poly -- )  mvp-sort (.mvp) mvp-drop ;
: mvp@  ( $addr --   o: -- poly )
   ?DUP-IF $@ mvp-push EXIT THEN
   mvp0 ;   \ zero-pointer treated as empty polynomial !
      
: mvp!  ( $addr --   o: poly -- )   mvp-tos  ROT $! mvp-drop ;
: mvp,  ( $addr --   o: poly -- )   HERE 0 ,  mvp! ;
: mvp-maxidx  ( o: poly -- n )  \ highest index of variable
   -1   ( maxidx)
   mvp-tos BOUNDS ?DO
      #mvp-vars 0 ?DO
         J cell+ I /mvp-exponent * + sw@ IF   I MAX THEN
      LOOP
   /mvp-term +LOOP
   mvp-drop ;
: mvp-eval  ( values{ o: poly -- n )
   \ evaluate polynomial using values{ 0} .. for values of X_0..
   \ assumes values{ is a INTEGER array.
   0  SWAP  ( s: sum values{)
   mvp-tos BOUNDS ?DO
      1   ( s: sum values{ product)
      #mvp-vars 0 ?DO
         OVER I CELLS + @   ( s: sum values{ product value[I] )
         J CELL+  I /mvp-exponent * + sw@ ( s: sum values{ product value[I] exp)
         n** *
      LOOP
      I @ *   \ apply coefficient  ( s: sum values{ product)
      ROT + SWAP
   /mvp-term +LOOP
   DROP mvp-drop ;
   
: x_n:  CREATE ,  DOES> ( o : poly ) @ mvp-x_n ;
0 x_n: x0    1 x_n: x1   2 x_n: x2   3 x_n: x3   4 x_n: x4 
5 x_n: x5    6 x_n: x6   7 x_n: x7   8 x_n: x8   9 x_n: x9
10 x_n: x10 11 x_n: x11 12 x_n: x12 13 x_n: x13 14 x_n: x14


 
\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth -m 1G mvpoly-test.fs -e bye"
   End:
[THEN]

