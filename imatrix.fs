\ -*-forth-*-
\ Integer (and gaussian integer) matrix routines
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./matrix.fs
require ./complex.fs
require ./mod.fs

: }}iprint ( n m addr -- )  \ print nXm elements of a integer 2-D array
   -ROT SWAP 0 ?DO
      CR DUP 0 ?DO
         OVER J I  }} @ r>signed 5 .R
      LOOP
   LOOP  2DROP ;
: }}zprint ( n m addr -- )  \ print nXm elements of a complex 2-D array
   -ROT SWAP 0 ?DO
      DUP 0 ?DO
	 OVER J I  }} 2@ z.
      LOOP   CR
   LOOP  2DROP ;
: }zprint ( n addr -- )  \ print n elements of an complex array
   SWAP 0 DO I print-width @ MOD 0= I AND IF CR THEN
      DUP I } 2@ z. LOOP
   DROP ;
: }}ifill: ( n m addr -- )  \ fill integer matrix with data from input
   -ROT SWAP 0 ?DO
      REFILL 0= ABORT" end of file?"
      DUP 0 ?DO
	 OVER J I }}
	 parse-word s>number? 0= ABORT" number?"
	 DROP SWAP !
      LOOP
   LOOP
   2DROP ;
: iexchange ( a-addr1 a-addr2 ncells -- )
   \ exchange cell-wise data from given address ranges
   0 ?DO
      2dup 2dup @ swap @   ( a1 a2 a1 a2 i2 i1 )
      rot ! swap !
      cell+ swap cell+ swap
   LOOP
   2drop ;


\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth imatrix-test.fs -e bye"
   End:
[THEN]
