\ -*-forth-*-
\ test dual graph generation
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./dual.fs -> }T

\ define the planar graph from wiki/Dual_graph
5 VALUE #nodes
DOUBLE ,  HERE  0 2 2,  2 2 2,  3 1 2,  2 0 2,  0 0 2,
VALUE xy{  

DOUBLE ,  HERE  0 1 2,  1 2 2,  2 3 2,  1 3 2,  3 4 2,  4 0 2,  1 4 2,  
VALUE edges{  7 VALUE #edges

T{ #nodes #edges edges{ }edges>edgemap  VALUE edgemap{{ -> }T
T{ #nodes xy{ edges{ edgemap{{ }}sort-edgemap -> }T

\ run dual-graph
#edges DOUBLE MARRAY faces{  \ output array
0 VALUE mem1
0 VALUE #faces
CREATE mem0
T{ #nodes xy{ #edges edges{ edgemap{{ faces{ }dual-graph TO #faces
HERE TO mem1  -> }T
CR #edges faces{ }xyprint
T{ #faces -> 4 }T
T{ mem1 mem0 - -> 0 }T
T{ faces{ 0 } 2@ -> 1 0 }T   ( 0 is the outside face)
T{ faces{ 5 } 2@ -> 1 0 }T
T{ faces{ 6 } 2@ -> 1 3 }T

T{ 0 1 edges{ edgemap{{ faces{ }next-boundary-edge -> 5 }T
T{ 5 1 edges{ edgemap{{ faces{ }next-boundary-edge -> 6 }T
T{ 4 3 edges{ edgemap{{ faces{ }next-boundary-edge -> 3 }T
T{ 1 2 edges{ edgemap{{ faces{ }next-boundary-edge -> 3 1negate }T

\ area of first inner face
T{ 0 1 xy{ edges{ edgemap{{ faces{ }face-area -> -4. }T
T{ 0 1negate 1 xy{ edges{ edgemap{{ faces{ }face-area -> -4. }T

\ area of the outer face
T{ 0 0 xy{ edges{ edgemap{{ faces{ }face-area -> 8. 2. D+ }T
T{ 0 1negate 0 xy{ edges{ edgemap{{ faces{ }face-area -> 8. 2. D+ }T

\ locating the outer face
#faces #edges faces{ }edges>edgemap  VALUE faceedgemap{{
T{ #faces #edges xy{ edges{ edgemap{{ faces{ faceedgemap{{ }outer-face -> 0 }T

( **** Verify that things work on pathological "tree-shaped" graphs ***** )
\ The problematic C-shaped graph that used to cause infinite loop
4 TO #nodes
DOUBLE ,  HERE  0 2 2,  0 4 2,  2 2 2,  2 4 2,  TO xy{  
3 TO #edges
DOUBLE ,  HERE  0 1 2,  0 2 2,  1 3 2, TO edges{

T{ #nodes #edges edges{ }edges>edgemap  TO edgemap{{ -> }T
T{ #nodes xy{ edges{ edgemap{{ }}sort-edgemap -> }T

CR .( edgemap :)
#nodes 0 [DO]
   CR [I] . edgemap{{ [I] } SPACE 2@ DUP . 0 [DO]
      DUP [I] } @ DUP 1abs . 1sign . SPACE [LOOP] DROP
[LOOP]

CR .( dual-graph for pathological graph )
T{ #nodes xy{ #edges edges{ edgemap{{ faces{ }dual-graph TO #faces -> }T

CR .( faces:)
#edges 0 [DO]
   CR faces{ [I] } 2@ . . 
[LOOP]

T{ #faces #edges faces{ }edges>edgemap  VALUE faceedgemap{{ -> }T

CR .( faceedgemap: )
#faces 0 [DO]
   CR [I] . faceedgemap{{ [I] } SPACE 2@ DUP . 0 [DO]
      DUP [I] } @ DUP 1abs . 1sign . SPACE [LOOP] DROP
[LOOP]
CR .( .)

T{ #faces #edges xy{ edges{ edgemap{{ faces{ faceedgemap{{ }outer-face -> 0 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth dual-test.fs -e bye"
   End:
[THEN]
