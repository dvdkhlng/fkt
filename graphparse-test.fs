\ -*-forth-*-
\ test graph description parser
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Oct 2012

3 assert-level !

require ./tester2.fs
T{ require ./graphparse.fs -> }T

T{ s" ./graphparse-test.graph" parse-graph end-zdraw 2DROP DROP -> 10 14 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth graphparse-test.fs -e bye"
   End:
[THEN]
