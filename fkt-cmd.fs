\ -*-forth-*-
\ FKT-Algorithm command line interface
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Nov 2012

warnings off

require ./graphparse.fs
require ./fkt.fs
require string.fs

: .help  ( -- )
   CR ." USAGE:"
   CR
   CR ." fkt [options] INPUT.GRAPH"
   CR
   CR ." OPTIONS:"
   CR ." -modulus N"
   CR ." -complex"
   CR ." -postscript PSFILE"
   CR ." -nocompute"
   CR
   CR ." Run FKT algorithm on graph described in INPUT.GRAPH"
   CR ." See the man-page for more information and/or send your questions"
   CR ." and suggestions to fkt-discuss@lists.sourceforge.net (note: requires"
   CR ." subscription)
   CR ;

VARIABLE graphfile
VARIABLE complex?
VARIABLE nocompute?
VARIABLE psfile

WORDLIST CONSTANT options
GET-CURRENT  options SET-CURRENT
: -modulus  ( -- true )
   1 arg s>unumber?
   0= ABORT" argument to -modulus is not a valid unsigned number"
   2DUP -1 1 RSHIFT 0 DU> ABORT" modulus too large"
   D>S modulus !
   shift-args
   true ;
: -complex  ( -- true )
   complex? on
   true ;
: -postscript  ( -- true )
   1 arg psfile $!
   shift-args
   true ;
: -nocompute  ( -- true )
   nocompute? on true ;
: --  ( -- false )  \ ends option processing
   false ;

SET-CURRENT

0 VALUE xy{  0 VALUE edges{  0 VALUE weights{
0 VALUE #edges  0 VALUE #nodes

: parse-options  ( -- )  \ parse options starting at '0 arg'
   \ stop otion processing as soon as an unknow option is encountered
   BEGIN argc @ 1 > WHILE
	 1 arg
	 OVER C@ [CHAR] - <> IF  2DROP EXIT THEN
	 options search-wordlist 0= ABORT" unknown option"
	 shift-args
	 EXECUTE 0= IF EXIT THEN
   REPEAT ;
      
: parse-args  ( -- )
   try
      parse-options
      argc @ 2 < ABORT" No graphfile given on command line."
      argc @ 2 > ABORT" Too many non-option arguments."
      1 arg 2DUP graphfile $!
   endtry-iferror
      CASE
	 -2 OF
	    ." ERROR: " "error @ COUNT TYPE CR
	    ['] .help stdout outfile-execute
	    1 (bye)
	 ENDOF
	 throw \ pass other errors on
      ENDCASE
   then ;

2VARIABLE #zmatchings

: parse-graph  ( -- )
   try
      graphfile $@ parse-graph  \ already prints parser errors to stderr
   endtry-iferror
      DUP #parser-error = IF 1 (bye) THEN
      THROW  \ parse other errors on
   then
   complex? @ IF end-zdraw ELSE end-draw THEN
   TO weights{ TO edges{ TO xy{ TO #edges TO #nodes ;
: postscript  ( -- )
   psfile @ ?DUP-0=-IF EXIT THEN
   ." Printing graph to postscript file '" psfile $@ TYPE ." '." CR
   #nodes #edges xy{ edges{ weights{   psfile $@  print-drawing ;
: compute  ( -- )
   nocompute? @ IF EXIT THEN
   #nodes #edges xy{ edges{ weights{ 
   complex? @ IF }#zweighted-matchings
   ELSE }#weighted-matchings 0 THEN
   #zmatchings 2! ;
: .result  ( -- )
   ." #perfmatch= "
   #zmatchings 2@  SWAP .
   complex? @ IF ." + " . ." i " ELSE DROP THEN
   ." (mod " modulus @ 0 .R ." )" CR ;

' parse-args stderr outfile-execute
parse-graph
postscript
compute
.result
BYE

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth fkt-cmd.fs -modulus 5 graphparse-test.graph"
   End:
[THEN]

