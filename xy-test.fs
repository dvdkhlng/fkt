\ -*-forth-*-
\ test 2-d vectors (pairs) of integers
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./xy.fs -> }T

T{ 5 6  1 2 xy+ -> 6 8 }T
T{ 5 6  1 2 xy- -> 4 4 }T
T{ 5 6  1 2 xy* -> 17. }T
T{ 1 2 r90ccw -> -2 1 }T
T{ 5 6  1 2 xycollinear? -> false }T
T{ 5 6  200 240 xycollinear? -> true }T
T{ 5 6  -200 240 xycollinear? -> true }T
T{ 5 6  1 2 xya< -> true }T
T{ 1 2  5 6 xya< -> false }T
T{ 0 1  0 -1 xya< -> false }T
T{ 0 1000  -1 -1000 xya< -> true }T
T{ 0 1000   1 -1000 xya< -> false }T
T{ 0 1  0 1 xya< -> false }T
T{ 0 1  0 1 xya<= -> true }T

T{ 1 0 0 1 -1 0   xyccw? -> true }T
T{ 1 0 0 -1 -1 0   xyccw? -> false }T

T{ 10 0 -10 0 10 -1  xyccw? -> true }T
T{ 10 0 -10 0 -10 -1  xyccw? -> true }T
T{ 10 0 -10 -1 -10 -2  xyccw? -> true }T
T{ 10 0 -10 -1 10 -1  xyccw? -> true }T
T{ 10 0 10 -2 10 -1  xyccw? -> true }T

T{ 10 -1 10 0 -10 0  xyccw? -> true }T
T{ -10 -1 10 0 -10 0  xyccw? -> true }T
T{ -10 -2 10 0 -10 -1  xyccw? -> true }T
T{ 10 -1 10 0 -10 -1  xyccw? -> true }T
T{ 10 -1 10 0 10 -2  xyccw? -> true }T

T{ -10 0 10 -1 10 0  xyccw? -> true }T
T{ -10 0 -10 -1 10 0  xyccw? -> true }T
T{ -10 -1 -10 -2 10 0  xyccw? -> true }T
T{ -10 -1 10 -1 10 0  xyccw? -> true }T
T{ 10 -2 10 -1 10 0  xyccw? -> true }T


T{ -10 0 10 0 10 -1  xyccw? -> false }T
T{ -10 0 10 0 -10 -1  xyccw? -> false }T
T{ -10 -1 10 0 -10 -2  xyccw? -> false }T
T{ -10 -1 10 0 10 -1  xyccw? -> false }T
T{ 10 -2 10 0 10 -1  xyccw? -> false }T

T{ 10 0 10 -1 -10 0  xyccw? -> false }T
T{ 10 0 -10 -1 -10 0  xyccw? -> false }T
T{ 10 0 -10 -2 -10 -1  xyccw? -> false }T
T{ 10 0 10 -1 -10 -1  xyccw? -> false }T
T{ 10 0 10 -1 10 -2  xyccw? -> false }T

T{ 10 -1 -10 0 10 0  xyccw? -> false }T
T{ -10 -1 -10 0 10 0  xyccw? -> false }T
T{ -10 -2 -10 -1 10 0  xyccw? -> false }T
T{ 10 -1 -10 -1 10 0  xyccw? -> false }T
T{ 10 -1 10 -2 10 0  xyccw? -> false }T

T{ 128 DOUBLE MARRAY v{ -> }T
T{ 128 INTEGER MARRAY idx{ -> }T

1  -1 v{ 0 } 2!
-1  1 v{ 1 } 2!
-1 -1 v{ 2 } 2!
2   1 v{ 3 } 2!

CR 4 v{ }xyprint
T{ 4 v{ }xysort-ccw -> }T
CR 4 v{ }xyprint

: (xy-testsort)  { n -- flag }
   \ fill array, must be careful to not produce collinear or zero-vectors
   n 0 DO
      I 1+ 197 * 139  MOD DUP DUP *  1001 - SWAP 113 -   v{ I } 2!
   LOOP
   \ CR n v{ }xyprint
   n v{ }xysort-ccw
   \ CR n v{ }xyprint
   n v{ }xysorted-ccw? ;

: (xy-testsort')  { n -- flag }
   \ fill array, must be careful to not produce collinear or zero-vectors
   n 0 DO
      I 1+ 197 * 139  MOD DUP DUP *  1001 - SWAP 113 -   v{ I } 2!
      I idx{ I } !
   LOOP
   \   CR n v{ }xyprint
   n idx{ v{ }xysort-ccw'
   \   n idx{ }iprint
   n idx{ v{ }xysorted-ccw?' ;

T{ 2 (xy-testsort) -> true }T
T{ 6 (xy-testsort) -> true }T
T{ 100 (xy-testsort) -> true }T
T{ 101 (xy-testsort) -> true }T
T{ 117 (xy-testsort) -> true }T

T{ 2 (xy-testsort') -> true }T
T{ 6 (xy-testsort') -> true }T
T{ 100 (xy-testsort') -> true }T
T{ 101 (xy-testsort') -> true }T
T{ 117 (xy-testsort') -> true }T

\ area 
5 CONSTANT #vertices
DOUBLE , HERE  1 1 2,  1 5 2,  3 7 2,  5 5 2,  5 1 2,
CONSTANT vertices{
T{ #vertices vertices{ xy-area*2 -> 40. }T  ( 2*{16 + 2 + 2} )

5 CONSTANT #vertices
DOUBLE , HERE  1 1 2,  3 1 2,  3 3 2,  2 4 2,  1 3 2,  
CONSTANT vertices{
T{ #vertices vertices{ xy-area*2 -> -10. }T  ( 2*{4 + 1/2 + 1/2} )

T{ 0 3 1 2 xy-integral*2 -> 5. }T

5 CONSTANT #vertices
DOUBLE , HERE  0 3 2,  1 2 2,  1 3 2,  1 4 2,  0 4 2,  
CONSTANT vertices{
T{ #vertices vertices{ xy-area*2 -> -3. }T  ( 2*{4 + 1/2 + 1/2} )

T{ 0 0  2 0  1 1  1 -1  xy-intersect? -> true }T
T{ 0 0  2 2  0 2  2 0  xy-intersect? -> true }T
T{ 0 0  2 0  3 1  3 -1  xy-intersect? -> false }T
T{ 0 0  2 2  0 2  10 3  xy-intersect? -> false }T
T{ 0 0  2 2  0 2  10 1  xy-intersect? -> true }T
T{ 0 0  2 2  1 0  3 2  xy-intersect? -> false }T
T{ 0 0  2 2  1 0  2 3  xy-intersect? -> true }T
T{ 1 0  3 2  0 0  2 2  xy-intersect? -> false }T
T{ 1 0  2 3  0 0  2 2  xy-intersect? -> true }T
T{ 0 0  1 0  2 0  3 0  xy-intersect? -> false }T
T{ 1 0  0 0  2 0  3 0  xy-intersect? -> false }T
T{ 1 0  0 0  3 0  2 0  xy-intersect? -> false }T


\ T{ 0 0  2 0  1 0  -1 1  xy-intersect? -> true }T
\ T{ 0 0  2 0  1 0  1 1  xy-intersect? -> true }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth xy-test.fs -e bye"
   End:
[THEN]
