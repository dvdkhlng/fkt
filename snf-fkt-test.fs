\ -*-forth-*-
\ Test Smith-Normal-Form computation of FKT determinants
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./tester2.fs
require ./imatrix.fs
require ./snf.fs
require ./fkt.fs
require ./inverse-fkt.fs

0 VALUE #nodes
0 VALUE matrix{{

T{ 3876 DUP * 5 M* inverse-fkt 3 PICK TO #nodes
FALSE fkt-edges>matrix TO matrix{{
#nodes #nodes matrix{{ }}left-snf  
-> }T

\ #nodes #nodes matrix{{ }}iprint

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth snf-fkt-test.fs -e bye"
   End:
[THEN]
