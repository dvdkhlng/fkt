\ -*-forth-*-
\ Modular arithmetic routines test-case
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

\ 3 assert-level !

require ./tester2.fs
T{ require ./mod.fs -> }T

7 modulus !
VARIABLE a  

T{ 1 6 r+  -> 0  }T
T{ 0 0 r+  -> 0  }T
T{ 1 6 r-  -> 2  }T
T{ 0 0 r-  -> 0 }T
T{ 1 a !  6 a r+!  a @ -> 0  }T
T{ 1 a !  2 a r-!  a @ -> 6  }T
T{ 4 6 r*  -> 3  }T
T{ 1 1 r**  -> 1  }T
T{ 2 0 r**  -> 1  }T
T{ 2 1 r**  -> 2  }T
T{ 2 2 r**  -> 4  }T
T{ 2 3 r**  -> 1  }T
T{ 1 rinv  -> 1  }T
T{ 3 rinv  -> 5  }T
T{ 3 dup rinv r* -> 1  }T
T{ 4 dup rinv r* -> 1  }T
T{ 0 rnegate -> 0 }T
T{ 4 rnegate -> 3 }T

max-modulus modulus !
T{ 123 rnegate 123 r+ -> 0 }T
T{ 1 rinv -> 1 }T
T{ 123 rinv  123 r* -> 1 }T

T{ 1 rinv-nonrec -> 1 }T
T{ 123 rinv-nonrec  123 r* -> 1 }T


\ Benchmarking code.  Disabled to conserve time 
1 [IF]
   : btime ( -- d )  cputime 2DROP ;
   : measure  ( xt n -- d )
      btime 2SWAP
      0 ?DO  DUP EXECUTE LOOP DROP
      btime  2SWAP D-  ;
   CR .( rinv-loop: )
   VARIABLE rinvval
   12345678 rinvval !
   :NONAME rinvval @ rinv-nonrec DROP  1 rinvval +! ;
   1000000 measure D. .(  us/1e6)
   CR .( rinv-bezout: )
   12345678 rinvval !
   :NONAME rinvval @ rinv DROP  1 rinvval +! ;  1000000 measure D. .(  us/1e6)
[THEN]

exit-test



\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth mod-test.fs -e bye"
   End:
[THEN]
