\ -*-forth-*-
\ compute spanning tree
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./graph.fs
require ./module.fs

module spanning

\ local variables during recursion.  pulled out to reduce locals stack usage
<private>
0 VALUE adjmap{{
0 VALUE seen?{
<public>

: (spanning-tree) { node -- }
   \ recursive tree scanning procedure
   adjmap{{ node } 2@	\ iterate over neighbours of node
   0 ?DO		( s: neighbours{ )
      DUP I } @	1abs	( s: neighbours{ neighbour )
      seen?{ OVER } DUP C@ 0= IF
	 TRUE SWAP C!
	 node OVER 2,		\ record edge
	 RECURSE
      ELSE  2DROP THEN
   LOOP
   DROP ;

\ public interface follows

: spanning-tree  ( adjmap{{ #nodes -- edges{ #edges )
   \ note: allocates slightly more memory than required.  seen{} array is
   \ kept.  simplifies things, and doesn't hurt too much.
   1 CHARS , HERE DUP ROT CHARS DUP ALLOT ERASE  TO seen?{  ALIGN
   TO adjmap{{
   ALIGN DOUBLE , HERE >R		( s: adjmap{{ seen?{  r: edges{ )
   TRUE seen?{ C!			\ mark start node 0  as seen
   0 (spanning-tree)			\ recursively compute tree
   R> HERE OVER - DOUBLE / ;		\ return array, compute size

: }spanning-path  { #idxs idxs{ edges{ -- idxs2{ }
   \ very special case: given an unsorted array of edge-idxs that form a path
   \ (NOT tree!), return an array of edge-ixs in traversal order, starting
   \ with the first edge.  sign marks on edge idxs are ignored (1abs), but
   \ still copied to output.
   \ note: this visits any edge once, but may visit nodes twice, which might
   \ not even be bad: for cycles, we sorts the cycle's edges without dropping
   \ any.
   \ note: leaves some undeallocatable memory before idxs2{}
   1 CHARS , HERE DUP  #idxs CHARS DUP ALLOT  ERASE	{ seen?{ }
   ALIGN INTEGER , HERE				( s: idxs2{ )
   idxs{ 0 } @ DUP ,				\ record first edge
   edges{ SWAP 1abs } @				( s: idxs2{ node )
   #idxs 1 ?DO	 \ search remaining #idxs-1 edges
      \ todo: could be more economical to generate an adjmap{} for our edges?
      #idxs 1 ?DO   \ search the next edge, the one that starts on 'node'
	 seen?{ I } C@ 0= IF
	    edges{ idxs{ I } @ 1abs } 2@ 
	    3DUP on-edge? IF	\ edge touches node?
	       other-node	\ next edge will start on edges' end node
	       idxs{ I } @ ,	\ record it
	       TRUE seen?{ I } C!
	       LEAVE		
	    ELSE 2DROP THEN
	 THEN
	 assert( I #idxs 1- <> )  \ oops, no edge found?
      LOOP
   LOOP						( s: idxs2{ node )
   assert( DUP edges{ idxs{ 0 } @ } 2@ on-edge? )  \ not a cycle? ??required??
   DROP ;
      
      


end-module

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth spanning-test.fs -e bye"
   End:
[THEN]
