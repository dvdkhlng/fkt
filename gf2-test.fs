\ -*-forth-*-
\ Test Galois Fields arithmetic
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2012

require ./tester2.fs
T{ require ./gf2.fs -> }T

T{ $10 $10 gf2>= -> true }T
T{ $10 $8f gf2>= -> false }T
T{ $10 $f gf2>= -> true }T
T{ $10 $1 gf2>= -> true }T
T{ $10 $20 gf2>= -> false }T

T{ 3 2 (gf2*) -> 6 }T
T{ 3 3 (gf2*) -> 5 }T

T{ 2 7 (gf2/mod) -> 0 2 }T

\ corresponds to pm3.fs and wikipedia examples
7 TO gf2-polynomial
T{ 1 1 gf2* -> 1 }T
T{ 2 1 gf2* -> 2 }T
T{ 2 2 gf2* -> 3 }T
T{ 3 3 gf2* -> 2 }T

\ corresponds to rijndael's finite field arithmitc
$11b TO gf2-polynomial
T{ $53 $CA gf2* -> $01 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth gf2-test.fs -e bye"
   End:
[THEN]