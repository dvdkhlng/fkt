\ -*-forth-*-
\ Gaussion matrix inversion test-case
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014

require ./tester2.fs
require ./imatrix.fs
T{ require ./rgauss.fs -> }T

7 MODULUS !

0 VALUE tt{{

3 , INTEGER , HERE TO tt{{
2 ,  0 ,  0 ,
0 ,  3 ,  0 ,
0 ,  0 ,  5 ,
T{ 3 tt{{ }}rinv -> 2 3 r* 5 r* }T
CR 3 3 TT{{ }}IPRINT
T{ tt{{ 1 1 }} @ -> 3 rinv }T
T{ tt{{ 2 2 }} @ -> 5 rinv }T

127 modulus !
3 , INTEGER , HERE TO tt{{
0 ,  8 ,  0 ,
5 ,  4 ,  2 ,
1 ,  3 ,  5 ,
T{ 3 tt{{ }}rinv -> 70 }T
CR 3 3 tt{{ }}iprint
T{ 3 tt{{ }}rinv -> 70 rinv }T
T{ tt{{ 0 0 }} @+ @+ @+  @+ @+ @+  @+ @+ @ -> 0 8 0  5 4 2  1 3 5 }T
CR 3 3 tt{{ }}iprint

3 , INTEGER , HERE TO tt{{
0 ,  0 ,  1 ,
0 ,  2 ,  0 ,
3 ,  0 ,  0 ,
T{ 3 tt{{ }}rinv -> 3 2 r* 1 r* rnegate }T
T{ 3 tt{{ }}rinv -> 3 2 r* 1 r* rnegate rinv }T
T{ tt{{ 0 0 }} @+ @+ @+  @+ @+ @+  @+ @+ @ -> 0 0 1  0 2 0  3 0 0 }T
CR 3 3 tt{{ }}iprint

\ todo: add some more tests on larger matrices.
\ todo: verify matrix outputs (also pin intermediate results?)

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth rgauss-test.fs -e bye"
   End:
[THEN]
