\ -*-forth-*-
\ Handling netlists
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./module.fs
require ./matrix.fs CR
require ./graph.fs

module netlist-module

<private>
VARIABLE (gates
VARIABLE #gates

<public>

: netlist  ( -- )
   0 (gates !  0 #gates ! ;
: 2gate  ( n1 n2 n3 -- n4 )
   \ gates number from 0.  -1 reserved for undefined inputs/outputs
   (gates linked , 2 , swap , ,
   #gates DUP @  1 ROT +! ;
: gate-inputs  ( a-addr -- a-addr2 n )
   2 CELLS + DUP @ SWAP CELL+ SWAP ;

: flush-gate-types  ( -- #undefined gates{ )
   \ Write array of gates-types.  Also allocate pseudo-gates for undefined
   \ values setting their type to -1.  These get indices starting with
   \ #gates@.
   INTEGER , HERE #gates @ CELLS ALLOT	{ gates{ }
   (gates @   0 #gates @ 1- ?DO \ traverse gates list back-to-front
      assert( dup )
      DUP CELL+ @ gates{ I } !		\ record gate types (index from 1)
      @				( s: next-elem)
   -1 +LOOP  assert( dup 0= ) DROP
   0 { #undefined }
   (gates @   0 #gates @ 1- ?DO	\ second pass, searching for variables
      DUP gate-inputs CELLS bounds ?DO
	 I @ -1 = IF  -1 ,	 	\ record gate type of -1
	    #undefined 1+ TO #undefined
	 THEN
      CELL +LOOP
      @					( s: next-elem)
   -1 +LOOP  assert( dup 0= ) DROP
   #undefined gates{ ;

: flush-wires { #undefined -- #wires wires{ }
   \ Write out netlist in forms of two-endpoint wires connecting two
   \ gate/variable nodes.  Hmm, order of wires is currently reversed.  Need
   \ to keep that in mind when mapping inputs via adjmap{{
   #gates @ #undefined + 1-  { vidx }	\ numbering undefineds in reverse order
   DOUBLE , HERE	     { wires{ }
   (gates @   0 #gates @ 1- ?DO	\ traverse gates list back-to-front
      assert( dup )
      DUP gate-inputs CELLS bounds ?DO			\ scan through inputs,
	 assert( I @  -1 #gates @ WITHIN )
	 assert( I @  -1 J WITHIN )	\ (currently no backrefs allowed)
	 I @ DUP 0< IF			\ use variables nodes for undefined
	    DROP vidx   vidx 1- TO vidx
	 THEN
	 J 2,			\ and record edges (out->in)	 
      CELL +LOOP
      @					( s: next-elem)
   -1 +LOOP  assert( DUP 0= ) DROP
   assert( vidx #gates @ 1- = )
   HERE wires{ - DOUBLE /  wires{ ;  

: flush-netlist ( -- #gates #variables #wires wires{ gates{ )
   \ Return netlist of wires connecting gates and variables.  gates start it
   \ index 0, variables at index #gates.  Variables are in same order as gates
   \ they are attached to (and then ordered by input).
   flush-gate-types		{ #undefined gates{ }
   #undefined flush-wires	{ #wires wires{ }
   #gates @  #undefined  #wires  wires{ gates{ ;

: end-netlist ( -- #gates #undefined #wires wires{ gates{ )
   \ todo, reset (gates etc.?
   flush-netlist ;

: (nand)   AND INVERT 1 AND ;
: (nor)    OR INVERT 1 AND ;
: (ident)   1 AND ;

ALIGN INTEGER , HERE
' (ident) ,
' (nand) ,
HERE SWAP
CONSTANT gatext{  gatext{ - CELL / CONSTANT #gatexts

: dogate  ( in0...inN gate -- out )
   assert( DUP 0 #gatexts WITHIN )
   gatext{ SWAP } PERFORM ;

: netlist-eval { #gates #undefined #wires wires{ gates{ values{ -- }
   \ Compute state of nodes of netlist.  On input values{
   \ #gates...#gates+#undef-1 } hold {0,1} values defining state of variable
   \ inputs.  -1 denotes unknown nodes.  On output, values{ 0..#gates-1 } are
   \ the values at the output at each gate.
   \
   \ Order of inputs of nodes is taken from order in wires{}.
   #gates #undefined +   { #nodes }
   #nodes #wires wires{ }edges>adjmap  { adjmap{{ }
   #gates 0 ?DO -1 values{ I } ! LOOP
   #gates 0 ?DO
      DEPTH   { was-depth }
      adjmap{{ I } 2@ }bounds ?DO   \ load inputs of gate
	 I @ DUP 0< IF			\ is input?
	    INVERT values{ SWAP } @
	    assert( DUP 0>= )  ( known?)
	 ELSE DROP THEN
      CELL +LOOP		( s: in1...inN )
      gates{ I } @ dogate values{ I } !
      DEPTH was-depth <> ABORT" your mistake!"
   LOOP
   adjmap{{ }unallot ;

end-module

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth netlist-test.fs  -e bye"
   End:
[THEN]
