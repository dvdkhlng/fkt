\ -*-forth-*-
\ GF(p^2) Pfaffian test
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Nov 2012

3 assert-level !

require ./tester2.fs
T{ require ./zpfaffian.fs -> }T

max-modulus modulus !

0 VALUE M{{
0 [IF]

4 , DOUBLE ,  HERE TO M{{
0 0  2,  1 0 2,  0 0 2,  0 0 2,
1 rnegate 0 2,  0 0 2,  1 0 2,  0 0 2,
0 0  2, 1 rnegate 0 2,  0 0 2,  1 0 2,
0 0  2,  0 0 2, 1 rnegate 0 2,  0 0 2,

T{ 4 M{{ }}zpfaffian -> 1 0 }T

4 , DOUBLE ,  HERE TO M{{
0 0  2,  5 0 2, 3 rnegate 0 2, 7 rnegate 0 2,
5 rnegate 0 2,  0 0 2, 4 rnegate 0 2,  0 0 2,
3 0  2,  4 0 2,  0 0 2,  1 0 2,
7 0  2,  0 0 2,  1 rnegate 0 2,  0 ,

\ note: modulus 37 won't work for GF(p^2) modulo (x^2+1), see complex.fs
47 modulus !

T{ 4 M{{ }}zpfaffian -> 33 0 }T

[THEN]

47 modulus !
   
4 , DOUBLE ,  HERE TO M{{
0 0  2,  0 0 2, 3 rnegate 0 2, 7 rnegate 0 2,
0 0  2,  0 0 2, 4 rnegate 0 2,  5 0 2,
3 0  2,  4 0 2,  0 0 2,  1 0 2,
7 0  2, 5 rnegate 0 2, 1 rnegate 0 2,  0 0 2,

T{ 4 M{{ }}zpfaffian -> 43 0 }T  ( 43 mod 37)

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth zpfaffian-test.fs -e bye"
   End:
[THEN]
