\ -*-forth-*-
\ Elementary matrix transform tests
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014

require ./tester2.fs
T{ require ./remt.fs -> }T


3 CONSTANT m
4 CONSTANT n
0 VALUE m{{
3 , INTEGER , HERE TO m{{
2 ,  0 ,  2 , 6 ,
9 ,  3 ,  1 , 1 ,
0 ,  0 ,  5 , 7 ,

m n INTEGER MMATRIX mcopy{{
: copym  m{{ mcopy{{ m n * CELLS MOVE ;

T{ copym 2 2 mcopy{{ 0 1 1 (}}rrowadd) -> }T
T{ mcopy{{ 1 0 }} @+ @+ @+ @ -> 9 3 5 1 }T

T{ copym 2 2 mcopy{{ 0 1 2 (}}rcoladd) -> }T
T{ mcopy{{ 0 2 }} @ mcopy{{ 1 2 }} @ mcopy{{ 2 2 }} @  -> 2 7 5 }T

T{ copym 2 2 mcopy{{ 1 1 (}}rrowmul) -> }T
T{ mcopy{{ 1 0 }} @+ @+ @+ @ -> 9 6 2 1  }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth remt-test.fs -e bye"
   End:
[THEN]
