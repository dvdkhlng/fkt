\ -*-forth-*-
\ Matrix routines
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./fsl-util.fs
require ./mem.fs

: }unallot ( a-addr -- ) \ free vector that was ALLOTed
   HERE CELL+ - ALLOT ;
: }}unallot ( a-addr -- ) \ free matrix that was ALLOTed
   HERE CELL+ CELL+ - ALLOT ;
: }bounds  ( a-addr n -- a-addr2 a-addr1 )
   \ convert array + length to start+end-ptrs suitable for do..loop
   OVER -ROT } SWAP ;
: }eltsize ( array -- n )  cell- @ ;
: }patch-eltsize ( n array -- )  cell- ! ;
' }eltsize Alias }}eltsize  \ for readability
: }}ncols ( m{{ -- n ) \ return number of columns  in matrix
   [ 2 CELLS ]L - @ ;
: }}rowsize  ( m{{ -- n )  \ return number of bytes (AUs) per matrix row
   [ 2 CELLS ]L - 2@ * ;
: }}patch-dim ( eltsize ncols m{{ -- ) \ patch matrix header
   cell- cell- 2! ;

: }}swap-rows   { ncols M{{ row1 row2 -- } \ swap rows.
   row1 row2 = IF EXIT THEN
   M{{ row1 0 }}  M{{ row2 0 }}  M{{ }eltsize ncols *  exchange-mem ;

: }}swap-cols { n M{{ col1 col2 -- }
   \ swap columns in matrix M{{ with n rows and CELL-sized elements
   col1 col2 = IF EXIT THEN
   M{{ 0 col1 }}  M{{ 0 col2 }}
   M{{ }}rowsize   { stride }
   n 0 ?DO			( addr1 addr2 )
      2DUP 2DUP @ SWAP @	( addr1 addr2 addr1 addr2 val2 val1 )
      ROT ! SWAP !
      stride + SWAP stride + SWAP
   LOOP
   2DROP ;

: }}2swap-cols { n M{{ col1 col2 -- }
   \ swap columns in matrix M{{ with n rows and 2*CELL-sized elements
   col1 col2 = IF EXIT THEN
   M{{ 0 col1 }}  M{{ 0 col2 }}
   M{{ }}rowsize   { stride }
   n 0 ?DO			( addr1 addr2 )
      2DUP 2DUP 2@ ROT 2@	( addr1 addr2 addr1 addr2 val2 val1 )
      2ROT SWAP >R 2! R> 2!
      stride + SWAP stride + SWAP
   LOOP
   2DROP ;

: }}extract { M{{ row col S{{ nrows -- }
   \ extract part of M{{ and store result into S{{
   S{{ }}rowsize  { /row }
   nrows 0 ?DO
      M{{ row I + col }}  S{{ I 0 }}   /row MOVE
   LOOP ;
: }}I!  { n M{{ -- } \ set M to diagonal matrix I
   n 0 ?DO
      1   M{{ I 0 }} DUP n CELLS ERASE   I CELLS + !
   LOOP ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth matrix-test.fs -e bye"
   End:
[THEN]
