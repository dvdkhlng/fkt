\ -*-forth-*-
\ Compute table of first Fibonacci numbers
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011
require ./matrix.fs

185 CONSTANT #fibonacci
#fibonacci DOUBLE MARRAY fibonacci{

0. fibonacci{ 0 } 2!
1. fibonacci{ 1 } 2!

: (fibonacci)  { n -- }
   fibonacci{ n 1 - } 2@  fibonacci{ n 2 - } 2@ D+  fibonacci{ n } 2! ;
: fill-fibonacci{}  ( -- )
   #fibonacci 2 DO  I (fibonacci) LOOP ;
: print-fibonacci{}  ( -- )
   #fibonacci 0 DO CR ." F" I 4 .R \ ." ( " I 1 - 4 .R ." )"
      fibonacci{ I } 2@ 40 D.R
   LOOP ;

: fibonacci-encode  ( ud -- n v{ )
   \ generate fibonacci code for given number.  Stored as array of chars.
   ALIGN 1 CHARS ,  HERE  { v{ }
   0 { n }
   2 #fibonacci 1- DO
      2DUP fibonacci{ I } 2@ DU>=
      DUP 1 AND v{ I 2 - } C!
      IF
	 I n MAX TO n
	 fibonacci{ I } 2@ D-
      THEN
   -1 +LOOP
   2DROP
   n CELLS ALLOT
   1 v{ n 1- } C!
   n v{ ;
: fibonacci-decode ( v{ -- ud )
   0. ROT
   #fibonacci 2 DO
      COUNT IF
	 -ROT fibonacci{ I } 2@ D+ 
	 ROT DUP C@ IF \ found two ones, so we're done
	    DROP UNLOOP EXIT
	 THEN
      THEN
   LOOP
   TRUE ABORT" fibonacci decode error" ;

fill-fibonacci{}

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth fibonacci-test.fs -e bye"
   End:
[THEN]
