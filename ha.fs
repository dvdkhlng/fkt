\ -*-forth-*-
\ Holographic Algorithms
\
\ Copyright (C) 2012,2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Jul 2012

require ./matrix.fs
require ./complex.fs

: }ztensp  { n1 t1{ n2 t2{ t3{ -- }
   \ tensor product, result stored to t3.  t3 should have n1*n2 elements
   n1 0 ?DO
      t1{ I } 2@
      n2 0 ?DO
	 2DUP  t2{ I } 2@ z*  t3{ J n2 *  I + } 2!
      LOOP
      2DROP 
   LOOP ;

: }}zbasetrafo  { #bits #base n{ p{ m{{ -- }
   \ for every binary signature with #bits, store the corresponding signature
   \ generated in base (n, p) (of length #base) into the rows of m{{
   \
   \ Order of bits is chosen so that basis B0 results in a diagonal matrix, i.e.
   \ the tensor-product is computed as MSB*...*LSB.
   2 0 DO
      DOUBLE , HERE 1 #bits LSHIFT 2* CELLS ALLOT
   LOOP   { tensp1{ tensp2{ }
   1 #bits LSHIFT 0 ?DO 
      I 1 AND IF p{ ELSE n{ THEN   \ bit0 special treatment
      tensp1{ #base 2* CELLS MOVE
      #bits 1 ?DO  \ remaining bits
	 #base  J I RSHIFT 1 AND IF p{ ELSE n{ THEN	( n1 t1{ )
	 #base I 1- LSHIFT tensp1{			( n1 t1{  n2 t2{ )
	 tensp2{ }ztensp
	 tensp1{ tensp2{ TO tensp1{ TO tensp2{    \ swap buffers
      LOOP
      tensp1{  m{{ I 0 }}    \ copy tensor product to row of m{{
      1 #bits LSHIFT 2* CELLS MOVE
   LOOP 
   tensp1{ tensp2{ MIN }unallot ;

: }}saturate-io  { x #i/o #nodes am{{ -- #nodes' am'{{ }
   \ saturate the i/o nodes using value x, writing new signature matrix
   \ am'{{ which has #nodes' nodes
   \ todo: determine bit-order that makes most sense
   x count1bits  { #1bits }
   #nodes #1bits -  { #nodes' }
   am{{ }}eltsize  { /elt }
   #nodes' , /elt ,  HERE   { am'{{ }
   \ copy matrix, trimming saturated i/o nodes in the process
   #nodes 0 ?DO
      I #i/o <  x I RSHIFT 1 AND  AND  0= IF
	 #i/o 0
	 /elt INTEGER = IF
	    ?DO   x I RSHIFT 1 AND 0= IF am{{ J I }} @ , THEN  LOOP
	 ELSE
	    ?DO   x I RSHIFT 1 AND 0=  IF am{{ J I }} 2@ 2, THEN  LOOP
	 THEN
	 am{{ I #i/o }}  HERE  #nodes #i/o - /elt * DUP ALLOT MOVE
      THEN
   LOOP
   CR ." m_" x 0 .R ." :" CR
   #nodes' DUP am'{{ }}iprint
   
   #nodes' am'{{  ;

: (}}signature)  { #i/o #nodes am{{ sig{ -- }
   \ write signature-vector given FKT-oriented adjecency matrix.  interface of
   \ gate assumed to be on first #io nodes.  write 2**#io element signature to
   \ sig{.  Data type determined via element-size of sig{ (must match that of
   \ am{{.
   sig{ }eltsize  { /elt }
   assert( am{{ }}eltsize  /elt = )
   1 #i/o LSHIFT 0 ?DO
      I #i/o #nodes am{{ }}saturate-io  { #nodes' am'{{ }
      sig{ I }  #nodes' am'{{
      /elt INTEGER = IF   }}rpfaffian SWAP !
      ELSE                }}zpfaffian ROT 2!   THEN
      am'{{ }}unallot
   LOOP ;
   

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth ha-test.fs -e bye"
   End:
[THEN]


   
