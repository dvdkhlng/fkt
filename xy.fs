\ -*-forth-*-
\ 2-d vectors (pairs) of integers
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./imatrix.fs
require ./misc.fs

: xy+  ( x1 y1 x2 y2 -- x3 y3 )
   ROT + -ROT + SWAP ;
: xy-  ( x1 y1 x2 y2 -- x3 y3 )
   ROT SWAP - -ROT - SWAP ;
: xymax  ( x1 y1 x2 y2 -- x3 y3 )
   ROT MAX -ROT MAX SWAP ;
: r90ccw  ( x1 y1 -- x2 y2 )
   NEGATE SWAP ;
: xy*  ( x1 y1 x2 y2 -- d ) \ dot product
   ROT M* 2SWAP M* D+ ;
: xylen2  ( x y -- d )  \ square of length
   2DUP xy* ;
: xycollinear?  ( x1 y1 x2 y2 -- flag )
   \ note that 0-vector is considered collinear to any other vector
   -ROT M* DABS  2SWAP M* DABS D= ;
   
: .xy  ( x1 y1 -- )
   SWAP ." ( " . ." | " . ." ) " ;

: xya<  ( x1 y1 x2 y2 -- flag )
   \ true, if counter-clockwise angle from v1 to v2 is [0..pi[ deg
   \ (false, if any vector has length 0)
   r90ccw  xy* D0< ;
: xya<=  ( x1 y1 x2 y2 -- flag )
   \ true, if counter-clockwise angle from v1 to v2 is [0..pi] deg
   \ (true, if any vector has length 0)
   r90ccw  xy* D0<= ;
: xya=  ( x1 y1 x2 y2 -- flag )
   \ true, if same angle&direction or either vector is 0,0
   2OVER 2OVER xy* D0>= >R	   \ same direction?
   -ROT M*  2SWAP M* D= R> AND ;   \ same angle?

CREATE acmptbl
\ dot-product <0 =0 >0
\ r90-dot-p.
(        <0 ) 1 ,  1 , 1 ,
(        =0 ) 2 , -1 , 0 ,
(        >0 ) 3 ,  3 , 3 ,

: xyacmp  ( x1 y1 x2 y2 -- n )
   \ compare angles.  counter-clockwise angle from v1 to v2 is 'a', return
   \ 0, if a = 0
   \ 1, if a = [0..pi[
   \ 2, if a = pi
   \ 3, if a = [pi..2pi[
   \ -1, if either vector is 0, and result is undefined
   2OVER 2OVER xy* d>sign  1+
   r90ccw xy* d>sign 1+ 3 * +
   CELLS acmptbl + @ ;
   
: xyccw?  ( x1 y1 x2 y2 x3 y3 -- flag )
   \ true, if v1..v3 are sorted counter-clockwise
   \ undefined (?), if any two vectors are colinear, or any vector has
   \ zero length
   2OVER 2OVER xya< IF
      2ROT 2TUCK xya< >R
      2SWAP xya< R> OR
   ELSE
      2ROT 2TUCK xya< >R
      2SWAP xya< R> AND
   THEN ;   
: xyccw-decidable?  ( x1 y1 x2 y2 x3 y3 -- flag )
   \ true, if v1..v3 have distinct angles and are not zero
   2OVER 2OVER xya= >R
   2ROT 2TUCK xya= >R
   xya=
   2R> OR OR 0= ;
   
: }xyprint  ( n v -- )  \ print vector of xy-pairs
   SWAP 0 ?DO
      DUP I } 2@ .xy
   LOOP
   DROP ;
      
: }xysort-ccw  { n v{ -- }  \ order vector-array counter-clockwise
   \ this might loop endlessly if input vector contains duplicate, co-linear
   \ or zero vectors.  Todo: add check to xyccw? for undefined result?
   n 3 < IF EXIT THEN
   v{ 0 } 2@
   BEGIN
      true { done }
      n 1- 1 DO
	 v{ I }   v{ I 1+ }   { a b }
	 2DUP  a 2@  b 2@
	 \ todo: why assertion triggered here?
	 assert( 6dup xyccw-decidable? )
	 xyccw? 0= IF 
	    a b 2 iexchange
	    ( CR ." ->" n v{ }xyprint)
	    false TO done
	 THEN
      LOOP
   done UNTIL
   2drop ;

: }xysorted-ccw?  { n v{ -- flag  } \ verify, whether sorted (see above)
   n 3 < IF true EXIT THEN
   n 0 ?DO
      3 0 DO  v{ J I +  n MOD } 2@  LOOP
      xyccw? 0= IF  FALSE UNLOOP EXIT THEN
   LOOP
   TRUE ;

: }xysort-ccw'  { n idx{ xy{ -- }  \ order vector-array counter-clockwise
   \ like above, just with cell-array idx{} indexing coordinates in read-only
   \ array xy{} .  sorts index-array idx{}, xy{} not touched
   n 3 < IF EXIT THEN
   xy{ idx{ 0 } @ } 2@
   BEGIN
      true { done }
      n 1- 1 DO
	 idx{ I }  idx{ I 1+ }  { idx-a idx-b }
	 2DUP  xy{ idx-a @ } 2@  xy{ idx-b @ } 2@
	 assert( 6dup xyccw-decidable? )
	 xyccw? 0= IF   \ exchange if not counter-clockwise (bubble sort)
	    idx-a @ idx-b @  idx-a ! idx-b !
	    ( CR ." ->" n v{ }xyprint)
	    false TO done
	 THEN
      LOOP
   done UNTIL
   2drop ;

: }xysorted-ccw?'  { n idx{ xy{ -- flag }
   \ verify, whether sorted via index (see above)
   n 3 < IF true EXIT THEN
   n 0 ?DO
      3 0 DO   xy{ idx{ J I + n MOD } @ } 2@  LOOP
      xyccw? 0= IF  FALSE UNLOOP EXIT THEN
   LOOP
   TRUE ;

\ : }sort-edges-ccw'  { n idx{ edges{ d: xy0 xy{ -- }
\    \ order edges in graph counter-clockwise.  idx{} refers to the index of
\    \ edges to work on, for each edge indexed in idx{}, edges{} gives the pairs
\    \ start,end nodes of the edge.  the start,end nodes are then used as
\    \ indexes into the xy{} array.  posive idx in idx{} refer to the start
\    \ node of the edge, negative idx{} to end nodes.   sorting is done
\    \ to the idx{} array
\    n 3 < IF EXIT THEN
\    xy{ edges{ idx{ 0 } @ }+-@node } 2@ xy0 xy-
\    BEGIN
\       true { done }
\       n 1- 1 DO
\ 	 idx{ I }  idx{ I 1+ }  { idx-a idx-b }
\ 	 2DUP
\ 	 xy{ edges{ idx-a @ }+-@node } 2@ xy0 xy-
\ 	 xy{ edges{ idx-b @ }+-@node } 2@ xy0 xy-
\ 	 assert( 6dup xyccw-decidable? )
\ 	 xyccw? 0= IF 
\ 	    idx-a @ idx-b @  idx-a ! idx-b !
\ 	    ( CR ." ->" n v{ }xyprint)
\ 	    false TO done
\ 	 THEN
\       LOOP
\    done UNTIL
\    2drop ;

: xy-integral*2 ( x1 y1 x2 y2 -- d )
   \ compute area below vector times two (sign of d like integral)
   2SWAP DUP >R xy-
   OVER >R
   M*			( area of triangle times two )
   2R> M* D2* D+ ;	( plus area of bar times two)

: xy-area*2  { n xy{ -- d }
   \ compute double area of polygon with given vertices.  area is positive for
   \ vertices in clockwise order, else negative.  Computing the double
   \ of the actual area allows us to be exact for all inputs (no rounding).   
   0.
   n 0 DO
      xy{ I } 2@ 
      xy{ I 1+ n MOD } 2@
      xy-integral*2 D+
   LOOP ;

: xy-intersect?  { d: v1 d: v2 d: v3 d: v4 -- flag )
   \ true, if lines v1-v2 and v3-v4 intersect.  Undefined, if endpoints lie on
   \ a line, but lines don't actually cross.  Does't use line equation
   \ solving, as that may be difficult to get right in integer arithmetic.
   \ Instead we use angular relations: angles from endpoints of one line to
   \ endpoints of other line must both be either be <= pi or > pi
   \ (i.e. quadrangle formed by both lines as diagonal must be convex).
   v1 v3 xy- v2 v3 xy- v1 v4 xy- v2 v4 xy- { d: t1 d: t2 d: t3 d: t4 }
   t1 t2 xya<
   t3 t4 xya< <>
   t1 t3 xya<
   t2 t4 xya< <> AND ;
\ todo: : xy-intersect?'  { d: v1 d: v2 d: v3 d: v4 -- flag )
   \ same as above, just also check for pathological case of points lieing on
   \ a line.  return true, if lines intersect or touch, else false.
   \ todo: xyacmp, then table-based or somehow arithmetic?
   
   
   
   
\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth xy-test.fs -e bye"
   End:
[THEN]
