\ -*-forth-*-
\ Matrix multiplication test-case
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014

require ./tester2.fs
T{ require ./rmatmul.fs -> }T

0 VALUE a{{
0 VALUE b{{

4 CONSTANT m
2 CONSTANT n
3 CONSTANT kk

\ A: mxk
kk , INTEGER , HERE TO a{{
1  , 2  , 3  ,
4  , 5  , 6  ,
7  , 8  , 9  ,
10 , 11 , 12 ,

\ B: kxn
n , INTEGER , HERE TO b{{
2  , 3  ,
7  , 9  ,
4  , 0  ,

\ C: mxn
n n INTEGER MMATRIX c{{

T{ kk a{{ 1 0 b{{ 0 1 }}rdot-rc  -> 57 }T

T{ m n kk a{{ b{{ c{{ }}rmatmul -> }T

T{ c{{ 0 0 }} @+ @+  @+ @+  @+ @+  @+ @+ DROP ->
28    21		\ expected constants of C
67    57
106   93
145   129 }T

\ multiply&add test subtracts A*B from C yielding 0:
T{ m n kk 1 rnegate a{{ b{{ c{{ }}rmatmadd -> }T
T{ c{{ 0 0 }} @+ @+  @+ @+  @+ @+  @+ @+ DROP ->  0 0  0 0  0 0  0 0 }T

4 , INTEGER , HERE TO a{{
1  , 0  , 0 ,  0 ,
0  , 2  , 0 ,  0 ,
0  , 0  , 3 ,  0 ,
0  , 0  , 0 ,  4 ,
0 VALUE mark  HERE TO mark

T{ 4 a{{ 7 }}rmatpow  HERE -> mark }T
T{ a{{ 0 0 }} @  -> 1 }T
T{ a{{ 1 1 }} @  -> 2 7 r** }T
T{ a{{ 3 3 }} @  -> 4 7 r** }T
T{ 4 a{{ 1 }}rmatpow  HERE -> mark }T
T{ a{{ 3 3 }} @  -> 4 7 r** }T
T{ 4 a{{ 2 }}rmatpow  HERE -> mark }T
T{ a{{ 3 3 }} @  -> 4 7 2 * r** }T
T{ 4 a{{ 0 }}rmatpow  HERE -> mark }T
T{ a{{ 3 3 }} @  -> 1 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth rmatmul-test.fs -e bye"
   End:
[THEN]

