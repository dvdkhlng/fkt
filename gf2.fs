\ -*-forth-*-
\ Arithmetic in Galois Fields of characteristic 2
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2012

require ./misc.fs

$1f VALUE gf2-polynomial  \ the reducing polynomial.  must not have factors

: gf2+  ( x1 x2 -- x3 )  \ galois field addition
   XOR ;
: (gf2*)  ( x1 x2 -- x3 )  \ polynomial multiplication (mod 2)
   \ (note: non-reduced x3 has degree x1 plus degree of x2
   0 >R
   BEGIN DUP WHILE
	 DUP 1 AND IF OVER R> gf2+ >R THEN
	 1 RSHIFT SWAP 2* SWAP
   REPEAT
   2DROP R> ;
: gf2>=  ( x1 x2 -- flag )  \ does subtracting x2 from x1 decrease degree?
   2DUP U> IF 2DROP TRUE EXIT THEN
   2DUP XOR TUCK U> -ROT U> AND ;
: (gf2/mod)  ( x1 x2 -- x3 x4 )  \ polynomial division (mod 2)
   DUP 0= ABORT" gf2 division by zero"
   0 -ROT		( s: quotient divisor divident )
   0 #bits/cell 1- DO
      DUP DUP I LSHIFT TUCK I RSHIFT = IF
	 SWAP >R	( s: quotient divisor shifted_divident  r: divident )
	 2DUP gf2>= IF gf2+ SWAP 2* 1 OR SWAP
	 ELSE DROP SWAP 2* SWAP THEN
	 R>
      ELSE DROP THEN
   -1 +LOOP 
   DROP ;
: gf2-reduce  ( x1 -- x2 ) 
   gf2-polynomial (gf2/mod) NIP ;
: gf2*   ( x1 x2 -- x3 x4 )  \ galois field multiplication
   (gf2*) gf2-reduce ;

: gf2-reduce1  ( x1 -- x2 )  \ perform simplified modulo-operation for case
   \ that x1 has order not higher than GF2-POLYNOMIAL
   gf2-polynomial 2DUP gf2>= IF gf2+ EXIT THEN
   DROP ;
: gf2*  ( x1 x2 -- x3 )  \ galois field multiplication.
   \ this version supports (almost) full bit-width of CELL by performing the
   \ modulo-step inside the loop.
   0 >R
   BEGIN DUP WHILE
	 DUP 1 AND IF OVER R> gf2+ gf2-reduce1 >R THEN
	 1 RSHIFT SWAP 2* gf2-reduce1 SWAP
   REPEAT
   2DROP R> ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth gf2-test.fs -e bye"
   End:
[THEN]