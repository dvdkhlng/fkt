\ -*-forth-*-
\ compute the dual of a graph
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./graph.fs
require ./equiv.fs

: edge>frontface  ( eidx -- face )
   \ given edge-idx with sign-bit encoding orientation of edge, return
   \ corresponding initial face-id (assuming that each edge has 1 corresponding
   \ face at front/back side each
   DUP 0< IF   1negate 2* EXIT THEN
   2* 1+ ;
: edge>backface  ( eidx -- face )
   \ given edge-idx with sign-bit encoding orientation of edge, return
   \ corresponding initial face-id (assuming that each edge has 1 corresponding
   \ face at front/back side each
   1negate edge>frontface ;

: }dual-graph  { #nodes xy{ #edges edges{ edgemap{{  faces{ -- #faces }
   \ Compute the dual graph
   \
   \ Input: xy{} : array, indexed by node-id, giving a planar coordinate for
   \ nodes in the graph
   \ edges{}: array of pairs of node-ids, describing the edges.
   \ edgemap{{: edgemap, SORTED(!) via }}sort-edgemap.
   \
   \ Output:
   \ faces{}: array of pairs, fulfilling two roles: (a) each entry is a pair
   \ of node-ids describing the edges of the dual graph.  note that edges may
   \ be listed multiple times.  (b) Every entry corresponds to the same index'
   \ entry in the edge{} array, giving the 2 face-ids (back-side, front-side)
   \ of the edge (front side is counter-clockwise side of edge).
   \
   \ #faces: number of faces of input, number of nodes of graph faces{} TODO:
   \ need a compacted dual graph edge array, listing edges only once, (ordered
   \ lower-face,higher-face)?

   \ equivalence relation for faces generated here has exactly 2*edges entries
   DOUBLE , HERE #edges 2* DOUBLE * ALLOT DUP		{ equivs{ 'equivs }
   
   \ iterate over all nodes of the graph, collecting equivalence relation of
   \ face-ids assigned above
   #nodes 0 ?DO
      \ list face equivalent relations: front face of edge is equivalent
      \ to backside of next edge in counter-clockwise order
      edgemap{{ I } 2@					{ eidxs{ #neighbours  }
      #neighbours 0 DO
	 \ front side of edge I...
	 eidxs{ I } @ edge>frontface
	 \ ... and back side of edge I+1
	 eidxs{ I 1+ #neighbours MOD } @ edge>backface
	 \ ... belong to same face 
	 'equivs 2!
	 'equivs CELL+ CELL+ TO 'equivs
      LOOP
   LOOP

   \ apply equivalence classes to faces{}.  note that for reduction we treat
   \ faces{} as array of face-ids, _not_ array of pairs of faces
   \ note that this leads to the ordering backface,frontface on 2@, as
   \ 2@ loads lowest address to TOS.
   faces{ }eltsize   ( s: was-eltsize )
   INTEGER faces{ }patch-eltsize
   #edges 2* equivs{ #edges 2* faces{ }equivs>idmap  ( s: was-eltsize #faces)
   SWAP faces{ }patch-eltsize
   equivs{ }unallot ;

: }prev-proper-edge  { eidx faces{ eidxs{ #neighbours -- eidx2 }
   \ Like }prev-edge, but skip over non-proper faces, i.e. faces which have
   \ the same front and back face.
   eidx BEGIN
      eidxs{ #neighbours }prev-edge    ( s: eidx2 )
      faces{ OVER 1abs } 2@ <>
      \ loop over edges EIDX until we find a new edge which has different
      \ faces on its front and back side.  This always terminates, if the
      \ incoming edge was a proper face.
   UNTIL ;

: }face-edge { faces{ faceedgemap{{ face -- eidx|-1 }
   \ Use faceedgemap{{}} to find any edge facing FACE that is a "proper" edge.
   \ A "proper" edge is an edge that has two different faces on both sides.
   \ Non-proper edges are never part of a cycle and we need to ignore them
   \ when re-orienting faces or when doing }face-area.  If face has no proper
   \ edges, return -1 (e.g. when FACE is the single outside face of a graph
   \ that has no cycles at all).
   \ CR ." faceedgemap print:" faceedgemap{{ face } 2@ }bounds ?DO I @ 1abs .
   \ CELL +LOOP
   faceedgemap{{ face } 2@ }bounds ?DO
      faces{ I @ 1abs } 2@ <> IF I @ 1abs UNLOOP EXIT THEN
   CELL +LOOP
   \ FACE does not have a proper edge on its boundary (e.g. it is the outer
   \ face and the graph does not have any cycles)
   -1 ;

: }next-boundary-edge { eidx face edges{ edgemap{{ faces{ -- eidx2 }
   \ Given one edge on the boundary of face, return next counter-clockwise
   \ edge on the face boundary.  Note that for the outside face, this is going
   \ to look clockwise, though (when viewed from inside the not-outside face).
   \ Edge sign ignored on input.  Return edge with sign, as stored in
   \ edgemap{{ entry of the node we cross.  I.e. negative for edges oriented
   \ along the counter-clockwise path.
   \
   \ EIDX needs to correspond to a "proper" edge i.e. an edge that has
   \ different faces on its front vs. back side (if not, we never terminate,
   \ BTW).
   \
   \ edgemap{{ needsto be sorted, like it's passed into }dual-graph
   \
   \ Explanation: faces{} maps edge to its backside&frontside faces.
   \ depending on which side of edge EIDX the face FACE is, counter-clockwise
   \ node is either start- or end-node.  Looking at all edges originating from
   \ that node, we take the edge that comes next after EIDX in clockwise edge
   \ order ni.e the "prev" edge in map sorted by }}sort-edgemap.
   \ Caveat: need to skip over non-proper faces (see also }face-edge)
   eidx 1abs TO eidx
   edges{ eidx } 2@			( s: startnode endnode )
   face faces{ eidx } 2@ start-node?    ( s: startnode endnode on-backside? }
   ( on-backside?) IF DROP ( startnode)
   ELSE   NIP ( endnode) THEN >R	( R:nextnode )
   eidx faces{ edgemap{{ ( nextnode)R> } 2@  }prev-proper-edge ;

: }face-area  { eidx face xy{ edges{ edgemap{{ faces{ -- d }
   \ Compute area of FACE (times 2) given an edge EDIX on FACE's boundary.
   \ Area is negative for inner faces, and positive for an outer face.
   eidx 1abs { startidx }
   0.        ( s: area)
   BEGIN
      eidx face edges{ edgemap{{ faces{ }next-boundary-edge TO eidx
      xy{ edges{ eidx }+-@edge >R } 2@
      xy{ R> } 2@
      xy-integral*2 D+
   eidx 1abs startidx = UNTIL ;

: }outer-face { #faces #edges xy{ edges{ edgemap{{ faces{ faceedgemap{{ -- face }
   \ Return the index of the outer face
   #faces 0 ?DO
      faces{ faceedgemap{{ I }face-edge DUP 0< IF   ( s: eidx)
         \ face does not have a proper boundary edge, assume that
         \ it is the outside face (it may still be an unconnected subgraph,
         \ but we rather not support those)
         DROP I UNLOOP EXIT
      THEN    ( s: eidx)
      I xy{ edges{ edgemap{{ faces{ }face-area D0< 0= IF
	 I UNLOOP EXIT
      THEN
   LOOP
   true ABORT" Ouch.  Failed to find an outer face." ;
   

      
   
   
\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth dual-test.fs -e bye"
   indent-tabs-mode: t
   End:
[THEN]
