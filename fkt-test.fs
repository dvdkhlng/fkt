\ -*-forth-*-
\ test FKT algorithm
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

3 assert-level !

require ./tester2.fs
T{ require ./fkt.fs -> }T
T{ require ./rdet.fs -> }T

0 VALUE xy{
0 VALUE edges{
0 VALUE weights{
0 VALUE zweights{
0 VALUE #edges
0 VALUE #nodes

\ define the graph from the FKT_algorithm wiki entry
10 TO #nodes
DOUBLE ,  HERE			\ planar embedding
0 5 2,  1 5 2,  2 5 2,
0 4 2,  1 4 2,  2 4 2,
0 3 2,  1 3 2,  2 3 2,
        1 2 2,
TO xy{  

14 TO #edges
DOUBLE ,  HERE			\ edges
0 1 2,  1 2 2,
0 3 2,  1 4 2,  2 5 2,
3 4 2,  4 5 2,
3 6 2,  4 7 2,  5 8 2,
7 8 2,
6 9 2,  7 9 2,  8 9 2, 
TO edges{  

#edges 1 CHARS MARRAY edgeorient{
edgeorient{ #edges 0 FILL

T{ #nodes #edges edges{ }edges>adjmap  CONSTANT adjmap{{ -> }T
T{ adjmap{{ #nodes spanning-tree  CONSTANT #span CONSTANT span{ -> }T
T{ #nodes #span span{ }edges>adjmap  CONSTANT spanadj{{ -> }T

CR #span span{ }xyprint

T{ #nodes #edges edges{ }edges>edgemap  CONSTANT edgemap{{ -> }T
T{ #nodes xy{ edges{ edgemap{{ }}sort-edgemap  -> }T
#edges DOUBLE MARRAY faces{
T{ #nodes xy{ #edges edges{ edgemap{{ faces{ }dual-graph CONSTANT #faces }T

CR #edges faces{ }xyprint

T{ #edges edges{ spanadj{{ faces{ }dual-spanning-tree
CONSTANT #dedges CONSTANT dedges{  }T

T{ #dedges  -> 5 }T
T{ dedges{ 0 } 2@  -> 0 1 }T
T{ dedges{ 1 } 2@  -> 1 2 }T
T{ dedges{ 2 } 2@  -> 3 4 }T
T{ dedges{ 3 } 2@  -> 4 0 }T
T{ dedges{ 4 } 2@  -> 5 0 }T

CR #dedges dedges{ }xyprint

\ tracing edges on face
0 VALUE #path
0 VALUE path{
T{ edges{ edgemap{{ faces{ ( edge)0 ( face)1 }trace-face
TO #path TO path{ -> }T
T{ #path -> 4 }T
CR #path path{  }iprint
T{ path{ 0 } @ -> 0 }T
T{ path{ 1 } @ -> 2 1negate }T
T{ path{ 2 } @ -> 5 1negate }T
T{ path{ 3 } @ -> 3 }T

\ orientation of initial edge of trace mustn't matter
T{ edges{ edgemap{{ faces{ ( edge)0 1negate ( face)1 }trace-face
TO #path TO path{ -> }T
T{ #path -> 4 }T
T{ path{ 0 } @ -> 0 }T

\ face orientation
0 VALUE #cwe
T{ edges{ edgeorient{ edgemap{{ faces{ ( edge)0 ( face)1
}face-orientation  TO #cwe -> }T
T{ #cwe -> 2 }T

T{ edges{ edgeorient{ edgemap{{ faces{ ( edge)0 1negate ( face)1
}face-orientation  TO #cwe    -> }T
T{ #cwe -> 2 }T

T{ edges{ edgeorient{ edgemap{{ faces{ ( edge)7 1negate ( face)3
}face-orientation  TO #cwe   -> }T
T{ #cwe -> 3 }T

-1 edgeorient{ 7 } c!
T{ edges{ edgeorient{ edgemap{{ faces{ ( edge)7 1negate ( face)3
}face-orientation  TO #cwe   -> }T
T{ #cwe -> 4 }T

T{ #faces #edges faces{ }edges>edgemap  CONSTANT faceedgemap{{ -> }T

T{ 0 edges{ edgeorient{ edgemap{{ spanadj{{ faces{ faceedgemap{{
}orient-edges .s -> }T
: printorient #edges 0 DO ." {" I . ." }=" edgeorient{ I } C@ . LOOP ;
CR printorient
T{ edges{ edgeorient{ edgemap{{ faces{ #edges #faces
}check-edge-orient .s -> 0 }T

\ integrated reorientation code
31 modulus !
T{ #nodes #edges xy{ edges{ }fkt-adjmap  CONSTANT fktadj{ -> }T
#nodes #nodes INTEGER MMATRIX adjmatrix{{
T{ #nodes fktadj{ adjmatrix{{ 1 1 rnegate (}}adjmap>+-0matrix) -> }T
T{ #nodes adjmatrix{{ }}rdet -> 25 }T

\ integrated matchings counting
max-modulus modulus !
T{ #nodes #edges xy{ edges{ }#matchings -> 5 }T

\ some simple patterns to test against

\ square
ALIGN
DOUBLE , HERE TO xy{  4 TO #nodes
0 0 2,  1 0 2,  1 0 2,  1 1 2,
DOUBLE , HERE TO edges{  4 TO #edges
0 1 2,  0 2 2,  2 3 2,  1 3 2,
T{ #nodes #edges xy{ edges{ }#matchings -> 2 }T

INTEGER , HERE  TO weights{  1 , 1 , 1 , 1 , 
T{ #nodes #edges xy{ edges{ weights{ }#weighted-matchings -> 2 }T
1 rnegate weights{ 0 } !
T{ #nodes #edges xy{ edges{ weights{ }#weighted-matchings -> 0 }T

\ 5-wheel
ALIGN
DOUBLE , HERE TO xy{  6 TO #nodes
0 0 2,  2 0 2,  2 2 2,  1 3 2,  0 2 2,  1 1 2,
DOUBLE , HERE TO edges{  10 TO #edges
0 1 2,  1 2 2,  2 3 2,  3 4 2,  0 4 2,
0 5 2,  1 5 2,  2 5 2,  3 5 2,  4 5 2,

T{ #nodes #edges xy{ edges{ }#matchings -> 5 }T

INTEGER , HERE  TO weights{
1 , 1 , 1 , 1 , 1 ,
1 , 1 , 1 , 1 , 1 ,
T{ #nodes #edges xy{ edges{ weights{ }#weighted-matchings -> 5 }T
1 rnegate weights{ 5 } !
T{ #nodes #edges xy{ edges{ weights{ }#weighted-matchings .s -> 3 }T
7 weights{ 6 } !
T{ #nodes #edges xy{ edges{ weights{ }#weighted-matchings .s -> 9 }T

DOUBLE , HERE  TO zweights{
1 0 2,  1 0 2,  1 0 2,  1 0 2,  1 0 2, 
1 0 2,  1 0 2,  1 0 2,  1 0 2,  1 0 2, 

T{ #nodes #edges xy{ edges{ zweights{ }#zweighted-matchings  -> 5 0 }T
1 rnegate 0 zweights{ 5 } 2!
T{ #nodes #edges xy{ edges{ zweights{ }#zweighted-matchings .s -> 3 0 }T

\ According to Ashley Montanaro's slides "Counting perfect matchings in planar
\ graphs".  
T{ 8 8 make-square-lattice }#matchings -> 12988816 }T
T{ 4 4 make-square-lattice }#matchings -> 36 }T

\ just for regression testing, no reference for thes numbers.
T{ 9 9 make-square-lattice }#matchings -> 0 }T  ( trivial)
T{ 6 6 make-square-lattice }#matchings -> 6728 }T

: 2xk-lattice-series  ( -- 1..n )
   20 1 ?DO  2 I make-square-lattice }#matchings LOOP ;
: fibonacci  ( n -- )
   DUP 1 > IF  DUP 1- RECURSE  SWAP 2 - RECURSE + EXIT THEN  DROP 1 ;
: fibonacci-series  ( -- 1..n )
   20 1 ?DO  I fibonacci LOOP ;

T{ fibonacci-series -> 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765 }T
T{ 2xk-lattice-series -> fibonacci-series }T

\ Bug: this spins endelssly when trying to compute the area of the outer face
\ T{ 4 1 4xk-lattice-series -> 1 }T

: 4xk-lattice-series  ( -- 1..n )
   16 2 ?DO  4 I make-square-lattice }#matchings LOOP ;

\ no reference for these numbers, just testing for regression
CELL 4 > [IF]
   T{ 4xk-lattice-series ->
   5 11 36 95 281 781 2245 6336 18061 51205 145601 413351 1174500 3335651 }T
[THEN]


: 2xk-lattice-series-complex  ( -- 1..n )
   12 1 ?DO
      2 I make-square-lattice  { #nodes #edges xy{ edges{ }
      DOUBLE , HERE  #edges 2* CELLS ALLOT  { zweights{ }
      #edges 0 ?DO
	 edges{ I } 2@ J / SWAP J / <> IF  \ vertical edge
	    0 1 ELSE 1 0
	 THEN  zweights{ I } 2!
      LOOP
      \ CR I 3 .R SPACE #edges zweights{ }zprint
      #nodes #edges xy{ edges{ zweights{ }#zweighted-matchings
      zweights{ }unallot
   LOOP ;

\ todo: fails after switch from det to pfaffian.  Add correct signs on right
\ side and done?
40 maxdepth-.s !
\ T{ 2xk-lattice-series-complex ~~ ->
\ 0 1 0 0 0 1 1 0 0 0 1 0 0 1 0 0 0 1 1 0 0 0 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth -r 1M -l 1M -m 16M -- fkt-test.fs  -e bye"
   End:
[THEN]

