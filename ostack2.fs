\ -*-forth-*-
\ 4HDL Object stack for complex, variable-length objects
\
\ Copyright (C) 2010,2021 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2010

\ Note: object sizes measured in address units.  

struct
   cell% field o-sp   
   cell% field o-limit    \ this points to the end of the stack
   cell% field o-bottom   \ this _is_ the start of stack, not a pointer
end-struct ostack%

\ o-sp is a no-op (zero offset). compile it away.
: o-sp ; immediate

: make-ostack:  ( size "name" -- )
   \ create object stack that can store 'size' AUs of data
   create here swap   ostack% rot + %allot drop
   >r r@ o-bottom  r@ o-sp !
   here r> o-limit ! ;

: (odepth)  ( ostack -- n )
   >r r@ o-sp @  r> o-bottom - ;
: (oalloc)  ( n ostack  -- oaddr )  \ create object on ostack
   >r
   r@ o-sp @ cell+ swap			\ fetch osp: returned as 'oaddr'
   2dup + aligned                       \ compute new osp
   assert( dup  r@ o-limit @ < )        \ overflow check
   dup r> o-sp !	                \ update osp
   ! ;					\ write size-marker to top of ostack
: (osize)  ( ostack -- n )  \ size of object on top of stack
   o-sp @ @ ;
: (oaddr)  ( ostack -- oaddr )  \ address of object size on top of object stack
   o-sp @ dup @ aligned -  ;
: (oresize)  ( ostack n -- )  \ change size of topmost item
   >r r@ (oaddr)   ( r: ostack   s: n addr )
   over aligned +  ( r: ostack   s: n addr2 )
   tuck            ( r: ostack   s: addr2 n addr2 )
   !               ( r: ostack   s: addr2 )
   r> o-sp ! ;
: (otos)   ( ostack -- addr n )  \ return size and address of top object
   o-sp @ dup @ tuck aligned -  swap ;
: (onos)   ( ostack -- addr n )  \ return size and address of next stack object
   >r r@ o-sp @  r> (osize) aligned cell+ -
   dup @  tuck aligned -  swap ;
: (odrop)  ( ostack --  o: o -- )  \ remove object from ostack
   >r assert( r@ o-sp @ r@ o-bottom > )	  \ underflow check
   r@ (osize) aligned cell+  negate  r> o-sp +! ;
: (o2drop)  ( ostack -- o: o1 o2 -- ) dup (odrop) (odrop) ;
: (o.s)  ( ostack -- )  \ dump object stack
   >r r@ o-sp @   cr
   begin  dup r@ o-bottom > while
	 dup @    dup ." == object sized " .
	 dup aligned   dup  ." (alloc " . ." ) =="
	 rot swap -       \ compute object start addr
	 dup rot  dump    \ dump object (only alloced size)
	 cell -           \ skip to next size marker 
   repeat
   rdrop drop ;
: (opush)  ( addr n ostack  --  o: -- o)  \ read object and copy to ostack
   >r dup r> (oalloc)  swap move ;
: (opop)  ( addr ostack --   o: o -- )  \ copy from ostack back to object
   >r r@ (otos) rot swap  move  r> (odrop) ;
: (odup)  ( ostack --   o: o1 -- o1 o1 )
   >r r@ (otos) dup r> (oalloc)  swap aligned move ;
: (oover)  ( ostack --   o: o1 o2 -- o1 o2 o1 )
   >r r@ (onos) dup r> (oalloc)  swap aligned move ;
: (onip)  ( ostack  --   o: o1 o2 -- o2 )
   >r r@ (otos)   r@ (onos) drop  swap aligned 2dup + >r  cell+ move
   r> r> o-sp ! ;
: (oswap)  ( ostack --  o: o1 o2 -- o2 o1 )
   >r  r@ (otos) r@ (onos)   \ get addresses of objects to swap 
   r@ (oover)       \ append o1 to o2 on stack,
   aligned cell+  rot aligned +   \ then move everything down (similar to nip)
   2dup + >r  cell+ move   
   r> r> o-sp ! ;
: (oconcat)  ( ostack -- o: o1 o2 -- o3 )  \ concatenate two objects
   >r
   r@ (otos)         \ tos-addr tos-n
   tuck              \ tos-n tos-addr tos-n
   r@ (onos)         \ tos-n tos-addr tos-n  nos-addr nos-n
   2dup 2>r          \  r:   ostack nos-addr nos-n
   +  swap move      \ tos-n
   \ objects concatenated, now write new stack entry size field.
   r> +              \ {tos-n+nos-n}
   dup aligned r> +  \ {tos-n+nos-n} sp-addr
   tuck !
   r> o-sp ! ;
: (oliteral)  ( ostack --   o: o --   execution: o:  -- o)
   >r r@ (otos)  postpone sliteral r@ postpone literal postpone (opush)
   r> (odrop) ; immediate compile-only

\ define words to create words like the above ones, with the 'ostack' argument
\ pre-set to a given stack

: odepth:  create ,  does> @ (odepth) ;
: oalloc:  create ,  does> @ (oalloc) ;
: osize:  create ,  does> @ (osize) ;
: oaddr:  create ,  does> @ (oaddr) ;
: oresize:  create ,  does> @ (oresize) ;
: otos:  create ,  does> @ (otos) ;
: onos:  create ,  does> @ (onos) ;
: odrop:  create ,  does> @ (odrop) ;
: o2drop:  create ,  does> @ (o2drop) ;
: o.s:  create ,  does> @ (o.s) ;
: opush:  create ,  does> @ (opush) ;
: opop:  create ,  does> @ (opop) ;
: odup:  create ,  does> @ (odup) ;
: oover:  create ,  does> @ (oover) ;
: onip:  create ,  does> @ (onip) ;
: oswap:  create ,  does> @ (oswap) ;
: oconcat:  create ,  does> @ (oconcat) ;
: oliteral:  create , immediate compile-only  does> @ POSTPONE (oliteral) ;

\ Define a "standard" object stack, and words that work on it

16384  make-ostack: ostack

ostack odepth: odepth
ostack oalloc: oalloc
ostack osize: osize
ostack oaddr: oaddr
ostack oresize: oresize
ostack otos: otos
ostack onos: onos
ostack odrop: odrop
ostack o2drop: o2drop
ostack o.s: o.s
ostack opush: opush
ostack opop: opop
ostack odup: odup
ostack oover: oover
ostack onip: onip
ostack oswap: oswap
ostack oconcat: oconcat
ostack oliteral: oliteral

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth ./test-ostack.fs -e bye"
   End:
[THEN]
