\ -*-forth-*-
\ Drawing graphs
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./xy.fs
require ./module.fs

module drawing

<private>
2VARIABLE cursor
VARIABLE color  

VARIABLE (points
VARIABLE (lines
VARIABLE (aliases
VARIABLE #points
VARIABLE #lines

<public>
: draw  ( -- )  \ start a drawing
   0 (points !  0 #points !
   0 (lines !  0 #lines !
   0 0 cursor 2!
   1 color ! ;
: flush-points  ( -- xy{ )
   DOUBLE , HERE #points @ 2* CELLS ALLOT		{ xy{ }
   (points @   -1 #points @ 1- -DO	\ traverse points list back-to-front
      assert( dup )			( s: elem )
      DUP CELL+ 2@ xy{ I } 2!
      @					( s: next)
   1 -LOOP assert( dup 0= ) DROP
   xy{ ;
: flush-lines  ( -- lines{ )
   DOUBLE , HERE #lines @ 2* CELLS ALLOT	{ lines{ }
   (lines @   -1 #lines @ 1- -DO		\ traverse lines list back-to-front
      assert( dup )			( s: elem )
      DUP CELL+ 2@ lines{ I } 2!
      @					( s: next)
   1 -LOOP assert( dup 0= ) DROP
   lines{ ;
: flush-colors  { complex? -- colors{ }
   complex? IF DOUBLE ELSE INTEGER THEN	
   DUP , HERE SWAP #lines @ * ALLOT	{ colors{ }
   (lines @   -1 #lines @ 1- -DO		\ traverse lines list back-to-front
      assert( dup )			( s: elem )
      complex? IF
	 DUP 3 CELLS + 2@ colors{ I } 2!
      ELSE
	 DUP 4 CELLS + @ colors{ I } !
      THEN
      @					( s: next)
   1 -LOOP assert( dup 0= ) DROP
   colors{ ;
: flush-drawing  { complex? -- #points #lines xy{ lines{ colors{ }
   \ Convert drawing to arrays for points, lines, colors can call it multiple
   \ times after starting a drawing to extract partial results.  Free
   \ memory by doig xy{ }unallot.
   #points @ #lines @
   flush-points flush-lines complex? flush-colors ;

: end-draw  ( -- #points #lines xy{ lines{ colors{ )
   false flush-drawing ;
: end-zdraw  ( -- #points #lines xy{ lines{ zcolors{ )
   true flush-drawing ;
   
: walk  ( x y -- )  \ change cursor position
   cursor 2@ xy+ cursor 2! ;
: travel  ( x y -- )  \ change cursor to aboslute position
   cursor 2! ;

\ for details about use of 'linked' and linked list, see gforth's chains.fs

: point  ( x y -- n )  \ add point, relative to cursor, return point id
   (points linked  cursor 2@ xy+ 2,
   #points @   1 #points +! ;
: point:  ( x y "name" -- )  \ add point, create named constant giving id
   point CONSTANT ;
: zline  ( n1 n2 z1 --  )  \ add line with color z1
   (lines linked 2>R 2, 2R> 2,   1 #lines +! ;
: cline  ( n1 n2 n3 --  )  \ add line with color n3
   0 zline ;
: line  ( n1 n2 -- )  \ add line between points, using current color
   color @ cline ;
: clineto  ( x y n1 -- n2 )  \ make a new point, connect to last point
   >R point DUP DUP 1- SWAP R> cline ;
: lineto  ( x y -- n )  color @ clineto ;
: clineto:  ( x y n "name" -- )  clineto CONSTANT ;
: lineto:  ( x y n "name" -- )  lineto CONSTANT ;

VARIABLE colors
: defcolor"  ( n ..." -- )
   colors linked ,    [CHAR] " PARSE string,  ;

-3 defcolor" 0.8 0.5 0.8 setrgbcolor"
-2 defcolor" 0.8 0.8 1.0 setrgbcolor"
-1 defcolor" 0.8 0.0 0.0 setrgbcolor"
0 defcolor" 0.5 0.5 0.5 setrgbcolor"
1 defcolor" 0.0 0.0 0.8 setrgbcolor"
2 defcolor" 0.0 0.8 0.8 setrgbcolor"
3 defcolor" 0.8 0.8 0.0 setrgbcolor"
4 defcolor" 0.0 0.8 0.0 setrgbcolor"
5 defcolor" 0.8 0.0 0.8 setrgbcolor"

: drawing>ps  { #points #lines xy{ lines{ zcolors{ -- }
   \ Output postscript code for drawing to terminal (use with outfile-execute
   \ to write a file).  zcolors{ may be real or complex array, format determined
   \ from element-size field at start of array.
   
   \ header, giving position, scaling and line width
   CR ." 10 10 translate  10 10 scale"
   CR ." /Helvetica findfont 0.5 scalefont setfont"
   \ color table
   colors @ BEGIN DUP WHILE
	 CR ." /color" DUP CELL+ @ .
	 ." { "  DUP 2 CELLS + COUNT TYPE ." } def"
	 @
   REPEAT DROP
   \ define points
   #points 0 ?DO
      CR ." /point" I . ." { " xy{ I } 2@ SWAP . . ." } def"
   LOOP
   \ draw lines
   zcolors{ }eltsize 0 ?DO   \ loop over imag,real component
      I INTEGER +  zcolors{ }eltsize =
      CR IF ." 0.1" ELSE ." 0.3" THEN   \ imag part wider (as border)
      ."  setlinewidth"
      #lines 0 ?DO	   \ then loop over lines
	 lines{ I } 2@ SWAP
	 CR ." point" .  ." moveto point" .
	 ." color" zcolors{ I } J + @ . ." lineto stroke"
      LOOP
   INTEGER +LOOP
   \ draw points
   CR ." 0 0 0 setrgbcolor"
   #points 0 ?DO
      CR ." newpath point" I . ." 0.2 0 360 arc fill stroke"
      CR ." newpath point" I . ." moveto 0.2 0.2 rmoveto (" I . ." ) show"
   LOOP
   \ prologue
   CR ." showpage" CR ;

: print-drawing  ( #points #lines xy{ lines{ zcolors{ c-addr u -- )
   W/O CREATE-FILE THROW >R
   ['] drawing>ps R@ OUTFILE-EXECUTE
   R> CLOSE-FILE THROW ;
: printto  ( #points #lines xy{ lines{ zcolors{ "file" -- )
   PARSE-NAME print-drawing ;
: snapshot  ( "name" -- )  \ todo: complex as option?
   \ export current state of current drawing to postscript
   true flush-drawing { #points #lines xy{ lines{ zcolors{ -- }
   #points #lines xy{ lines{ zcolors{ printto
   xy{ }unallot ;

end-module

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth drawing-test.fs  -e bye"
   indent-tabs-mode: t
   End:
[THEN]
