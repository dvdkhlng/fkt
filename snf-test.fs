\ -*-forth-*-
\ test-case for snf.fs
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./tester2.fs
require ./imatrix.fs

T{ require ./snf.fs -> }T

0 VALUE #nodes
0 VALUE matrix{{

\ the example matrix from en.wikipedia.org/wiki/Smith_normal_form 
ALIGN 3 , INTEGER , HERE  
2 , 4 , 4 ,
-6 , 6 , 12 ,
10 , -4 , -16 ,
CONSTANT M{{

T{ M{{ 0 0 }} @ -> 2 }T

T{ 3 3 M{{ }}left-snf -> }T
3 3 M{{ }}iprint

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth snf-test.fs -e bye"
   End:
[THEN]
