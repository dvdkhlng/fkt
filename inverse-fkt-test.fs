\ -*-forth-*-
\ Test inverse fkt (inverse-fkt.fs)
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011


require ./tester2.fs
T{ require ./inverse-fkt.fs -> }T
T{ require ./fkt.fs -> }T

0 VALUE #points
0 VALUE #lines
0 VALUE xy{
0 VALUE lines{

T{ 6. inverse-fkt  TO lines{ TO xy{ TO #lines TO #points  ->  }T
T{ snapshot inverse-fkt-5.ps -> }T
T{ #points #lines xy{ lines{ }#matchings -> 6 }T

\ Todo: augment FKT algorithm to compute correct sign and remove the RNEGATEs
\ below
T{ 7. inverse-fkt  }#matchings -> 7 rnegate }T
T{ 12. inverse-fkt  }#matchings -> 12 }T
T{ 12346. inverse-fkt  }#matchings -> 12346 rnegate }T
T{ snapshot inverse-fkt-12346.ps -> }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth inverse-fkt-test.fs -e bye"
   End:
[THEN]

