\ -*-forth-*-
\ Complex matrix Determinant
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./imatrix.fs

: }}zdet  { n M{{ -- result }
   \ note: destroys the contents of M{{ during computation
   1 0 { d: result }
   n 0 ?DO

      \ search pivot (i.e. swap rows to ensure m(i,i)>0)
      n I ?DO
	 M{{ I J }} 2@ z0<> IF
	    I J <> IF
	       n M{{ I J }}swap-rows
	       result znegate TO result
	    THEN
	    LEAVE
	 THEN
      LOOP

      \ read pivot, adjust result, precompute inverse
      M{{ I I }} 2@   2DUP result z* TO result	( s: pivot)
      2DUP z0= IF		\ zero pivot 
	 UNLOOP EXIT			\ -> abort with determinant zero
      THEN
      zinv { d: p^-1 }

      \ zero columns I in rows below.  todo: optimize loop/factor
      n I 1+ ?DO
	 M{{ I J }} 2@ 2DUP z0<> IF
	    p^-1 z*
	    n J ?DO
	       2DUP M{{ K  I }} 2@ z*  M{{ J I }} z-!
	    LOOP
	    2DROP	    
	 ELSE 2DROP THEN
      LOOP
   LOOP
   result ;


\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth zdet-test.fs -e bye"
   End:
[THEN]
