\ -*-forth-*-
\ FKT Algorithm Implementation
\
\ Copyright (C) 2011,2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./dual.fs
require ./spanning.fs
require ./rpfaffian.fs
require ./zpfaffian.fs
require ./module.fs

module fkt

: }dual-spanning-tree  { #edges edges{ spanadj{{ faces{ -- dedges{ #dedges }
   \ Input: edges of the graph; adjacency map for the spanning tree of the
   \ graph (use spanning-tree + }edges>adjmap); faces{} array describing
   \ the dual graph
   \
   \ Output: edges that form the spanning tree of the dual graph, not
   \ intersecting any edge of the original spanning tree.
   DOUBLE , HERE		( s: dedges{ )
   #edges 0 DO			\ for every edge in graph
      edges{ I } 2@ spanadj{{ }}has-edge? 0= IF   \ that's not in spanning tree
	 faces{ I } 2@ 2,		  	  \ add edge to dual tree
      THEN
   LOOP
   HERE OVER - DOUBLE / ;		\ compute&return #dedges

<private>
\ variables used during (deep) recursion, don't want to put them into locals
0 VALUE edges{
0 VALUE edgemap{{
0 VALUE faces{
0 VALUE faceedgemap{{
0 VALUE edgeorient{	\ orientation correction for every edge
0 VALUE spanadj{{	\ adjacency map of spanning tree

<public>

: (next-boundary-edge)  (  eidx face -- eidx2 )
   \ wrapper around }next-boundary-edge, passing it our module variables
   \ describing the graps.  see }next-boundary-edge documentation for details.
   edges{ edgemap{{ faces{ }next-boundary-edge ;

: }trace-face ( edges{ edgemap{{ faces{ starteidx face -- path{ #path )
   \ Return list of edge-idx that lie on boundary of face, tracing FACE's
   \ boundary in counter-clockwise order.
   \
   \ Sign of edges is negative for edges oriented along the path.  First edge
   \ STARTEIDX's sign is ignored on input and output corrected according to
   \ its orientation along the path.
   \
   \ Note: #path must match 'faceedgemap{{ face } 2@ NIP', i.e.  number of
   \ neighbours of face.
   { eidx face }   TO faces{ TO edgemap{{ TO edges{
   INTEGER , HERE		( s: path{ )
   0  eidx 1abs SWAP		( s: path{ startidx #path )
   BEGIN
      eidx ,   eidx face (next-boundary-edge) TO eidx
      1+	( s: path{ startidx #path+1 )
   OVER eidx 1abs = UNTIL  NIP  ( s: path{ #path )
   OVER eidx SWAP ! ;  \ overwrite STARTEIDX, with eidx that has correct sign

: (face-orientation)  { eidx face -- n }
   \ Operate on private variables xy{ edges{ edgemap{{ faces{ edgeorient{.
   \ Count number of edges on face's boundary that are oriented clock-wise,
   \ starting at edge eidx.  Sign on EIDX is ignored.
   eidx 1abs 0			( s: startidx n)
   BEGIN
      eidx face (next-boundary-edge) TO eidx
      eidx DUP 0>=				( s: startidx n eidx orient)
      edgeorient{ ROT 1abs } C@ 0<> XOR		( add orientation override)
      IF 1+ THEN				\ increment n if clockwise
   OVER eidx 1abs = UNTIL
   NIP ;

: }face-orientation
   ( edges{ edgeorient{  edgemap{{ faces{ starteidx face -- area n )
   \ helper method setting up variables (for testing)
   { eidx face }  TO faces{ TO edgemap{{ TO edgeorient{ TO edges{
   eidx face (face-orientation) ;

: fix-face-orientation { eidx face -- }
   \ toggle direction of EDGE{EIDX} to enforce odd number of clockwise edges
   \ on boundary of FACE .
   eidx face (face-orientation) even? IF
      edgeorient{ eidx } toggle
      \ cannot assert(), when paren ')' is used in commands :o
      \ assert2( eidx face (face-orientation) odd? )
   THEN ;   

\ ATTENTION: we must double-check that the root of the tree is the outside
\ face.  re-orienting the outside face will be extremely expensive and take
\ tons of memory.  Well not too expensive run-time wise but memory wise.
\ correction: current implementation doesn't need memory per face.  No problem
\ any more.
\
\ Other problem is whether orienting the outside face produces a correct
\ pfaffian orientation.  Since we traverse the outside in reverse direction
\ from the inside (see comments in }next-boundary-edge), it just may work, but
\ needs testing.

\ Ok, the problem with which face to start re-orienting may have been solved,
\ however when finally reorienting the outside face instead of the last
\ remaining inside face, the current implementation inverts the sign on some
\ graphs (e.g. the 5-wheel).  Kamenetsky seems to write that what's the outer
\ face is merely an artifact of the drawing and shouldn't matter.  It seems to
\ matter here because clock-wise traversal of the outside face actually
\ traverses in counter-clockwise direction, causung the problem only on graphs
\ with odd number of edges on the outside face.
\
\ How do we guarantee to never iterate over the outside of /any/ (possibly
\ inside) face anyways?  AFAIR te current algorithm allows both kinds of
\ iterations.  The sum of angles would be different, so we could correct
\ afterwards, though sum of angles is an unusable concept in integer
\ arithmetic.
\
\ UPDATE: No, there seems to be no problem WRT re-orientation.  The problem of
\ sign of matchings being miscomputed is inherint in the FKT algorithm:
\ reorienting all faces merely guarantees that all matchings have the same
\ sign in the pfaffian, it does NOT guarantee the sign to be positive.  To fix
\ the sign we'll have to compute the sign of a single matching and compensate
\ accordingly.  Todo...

: (traverse-dual)  ( edge face -- )
   \ recursively traverse dual graph's spanning tree formed by only traversing
   \ edges not crossing the original graph's spanning tree correct orientation
   \ of faces' edges in post-order (i.e. processing leaves that are removed
   \ afterwards)
   \
   \ todo: clean up the edge recomputation mess below.  need convention for
   \ where sign is stripped
   \
   \ Todo: isn't this needlessly complicated?  Can't we just use the spanning
   \ tree of the dual graph and traverse that?  Hmm, are there edge cases
   \ where starting from the original graph's spanning tree is required?  Is
   \ the spanning tree of the dual guaranteed to yield a spanning tree of the
   \ original graph when striking out crossed edges?  Maybe we'd risk
   \ separating the graps?  Puncturing a face twice seems possible.
   \
   \ Is the implementation cheaper when first constructing the full dual
   \ spanning tree, not generating it here on-the-fly?  No it is not.
   
   faceedgemap{{ OVER } 2@	\ iterate over faces connected to face
   }bounds ?DO			( s: edge face )
      \ note how every edge-id in the original graph corresponds to the
      \ "crossing" edge in the dual graph, see comments in }dual-graph
      \ this is why we can the edgeid from faces{} on edges{} here.
      OVER 1abs I @ 1abs <> IF
	 edges{ I @ 1abs } 2@ spanadj{{ }}has-edge? 0= IF
	    \ edge in the dual graph does not cross edge in original spanning
	    \ tree
	    DUP faces{ I @ 1abs } 2@ other-node 	( s: edge face face2 )
	    I @ 1abs SWAP 2DUP RECURSE	\ doing post-order recursion of
					\ dual tree
	    fix-face-orientation
	 THEN
      THEN
   CELL +LOOP
   2DROP ;

: }orient-edges
   ( start-face edges{ edgeorient{ edgemap{{ spanadj{{ faces{ faceedgemap{{ -- )
   \ change orientation of edges in graph so that all faces have odd number
   \ of clockwise edges on boundary.
   \ todo: need to make sure that we start on an edge that crosses from
   \ outside face to the inside, so that the outside face is never reoriented
   TO faceedgemap{{ TO faces{
   TO spanadj{{ TO edgemap{{ TO edgeorient{ TO edges{
   ( s: start-face)
   faceedgemap{{ OVER } 2@ DROP @  ( start-edge)
   SWAP (traverse-dual) ;

: }check-edge-orient
   ( edges{ edgeorient{ edgemap{{ faces{ #edges #faces -- #errors)
   \ check all faces in the graph for whether odd number of clockwise edges on
   \ boundary.
   { #edges #faces }
   TO faces{ TO edgemap{{ TO edgeorient{ TO edges{
   0  ( s: #errors)
   #faces 0 ?DO
      #edges 0 ?DO
	 J faces{ I } 2@ on-edge? IF  \ found an edge on face's boundary
	    I ( eidx)  J ( face) (face-orientation) odd? 0= IF
	       CR ."  face " J . ." , start-edge " I . ." fails test"
	       1+
	    THEN
	    LEAVE
	 THEN
	 assert0( I #edges 1- < )  \ huh, no edge on boundary found?
      LOOP
   LOOP ;

: }fkt-adjmap { #nodes #edges xy{ edges{ -- adjmap{{ }
   \ Compute adjmap for the given graph, with proper orientation of edges so
   \ that pfaffian of adjacency matrix will be #perfmatch.  Returned array can
   \ be freed via }unallot

   \ todo: could compute spanning tree based on adjmap
   \ could compute a _new_ adjmap quicker based on edgemap
   \ (copy edgemap, than edgemap>adjmap?)
   #nodes #edges edges{ }edges>edgemap  { edgemap{{ }
   #nodes #edges edges{ }edges>adjmap   { adjmap{{ }
   DOUBLE , HERE #edges 2* CELLS ALLOT  { faces{ }
   
   \ sort edges for }dual-graph, }next-boundary-edge to work
   #nodes xy{ edges{ edgemap{{ }}sort-edgemap  

   #nodes xy{ #edges edges{ edgemap{{ faces{ }dual-graph { #faces }
   #faces #edges faces{ }edges>edgemap  { faceedgemap{{ }
   
   adjmap{{ #nodes spanning-tree	{ span{ #span }
   #nodes #span span{ }edges>adjmap     { spanadj{{ }

   1 CHARS ,  HERE #edges CHARS 0allot  ALIGN	{ orient{ }  
   \ todo: using 0 instead of outer-face has same result, with same bad sign
   \ in inverse-fkt-test.  Check result of orient-edges.  Are we right, and
   \ the algo just can't get the sign right or does }}orient-edges somehow
   \ screw up?
   #faces #edges xy{ edges{ edgemap{{ faces{ faceedgemap{{ }outer-face
   edges{ orient{ edgemap{{ spanadj{{ faces{ faceedgemap{{ }orient-edges

   \ CR orient{ #edges bounds ?DO  I C@ . LOOP
   
   \ convert graph's edgemap into an orientation-corrected adjmap that we can
   \ return
   #nodes orient{ edges{ edgemap{{ }}edgemap>adjmap'

   \ free all temporary structures that were allocated "on top"
   adjmap{{ }unallot
   edgemap{{ ;

: fkt-edges>matrix  { #nodes #edges xy{ edges{ complex? -- matrix{{ }
   \ Generate adjacency matrix in Pfaffian orientation of given planar graph.
   \ When selecting complex operation, a matrix of correct element-size is
   \ returned, but adjacency matrix is only written to complex parts of its
   \ elements (yielding a (0,i,-i) matrix with unknown real values)
   #nodes , INTEGER complex? IF 2* THEN , HERE        { matrix{{ }
   #nodes DUP * complex? IF 2* THEN CELLS ALLOT
   #nodes #edges xy{ edges{ }fkt-adjmap	           { fktadj{{ }
   #nodes fktadj{{ matrix{{  1 dup rnegate (}}adjmap>+-0matrix)
   matrix{{ ;

: }}*edge-weigths { #edges edges{ weights{ matrix{{ -- }
   \ add edge weights to adjacency matrix
   #edges 0 ?DO				
      2 0 ?DO				\ each edge has two entries in matrix
	 matrix{{ edges{ J } 2@ I IF SWAP THEN }} DUP @
	 weights{ J } @ r*  SWAP !	\ todo: modulo arithmetic?
      LOOP
   LOOP ;

: }}*edge-zweigths { #edges edges{ zweights{ matrix{{ -- }
   \ Add complex (!) edge weights to (+i,-i,0) adjacency matrix.  Real part of
   \ matrix entries treated as zero.
   #edges 0 ?DO				
      2 0 ?DO				\ each edge has two entries in matrix
	 matrix{{ edges{ J } 2@ I IF SWAP THEN }} DUP @ 0
	 zweights{ J } 2@ z*  ROT 2!
	 assert3( zweights{ J } 2@  zok? )
      LOOP
   LOOP ;

: fkt-wedges>matrix { #nodes #edges xy{ edges{ (z)weights{ complex? -- matrix{{ }
   \ Return weighted, pfaffian-oriented adjacency matrix corresponding to the
   \ planar graph given by XY and EDGES with weights taken from (Z)WEIGHTS{.
   \ Returned MATRIX{{ is of dimension #NODES x #NODES and has complex-valued
   \ edges when COMPLEX? is true.  When (Z)WEIGHTS{ has an address of 0,
   \ assume weights of 1.  COMPLEX? must be false when no weights are
   \ provided.  Returned matrix{{ is allotted at HERE, so use }}unallot to
   \ free.
   #nodes #edges xy{ edges{ complex? fkt-edges>matrix		{ matrix{{ }
   (z)weights{ IF
      #edges edges{ (z)weights{ matrix{{
      complex? IF }}*edge-zweigths ELSE }}*edge-weigths THEN
   THEN
   matrix{{ ;

: (}#weighted-matchings) { #nodes #edges xy{ edges{ (z)weights{ complex? -- n }
   \ Compute the number of weighted perfect matchings for given planar graph.
   #nodes #edges xy{ edges{ (z)weights{ complex? fkt-wedges>matrix { matrix{{ }
   #nodes matrix{{ complex? IF }}zpfaffian ELSE }}rpfaffian THEN
   matrix{{ }unallot ;

: }#matchings ( #nodes #edges xy{ edges{ -- n )
   \ Compute the number of perfect matchings for given planar graph.
   0 false (}#weighted-matchings) ;

: }#weighted-matchings ( #nodes #edges xy{ edges{ weights{ -- n )
   \ Compute the number of weighted perfect matchings for given planar graph.
   assert( DUP }eltsize INTEGER = )
   false (}#weighted-matchings) ;

: }#zweighted-matchings ( #nodes #edges xy{ edges{ zweights{ -- n1 n2 )
   \ Compute the number of weighted perfect matchings for given planar graph.
   \ Implements complex arithmetic on weights.
   assert( DUP }eltsize DOUBLE = )
   true (}#weighted-matchings) ;
   
end-module

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth fkt-test.fs -e bye"
   End:
[THEN]

