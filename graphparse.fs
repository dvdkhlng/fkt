\ -*-forth-*-
\ Parser for simple graph description
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Oct 2012
\
\ Some code fragments ripped out of Gray4's example files, as noted below.
\ Those are Copyright (C) Anton Ertl.

\ Note: word collisions: }} also defined by FSL, loading gray.fs last works
\ around any problems for now.
require ./drawing.fs

module graphparse

<private>
require gray.fs

<public>
2VARIABLE graph-filename  \ name of graph currently parsed
s" Error parsing graph" exception CONSTANT #parser-error

<private>
: (.graph-error)  ( c-addr u -- )
   CR graph-filename 2@ TYPE
   ." :" sourceline# 0 .R
   ." :" >IN @ 1+ 0 .R
   ." : " TYPE CR ;
: .graph-error  ( c-addr u -- )
   ['] (.graph-error) stderr outfile-execute ;

\
\ I/O interface to Gray parser: token checker and token consumer
\
VARIABLE eof?

: test?  ( set -- flag )  \ check whether set is matched by next "token"
   \ CR ." {" 255 0 ?DO  DUP I member? IF I EMIT THEN LOOP ." }"
   SOURCE >IN @ /STRING IF C@
   ELSE DROP eof? @ IF 255 ELSE 10 THEN  \ line end: give CR or EOF character
   THEN
   \ CR ." <" DUP EMIT ." >" DUP .
   member? ;
: check&read  ( flag -- ) 
   0= IF
      S" parser error"  .graph-error
      #parser-error throw
   THEN
   >IN @ SOURCE NIP <  IF  1 >IN +!
   ELSE REFILL 0= eof? ! THEN ;
: parsed  ( n -- c-addr u )
   \ given value of '>IN @' at some time return c-addr u of string parsed
   \ since then
   SOURCE DROP OVER +  >IN @ ROT - ;

' test? test-vector !

\
\ Helpers to define grammar terminals (borrowed from Gray's mini.fs example)
\
: ` ( "char" -- terminal )
   \ create anonymous terminal for the character c 
   CHAR singleton ['] check&read make-terminal ;
: .. ( c1 c2 -- set )
   \ create set that includes the characters c, c1<=c<=c2 )
   empty copy-set
   swap 1+ rot DO
      i over add-member
   LOOP ;
   
\
\ Grammar
\
255 max-member

char 0 char 9 ..  ' check&read terminal <digit>
0 32 .. ' check&read terminal <blank>
14 254 ..   ( 9 over add-member) ' check&read terminal <^\n>
10 13 ..  ' check&read terminal <\n>
255 singleton ' check&read terminal <eof>

(( ` n ` o ` d ` e  <blank> ** )) <- "node"
(( ` e ` d ` g ` e  <blank> ** )) <- "edge"
(( ` i <blank> ** )) <- "i"
(( ` + <blank> ** )) <- "+"
(( ` - <blank> ** )) <- "-"
(( ` ( <blank> ** ))  <- "("
(( ` ) <blank> ** ))  <- ")"
(( ` , <blank> ** ))  <- ","

(( ` # <^\n> ** (( <\n> <blank> ** || <eof> )) )) <- [comment]

(( {{ >IN @  }}
   <digit> ++
   {{ parsed s>unumber? 2DROP }}
   <blank> ** 
)) <- [unumber]

(( "-" [unumber] {{ NEGATE }} || [unumber] )) <- [number]

(( [number]  ( real component)
   ((        ( imaginary component:)
      (( 
	 "+" [number] "i" ||
	 "-" [number] "i" {{ NEGATE }}
      )) ||
      {{ 0 }}  ( if imaginary part missing, just assume zero)
   ))
)) <- [znumber]

((
   "node" "(" [number] "," [number] ")" {{ point DROP }}
)) <- [node]

((
   "edge" "(" [number] "," [number]
   (( "," [znumber] || {{ 1 0 }} )) ")"
   {{ zline }}
)) <- [edge]

((
   <blank> ** (( [node] || [edge] || [comment] )) ** <eof>
)) <- [graph]

[graph] parser parsegraph

\
\ Parse files
\
: (parsegraph)  ( -- ( -- #points #lines xy{ lines{ zcolors{ )
   REFILL DROP
   eof? off
   draw
   parsegraph ;

<public>
: parse-graph  ( c-addr u -- #points #lines xy{ lines{ zcolors{ -- )
   \ currently throws on parse-errors, parse-errors directly output to stderr
   \ before throwing
   2DUP graph-filename 2!
   R/O OPEN-FILE  THROW  { fid }
   fid ['] (parsegraph) execute-parsing-file ;   

end-module

\ Customize Emacs highlighting and indentation for this file
0 [IF]
   Local Variables:
   compile-command: "gforth ./graphparse-test.fs -e bye"
   forth-local-words:
   (
    (("{{") definition-starter (font-lock-keyword-face . 1))
    (("}}") definition-ender (font-lock-keyword-face . 1))
    (("((" "))") non-immediate (font-lock-keyword-face . 1))
    (("`") immediate (font-lock-keyword-face . 2)
     "[ \t\n]" t name (font-lock-string-face . 3))
    (("t'" "<-") non-immediate (font-lock-keyword-face . 1)
    "[ \t\n]" t name (font-lock-variable-name-face . 1))
    )
   forth-local-indent-words:
   ((("((" "{{") (0 . 2) (0 . 2))
    (("))" "}}") (-2 . 0) (0 . -2)))
   End:
[THEN]

