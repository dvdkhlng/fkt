\ -*-forth-*-
\ Load Gforth's FSL array routines, suppressing bogus output during loading
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Nov 2012

VOCABULARY quiten-fsl
GET-CURRENT  ALSO quiten-fsl DEFINITIONS PREVIOUS
: CR ;
: .(  [COMPILE] ( ;
SET-CURRENT

ALSO quiten-fsl
require fsl-util.4th
PREVIOUS
