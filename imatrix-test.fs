\ -*-forth-*-
\ Integer matrix routines test-case
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./tester2.fs

T{ require ./imatrix.fs -> }T

T{ 3 INTEGER MARRAY tt{ -> }T

T{ 12 tt{ 0 }  !  34 tt{ 1 }  !  56 tt{ 2 }  ! -> }T

T{ cr 3 tt{ }iprint -> }T

T{ 3 2 INTEGER MMATRIX tt{{ -> }T

T{ 12 tt{{ 0 0 }}  !  34 tt{{ 0 1 }}  !  
T{ 42 tt{{ 1 0 }}  !  64 tt{{ 1 1 }}  !  
T{ 52 tt{{ 2 0 }}  !  94 tt{{ 2 1 }}  !  

T{ cr 3 2 tt{{ }}iprint -> }T
T{ cr 6 tt{{ }iprint -> }T

3 2 tt{{ }}ifill:
  77 88
  99 11
  22 33
T{ cr 3 2 tt{{ }}iprint -> }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth imatrix-test.fs -e bye"
   End:
[THEN]
