\ -*-forth-*-
\ Pfaffian test
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Oct 2012

require ./tester2.fs
T{ require ./rpfaffian.fs -> }T

max-modulus modulus !

0 VALUE M{{

4 , INTEGER ,  HERE TO M{{
0  ,  1 ,  0 ,  0 ,
-1 ,  0 ,  1 ,  0 ,
0  , -1 ,  0 ,  1 ,
0  ,  0 , -1 ,  0 ,

T{ 4 M{{ }}rpfaffian -> 1 }T

4 , INTEGER ,  HERE TO M{{
0  ,  5 , -3 , -7 ,
-5 ,  0 , -4 ,  0 ,
3  ,  4 ,  0 ,  1 ,
7  ,  0 , -1 ,  0 ,

37 modulus !

T{ 4 M{{ }}rpfaffian -> 33 }T

4 , INTEGER ,  HERE TO M{{
0  ,  0 , -3 , -7 ,
0  ,  0 , -4 ,  5 ,
3  ,  4 ,  0 ,  1 ,
7  , -5 , -1 ,  0 ,

T{ 4 M{{ }}rpfaffian -> 6 }T  ( 43 mod 37)

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth rpfaffian-test.fs -e bye"
   End:
[THEN]
