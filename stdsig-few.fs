\ -*-forth-*-
\ Holographic standard signature fastest evaluation in the west.
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA posteo TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: July 2021
\
\ See also stdsig.fs
require  ./stdsig.fs

: stdsig-few-ref { n values{ sig{ -- }
   \ same as stdsig-n-eval but different loop structure.  Reference
   \ for the unrolled factors below.
   sig{  n 2** CELLS ERASE
   1 sig{ 0 } !
   0 sig{ 1 } !
   
   n 1 ?DO
      I DUP 1- * 2/   { voffs } \ start offset for variables x_n added here
      I 0 ?DO  \ iterate over all possible connections of new input "n+1"
         values{ voffs I + } @   ( s: coeff )
         J 2** 0 ?DO
            I J 2** AND IF
               DUP
               sig{ I  J 2** INVERT AND } @ *
               I J signadjust *
               sig{ I  K 2** + } +!
            THEN
         LOOP
         DROP
      LOOP
   LOOP ;

16 CONSTANT #FEW-BS

: (updatex16-col0)  ( rptr wptr coeff  -- )
   \ pattern: 0 1 0 1
   >R
   [ 8 0 ] [DO]
      OVER @ R@ * OVER +!
      CELL+ CELL+   [ 2 CELLS ]L UNDER+
   [LOOP]
   2DROP RDROP ; 

: (updatex16-col0)  ( rptr wptr coeff  -- )
   \ pattern: 0 1 0 1
   ]] >R 
   [[ 8 0 DO ]]
      OVER @ R@ * OVER +!
      CELL+ CELL+   [[ 2 CELLS ]] LITERAL UNDER+
   [[ LOOP ]]
   2DROP RDROP [[ ;  IMMEDIATE

: (updatex16-col1)  ( rptr wptr coeff  -- )
   \ pattern:  0 0 1 1 0 0 1 1
   ]] >R
   [[ 8 0 DO ]]
      OVER @ R@ * [[ I 1 AND IF ]] NEGATE [[ THEN ]]
      OVER +!
      [[ I 1 AND 1 = IF ]]
         [[ 3 CELLS ]] LITERAL +
         [[ 3 CELLS ]] LITERAL UNDER+
      [[ ELSE ]]
         CELL+  CELL UNDER+ 
      [[ THEN ]]
   [[ LOOP ]]
   2DROP RDROP [[ ; IMMEDIATE

: (updatex16-col2)  ( rptr wptr coeff  -- )
   \ pattern:  0 0 0 0 1 1 1 1
   >R
   [ 8 0 ] [DO]
      OVER @ R@ * [ [I] 3 AND parity ] [IF] NEGATE [THEN]
      OVER +!
      [ [I] 3 AND 3 = ] [IF]
         [ 5 CELLS ]L +
         [ 5 CELLS ]L UNDER+
      [ELSE]
         CELL+  CELL UNDER+
      [THEN]
   [LOOP]
   2DROP RDROP ;

: (updatex16-col2)  ( rptr wptr coeff  -- )
   \ pattern:  0 0 0 0 1 1 1 1
   ]] >R
   [[ 8 0 DO ]]
      OVER @ R@ * [[ I 3 AND parity IF ]] NEGATE [[ THEN ]]
      OVER +!
      [[ I 3 AND 3 = IF ]]
         [[ 5 CELLS ]] LITERAL +
         [[ 5 CELLS ]] LITERAL UNDER+
      [[ ELSE ]]
         CELL+  CELL UNDER+
      [[ THEN ]]
   [[ LOOP ]]
   2DROP RDROP [[ ; IMMEDIATE

: (updatex16-col3)  ( rptr wptr coeff  -- )
   \ pattern:  8*0 8*1
   ]] >R
   [[ 8 0 DO ]]
      OVER @ R@ * [[ I 7 AND parity IF ]] NEGATE [[ THEN ]]
      OVER +!
      [[ I 7 AND 7 = IF ]]
         [[ 9 CELLS ]] LITERAL +
         [[ 9 CELLS ]] LITERAL UNDER+
      [[ ELSE ]]
         CELL+  CELL UNDER+
      [[ THEN ]]
   [[ LOOP ]]
   2DROP RDROP [[ ; IMMEDIATE

: (updatex16-col4+)  ( rptr wptr coeff  -- )
   \ pattern: 16*0 or more.
   \ note: about 2k bytes, about 400 uOps
   ]] >R
   [[ 16 0 DO ]]
      OVER @ R@ * [[ I parity IF ]] NEGATE [[ THEN ]]
      OVER +!
      CELL+  CELL UNDER+
   [[ LOOP ]]
   2DROP RDROP [[ ; IMMEDIATE

: hparity-sign  ( x -- -1|+1 )
   \ limited parity function on X.  Parity=1 -> return -1
   \ this computes parity of bits 19..4 only
   \ (lower bits not needed, as that part is unrolled into the updatex16-
   \ familiy of functions above)
   ]]
   DUP 8 RSHIFT XOR
   DUP 4 RSHIFT XOR
   DUP 2 RSHIFT XOR
   DUP 2/ XOR
   3 RSHIFT [[  \ (i.e. log2( #FEW-BS ) -1 ) 
   ]] 2 AND 1- NEGATE [[ ; IMMEDIATE

: stdsig-few-lift { n values{ sig{ -- }
   \ given the standard signature sig{} of arity N for weights values{},
   \ extend sig{} to the corresponding N+1 standard signature.
   \ Make sure that values{} and sig{} are of sufficient size.
   \ Due use of +! make sure part of sig{} to be populated is zero on entry.
   \
   \ Due to unrolling, this word only works for n>=4.
   values{ n DUP 1- * 2/ CELLS +  { >values } \ start addr for variables x_m 
   n 2** CELLS  { stride }
      
   \ treat all possible connections of new input "I" to inputs 0..(I-1)
   
   \ special cases 0..3: connection to column 0..3 these updates are
   \ non-conditional (as n>4) and can thus be easily merged into a single
   \ loop
   sig{ stride BOUNDS ?DO  \ todo: loop over addr offsets or memory
      I 
      I CELL+  stride + 
      >values @   (updatex16-col0)
      I 
      I [ 2 CELLS ]L +  stride +
      >values CELL+ @   (updatex16-col1)
      I 
      I [ 4 CELLS ]L +  stride +
      >values [ 2 CELLS ]L + @   (updatex16-col2)
      I
      I [ 8 CELLS ]L +  stride +
      >values [ 3 CELLS ]L + @   (updatex16-col3)
   [ #FEW-BS CELLS ]L +LOOP

   \ special case (?): connection to column 4
   n 4 > IF
      >values [ 4 CELLS ]L + @  { coeff }
      sig{ stride BOUNDS ?DO
         I 
         I [ 16 CELLS ]L +  stride +
         coeff  (updatex16-col4+)
      [ 32 CELLS ]L +LOOP  \ iterate to next index w/ bit4 unset

      \ special case (?): connection to column 5
      n 5 > IF
         >values [ 5 CELLS ]L + @  TO coeff 
         sig{ stride BOUNDS ?DO  
            I
            I [ 32 CELLS ]L +  stride +
            coeff  (updatex16-col4+)
            I [ 16 CELLS ]L + 
            I [ 48 CELLS ]L +  stride +
            coeff NEGATE  (updatex16-col4+)
         [ 64 CELLS ]L +LOOP  \ iterate to next index w/ bit5 unset
         
         \ special case (?): connection to column 6+
         n 6 > IF
            n 6 ?DO
               >values I CELLS + @ TO coeff
               I 2** CELLS { >colbit }
               
               sig{ stride BOUNDS ?DO
                  
                  J 2** 0 ?DO
                     
                     J   I CELLS +   \ rptr bit "col" unset
                     J   I CELLS + >colbit + stride +  \  wptr: bit "col" set
                     coeff I hparity-sign *
                     (updatex16-col4+)
                     
                  #FEW-BS +LOOP
                  
               J 1+ 2** CELLS +LOOP \ iterate to next index w/ bit "col" unset
               
            LOOP
         THEN
      THEN
   THEN ;

: stdsig-few { n values{ sig{ -- }
   \ For this attempts to keep and instances of "NEXT" out of the main loops.
   \ i.e. everything that cannot go into dynamic superinstructions, such as
   \ function calls and branches.

   \ todo: need to add another loop layer to keep memory access patterns more
   \ local
   
   \ start with empty array as accumulating with +! below.
   sig{  n 2** CELLS ERASE

   \ pre-seed first 16 rows using base version
   n 4 MIN values{ sig{ stdsig-n-eval'
   n 4 <= ?EXIT
   n 4 ?DO
      I values{ sig{ stdsig-few-lift
   LOOP ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth-fast -m 1G stdsig-few-test.fs -e bye"
   End:
[THEN]