\ -*-forth-*-
\ test equivalence class processing 
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./equiv.fs -> }T

DOUBLE ,  HERE  1 3 2,   7 5 2,   6 4 2,   8 9 2,   1 2 2,   4 9 2,   5 2 2,
CONSTANT equivs{   7 CONSTANT #equivs
10 CONSTANT #elts   #elts INTEGER MARRAY map{

T{ #equivs equivs{ #elts map{ }equivs>protomap -> }T
T{ map{ 0 } @ -> 0 }T
T{ map{ 1 } @ -> 1 }T
T{ map{ 2 } @ -> 1 }T
T{ map{ 3 } @ -> 1 }T
T{ map{ 5 } @ -> 1 }T
T{ map{ 7 } @ -> 1 }T
T{ map{ 4 } @ -> 4 }T
T{ map{ 8 } @ -> 4 }T
T{ map{ 6 } @ -> 4 }T
T{ map{ 9 } @ -> 4 }T

T{ #elts map{ }protomap>idmap -> 3 }T
CR #elts map{ }iprint
T{ map{ 0 } @ -> 0 }T
T{ map{ 1 } @ -> 1 }T
T{ map{ 7 } @ -> 1 }T
T{ map{ 4 } @ -> 2 }T
T{ map{ 6 } @ -> 2 }T
T{ map{ 9 } @ -> 2 }T

0 VALUE mem0
0 VALUE mem1
HERE TO mem0
T{ #equivs equivs{ #elts map{ }equivs>idmap -> 3 }T
HERE TO mem1
T{ mem1 mem0 - -> 0 }T
CR #elts map{ }iprint
T{ map{ 0 } @ -> 0 }T
T{ map{ 1 } @ -> 1 }T
T{ map{ 7 } @ -> 1 }T
T{ map{ 4 } @ -> 2 }T
T{ map{ 6 } @ -> 2 }T
T{ map{ 9 } @ -> 2 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth equiv-test.fs -e bye"
   End:
[THEN]
