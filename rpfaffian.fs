\ -*-forth-*-
\ Matrix Pfaffian
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Oct 2012

require ./module.fs
require ./imatrix.fs
require ./mod.fs
require ./remt.fs

module rpfaffian
<private>

: }}rc-exchange { n M{{ j k -- }  \ simultaneous row+column exchange
   n M{{ j k }}swap-cols
   n M{{ j k }}swap-rows ;

: }}rc-scaleadd { a n M{{ j c k -- }
   \ add multiple of partial row&column j to partial row&column k.
   \ Only touch elements c..c+n-1 on each row/column
   a n M{{ j c k (}}rrowadd)
   a n M{{ c j k (}}rcoladd)
   ( Cr n n M{{ }}iprint) ;

<public>
: }}rpfaffian { n M{{ -- result }
   \ note: destroys the contents of M{{ during computation
   n 1 AND IF  0 EXIT   \ abort early on odd dimension, code below can't cope
   THEN                 \ in that case pfaffian is 0 anyways
   1 { result }
   n 1 ?DO		\ iterating over odd I: 1, 3,.., n-1
      \ search pivot (i.e. swap columns to ensure M(i-1,i)>0)
      n I ?DO
	 M{{ J 1- I }} @ IF
	    I J <> IF  n M{{ I J }}rc-exchange
	       result rnegate TO result
	    THEN
	    LEAVE
	 THEN
      LOOP

      \ read pivot, adjust result, precompute inverse
      M{{ I 1- I }} @                             ( s: pivot)
      DUP result r* TO result                     ( s: pivot)
      DUP 0= IF   UNLOOP EXIT  THEN   \ zero pivot -> return pfaffian=0
      rinv { p^-1 }
 
      \ zero column I-1 in rows below and row I-1 in colums right.  Note how
      \ zeroing even rows /and/ colums is sufficient: there will be no
      \ permutation (think: matching) containing any of the odd rows/columns
      \ apart from the subdiagonal.  For this reason row/column update will
      \ also ignore even rows/colums at indices <I-1
      n I 1+ ?DO
	 M{{ J 1- I }} @ ?DUP IF
	    p^-1 r* rnegate n J 1- - M{{ J  J 1-  I }}rc-scaleadd
	 THEN
      LOOP
   2 +LOOP			\ iterate over odd I
   result ;

\ Customize Emacs highlighting and indentation for this file
0 [IF]
   Local Variables:
   compile-command: "gforth ./rpfaffian-test.fs -e bye"
   End:
[THEN]

