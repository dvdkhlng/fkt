\ -*-forth-*-
#! /usr/bin/gforth

warnings off
require ./fkt.fs

\ define the graph from the FKT_algorithm wiki entry

\ Coordinates of graph nodes (planar embedding)
10 CONSTANT #nodes   		\ make sure this reflects number of nodes
DOUBLE ,  HERE
0 5 2,  1 5 2,  2 5 2,          \ tuples of x-y coordinates followed by '2,'
0 4 2,  1 4 2,  2 4 2,
0 3 2,  1 3 2,  2 3 2,
        1 2 2,
CONSTANT xy{  

\ Edges
14 CONSTANT #edges              \ make sure this reflects number of edges
DOUBLE ,  HERE
0 1 2,  1 2 2,                  \ tuples of point-indices followed by '2,'
0 3 2,  1 4 2,  2 5 2,
3 4 2,  4 5 2,
3 6 2,  4 7 2,  5 8 2,
7 8 2,
6 9 2,  7 9 2,  8 9 2, 
CONSTANT edges{  

\ Weights
INTEGER , HERE                  \ weights corresponding to edges above
1 , 1 ,                         \ each edge weight followed by a ','
1 , 1 , 1 ,
1 , 1 ,
1 , 1 , 1 ,
1 , 
1 , 1 , 1 ,
CONSTANT weights{

\ Export graph to postscript file named "graph.ps"
require ./drawing.fs
#nodes #edges xy{ edges{ weights{ s" graph.ps" print-drawing

\ Compute number of matchings and output to stdout
CR .( #matchings: )
#nodes #edges xy{ edges{ weights{ }#weighted-matchings .

\ Or try with complex weights
DOUBLE , HERE                    \ weights corresponding to edges above
1 0 2,  1 0 2,                   \ each 'real imag' tuple followed by '2,'
1 0 2,  1 0 2,  1 0 2, 
1 0 2,  1 0 2, 
1 0 2,  1 0 2,  1 0 2, 
1 0 2,  
1 0 2,  1 0 2,  1 0 2,
CONSTANT zweights{

\ Export graph with complex weigtks to postscript file named "zgraph.ps"
#nodes #edges xy{ edges{ zweights{ s" zgraph.ps" print-drawing

\ Compute number of matchings using complex, output to stdout
CR .( "complex" #matchings: )
#nodes #edges xy{ edges{ zweights{ }#zweighted-matchings  SWAP . . 

\ Quit
CR BYE
