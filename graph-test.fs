\ -*-forth-*-
\ test graph code
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
require ./misc.fs

T{ require ./graph.fs -> }T

\ minor stuff

T{ 1 2 3 on-edge? -> false }T
T{ 2 2 3 on-edge? -> true }T
T{ 3 2 3 on-edge? -> true }T
T{ 3 2 3 other-node -> 2 }T
T{ 2 2 3 other-node -> 3 }T
T{ 1 1 3 other-node -> 3 }T
T{ 2 2 3 other-node' -> 3 1negate }T
T{ 3 2 3 other-node' -> 2 }T

\ edge/neighbour handling

\ define the planar graph from wiki/Dual_graph
5 CONSTANT #nodes
DOUBLE ,  HERE  0 2 2,  2 2 2,  3 1 2,  2 0 2,  0 0 2,
CONSTANT xy{  

DOUBLE ,  HERE  0 1 2,  1 2 2,  2 3 2,  1 3 2,  3 4 2,  4 0 2,  1 4 2,  
CONSTANT edges{  7 CONSTANT #edges

0 VALUE edgemap{{
0 VALUE adjmap{{
0 VALUE outmap{{
0 VALUE inmap{{
0 VALUE outedgemap{{
0 VALUE inedgemap{{
0 VALUE exy{
0 VALUE #idxs
0 VALUE idxs{

T{ 3 #edges edges{ }list-neighbours
CONSTANT #neighbours CONSTANT neighbours{ -> }T
T{ #neighbours -> 3 }T
T{ neighbours{ 0 } @  -> 2 }T
T{ neighbours{ 1 } @  -> 1 }T
T{ neighbours{ 2 } @  -> 4 }T

T{ 3 #edges edges{ }list-neighbours'
CONSTANT #neighbours' CONSTANT neighbours'{ -> }T
T{ #neighbours' -> 3 }T
T{ neighbours'{ 0 } @  -> 2 }T
T{ neighbours'{ 1 } @  -> 1 }T
T{ neighbours'{ 2 } @  -> 4 1negate }T

T{ 4 #edges edges{ }list-edges' TO #idxs TO idxs{ -> }T
T{ #idxs -> 3 }T
T{ idxs{ 0 } @ -> 4 }T
T{ idxs{ 1 } @ -> 5 1negate  }T
T{ idxs{ 2 } @ -> 6 }T

T{ 3 #edges edges{ }list-edges'  TO #idxs TO idxs{  -> }T
T{ #idxs -> 3 }T
T{ edges{ idxs{ 0 } @ }+-@node -> 2 }T
T{ edges{ idxs{ 1 } @ }+-@node -> 1 }T
T{ edges{ idxs{ 2 } @ }+-@node -> 4 }T

T{ #idxs idxs{ xy{ edges{ }list-edge-xys  TO exy{  -> }T
T{ exy{ 0 } 2@ -> 3 1 }T
T{ exy{ 1 } 2@ -> 2 2 }T
T{ exy{ 2 } 2@ -> 0 0 }T

T{ #idxs idxs{ xy{ edges{ }list-edge-vs  CONSTANT vs{  -> }T
T{ vs{ 0 } 2@ -> 1 1 }T
T{ vs{ 1 } 2@ -> 0 2 }T
T{ vs{ 2 } 2@ -> -2 0 }T

T{ #idxs idxs{ xy{ edges{ }list-edge-xys  TO exy{  -> }T
T{ exy{ 0 } 2@ -> 3 1 }T
T{ exy{ 1 } 2@ -> 2 2 }T
T{ exy{ 2 } 2@ -> 0 0 }T

5 TO #idxs
INTEGER ,  HERE  2 , 4 , 5 ,  0 , 1 ,  TO idxs{
T{ #idxs idxs{ xy{ edges{ }list-path-xys  TO exy{  -> }T
T{ exy{ 0 } 2@ -> 3 1 }T
T{ exy{ 1 } 2@ -> 2 0 }T
T{ exy{ 2 } 2@ -> 0 0 }T
T{ exy{ 3 } 2@ -> 0 2 }T
T{ exy{ 4 } 2@ -> 2 2 }T
T{ exy{ 5 } 2@ -> 3 1 }T

T{ 2 3 #edges edges{ }find-edge -> 2 }T
T{ 3 2 #edges edges{ }find-edge -> 2 }T

\ test adj-maps
T{ #nodes #edges edges{ }edges>adjmap  TO adjmap{{ -> }T
T{ adjmap{{ 0 } 2@ NIP  ->  2 }T
T{ adjmap{{ 1 } 2@ NIP  ->  4 }T
T{ adjmap{{ 2 } 2@ NIP  ->  2 }T
T{ adjmap{{ 3 } 2@ NIP  ->  3 }T
T{ adjmap{{ 4 } 2@ NIP  ->  3 }T

T{ adjmap{{ 0 } 2@ DROP  @+ @+ DROP ->  1  4 1negate }T
T{ adjmap{{ 1 } 2@ DROP  @+ @+ @+ @+ DROP ->  0 1negate  2 3 4  }T
T{ adjmap{{ 4 } 2@ DROP  @+ @+ @+ DROP ->  3 1negate  0  1 1negate }T

T{ 0 1 adjmap{{ }}has-edge? -> true }T
T{ 0 2 adjmap{{ }}has-edge? -> false }T
T{ 0 4 adjmap{{ }}has-edge? -> true }T
T{ 1 2 adjmap{{ }}has-edge? -> true }T
T{ 1 3 adjmap{{ }}has-edge? -> true }T
T{ 1 4 adjmap{{ }}has-edge? -> true }T
T{ 2 3 adjmap{{ }}has-edge? -> true }T
T{ 2 1 adjmap{{ }}has-edge? -> true }T
T{ 2 0 adjmap{{ }}has-edge? -> false }T
T{ 2 4 adjmap{{ }}has-edge? -> false }T

\ outmaps/inmaps
\ test adj-maps
T{ #nodes #edges edges{ }edges>outmap  TO outmap{{ -> }T
T{ outmap{{ 0 } 2@ NIP  ->  1 }T
T{ outmap{{ 1 } 2@ NIP  ->  3 }T
T{ outmap{{ 2 } 2@ NIP  ->  1 }T
T{ outmap{{ 3 } 2@ NIP  ->  1 }T
T{ outmap{{ 4 } 2@ NIP  ->  1 }T

T{ outmap{{ 0 } 2@ DROP  @ ->  1  }T
T{ outmap{{ 1 } 2@ DROP  @+ @+ @+ DROP ->  2 3 4 }T
T{ outmap{{ 4 } 2@ DROP  @ ->  0 }T
T{ #nodes outmap{{ }}sort-map> -> }T
T{ outmap{{ 1 } 2@ DROP  @+ @+ @+ DROP ->  4 3 2 }T

T{ #nodes #edges edges{ }edges>inmap  TO inmap{{ -> }T
T{ inmap{{ 0 } 2@ NIP  ->  1 }T
T{ inmap{{ 1 } 2@ NIP  ->  1 }T
T{ inmap{{ 2 } 2@ NIP  ->  1 }T
T{ inmap{{ 3 } 2@ NIP  ->  2 }T
T{ inmap{{ 4 } 2@ NIP  ->  2 }T

T{ inmap{{ 0 } 2@ DROP  @ ->  4  }T
T{ inmap{{ 1 } 2@ DROP  @ ->  0 }T
T{ inmap{{ 3 } 2@ DROP  @+ @+ DROP ->  2 1 }T
T{ inmap{{ 4 } 2@ DROP  @+ @+ DROP ->  3 1 }T
T{ #nodes inmap{{ }}sort-map< -> }T
T{ inmap{{ 3 } 2@ DROP  @+ @+ DROP ->  1 2 }T

\ test edge-maps
T{ #nodes #edges edges{ }edges>edgemap  TO edgemap{{ -> }T
T{ edgemap{{ 0 } 2@ NIP  ->  2 }T
T{ edgemap{{ 1 } 2@ NIP  ->  4 }T
T{ edgemap{{ 2 } 2@ NIP  ->  2 }T
T{ edgemap{{ 3 } 2@ NIP  ->  3 }T
T{ edgemap{{ 4 } 2@ NIP  ->  3 }T

T{ edgemap{{ 0 } 2@ DROP  @+ @+ DROP ->  0 1negate  5 }T
T{ edgemap{{ 1 } 2@ DROP  @+ @+ @+ @+ DROP ->
0  1 1negate  3 1negate  6 1negate }T
T{ edgemap{{ 4 } 2@ DROP  @+ @+ @+ DROP ->  4  5 1negate  6 }T

T{ edgemap{{ 0 } 2@ SWAP xy{ edges{ }list-edge-vs CONSTANT edge-vs{ -> }T
T{ edge-vs{ 0 } 2@ -> 2 0 }T
T{ edge-vs{ 1 } 2@ -> 0 -2 }T

T{ 0 1 edges{ edgemap{{ }}has-edge?' -> true }T
T{ 0 2 edges{ edgemap{{ }}has-edge?' -> false }T
T{ 0 4 edges{ edgemap{{ }}has-edge?' -> true }T
T{ 1 2 edges{ edgemap{{ }}has-edge?' -> true }T
T{ 1 3 edges{ edgemap{{ }}has-edge?' -> true }T
T{ 1 4 edges{ edgemap{{ }}has-edge?' -> true }T
T{ 2 3 edges{ edgemap{{ }}has-edge?' -> true }T
T{ 2 1 edges{ edgemap{{ }}has-edge?' -> true }T
T{ 2 0 edges{ edgemap{{ }}has-edge?' -> false }T
T{ 2 4 edges{ edgemap{{ }}has-edge?' -> false }T

\ test in/out edge-maps
T{ #nodes #edges edges{ }edges>outedgemap  TO outedgemap{{ -> }T
T{ #nodes #edges edges{ }edges>inedgemap  TO inedgemap{{ -> }T
T{ outedgemap{{ 0 } 2@ NIP  ->  1 }T
T{ outedgemap{{ 1 } 2@ NIP  ->  3 }T
T{ outedgemap{{ 2 } 2@ NIP  ->  1 }T
T{ outedgemap{{ 3 } 2@ NIP  ->  1 }T
T{ outedgemap{{ 4 } 2@ NIP  ->  1 }T
T{ inedgemap{{ 0 } 2@ NIP  ->  1 }T
T{ inedgemap{{ 1 } 2@ NIP  ->  1 }T
T{ inedgemap{{ 2 } 2@ NIP  ->  1 }T
T{ inedgemap{{ 3 } 2@ NIP  ->  2 }T
T{ inedgemap{{ 4 } 2@ NIP  ->  2 }T

T{ outedgemap{{ 0 } 2@ DROP  @ ->  0  }T
T{ outedgemap{{ 1 } 2@ DROP  @+ @+ @+ DROP ->  1  3  6 }T
T{ outedgemap{{ 4 } 2@ DROP  @ ->  5 }T
T{ inedgemap{{ 0 } 2@ DROP  @ ->  5 }T
T{ inedgemap{{ 1 } 2@ DROP  @ -> 0 }T
T{ inedgemap{{ 4 } 2@ DROP  @+ @+ DROP ->  4 6 }T


\ sort edgemap (by angle)
VARIABLE mem0
VARIABLE mem1
T{ HERE mem0 !  #nodes xy{ edges{ edgemap{{ }}sort-edgemap  HERE mem1 ! -> }T
T{ mem1 @  mem0 @ -  -> 0 }T
T{ edgemap{{ 1 } 2@ DROP  @+ @+ @+ @+ DROP -> 
0  6 1negate  3 1negate  1 1negate }T

T{ 3 edgemap{{ 1 } 2@ }prev-edge -> 6 1negate }T
T{ 0 edgemap{{ 1 } 2@ }prev-edge -> 1 1negate }T

T{ 0 1 edges{ edgemap{{ }}has-edge?' -> true }T

\ edgemap to adjmap
T{ #nodes #edges edges{ }edges>edgemap  TO edgemap{{ -> }T
T{ #nodes edges{ edgemap{{ }}edgemap>adjmap -> }T
T{ edgemap{{ 0 } 2@ CELLS adjmap{{ 0 } 2@ CELLS COMPARE -> 0 }T
T{ edgemap{{ 1 } 2@ CELLS adjmap{{ 1 } 2@ CELLS COMPARE -> 0 }T
T{ edgemap{{ 2 } 2@ CELLS adjmap{{ 2 } 2@ CELLS COMPARE -> 0 }T
T{ edgemap{{ 3 } 2@ CELLS adjmap{{ 3 } 2@ CELLS COMPARE -> 0 }T
T{ edgemap{{ 4 } 2@ CELLS adjmap{{ 4 } 2@ CELLS COMPARE -> 0 }T

\ with orientation override
T{ #nodes #edges edges{ }edges>edgemap  TO edgemap{{ -> }T
1 CHARS , HERE  -1 c, 0 c, -1 c, 0 c, -1 c, 0 c, -1 c,  CONSTANT orient{
T{ #nodes orient{ edges{ edgemap{{ }}edgemap>adjmap' -> }T
T{ edgemap{{ 0 } 2@ DROP  @+ @+ DROP ->  1 1negate 4 1negate }T
T{ edgemap{{ 1 } 2@ DROP  @+ @+ @+ @+ DROP  ->  0  2  3  4 1negate  }T
T{ edgemap{{ 4 } 2@ DROP  @+ @+ @+ DROP  ->  3  0  1 }T

\ matrices
#nodes #nodes INTEGER MMATRIX seidel{{
T{ #nodes adjmap{{ seidel{{ }}adjmap>seidelmatrix -> }T
\ CR #nodes #nodes seidel{{ }}iprint

#nodes #nodes INTEGER MMATRIX +-0matrix{{
T{ #nodes adjmap{{ +-0matrix{{ }}adjmap>+-0matrix -> }T
\ CR #nodes #nodes +-0matrix{{ }}iprint


exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth graph-test.fs -e bye"
   End:
[THEN]
