\ -*-forth-*-
\ Misceallanous forth extensions
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

: 3dup  ( x1 x2 x3 -- x1 x2 x3 x1 x2 x3 )
   >R 2DUP R@ -ROT R> ;
: 6dup  ( x1 y1 x2 y2 x3 y3 -- x1 y1 x2 y2 x3 y3 x1 y1 x2 y2 x3 y3 )
   2>R 2OVER 2OVER 2R@ 2ROT 2ROT 2R> ;
: @+  ( a-addr -- x a-addr2 ) \ load from address with post-increment
   DUP @  SWAP CELL+ ;
: C@+  ( a-addr -- x a-addr2 ) \ load from address with post-increment
   COUNT SWAP ;
: 2@+  ( a-addr -- w a-addr2 ) \ load 2 cells from address with post-increment
   DUP 2@  ROT CELL+ CELL+ ;
: @!+  ( x a-addr -- )  \ store indirectly with post-increment
   DUP @ DUP CELL+ ROT !   ! ;
: D+!  ( d a-addr -- )  \ like +! for double-precision integers
   DUP >R 2@ D+ R> 2! ;
: OR!  ( x a-addr -- )  \ like +! but doing OR instead of +
   TUCK @ OR SWAP ! ;
: toggle  ( c-addr -- )  \ invert truth value of char variable
   DUP C@ 0= SWAP C! ;
: odd?  ( n -- flag )  1 AND 0<> ;
: even?  ( n -- flag )  1 AND 0= ;
: >sign  ( n -- -1|0|1 )  DUP 0< SWAP 0> NEGATE + ;
: d>sign  ( d -- -1|0|1 )  2DUP D0< -ROT D0> NEGATE + ;
: >sign+-  ( n -- -1|1 )  0< 2* 1+ ;
: 0allot  ( n -- )  HERE OVER ALLOT  SWAP ERASE ;

: ud/mod  ( ud1 u1 -- u2 ud2 )
   \ mixed precision unsigned division with double precision quotient, single
   \ precision remainder.  (gforth defines this, too, having it here for
   \ compatability/tuning)
   >R 0 R@ UM/MOD		( s: ud1-low rem1 ud2-high  r: u1 )
   -ROT R> UM/MOD ROT ;

8 CELLS CONSTANT #bits/cell

: isqrt  ( u1² -- u2 u3 )  \ u3 is square root of u1², u2 is remainder
   \ this works by adding a bit B to u3 if the binomial inequality u1² >=
   \ (u3+B)² = u3² + 2Bu3 + B² is satisfied.  Since we're always checking
   \ against u3² + x, we keep the difference u1²-u3² in u2 during formation of
   \ u3.  This is accomplished by subtracting the part of the binomial 2Bu3+B²
   \ whenever we change u3 (u3 starts out 0).  No multiply needed (B is one
   \ bit).
   \
   \ To keep us from shifting around u3 to get 2Bu3, we instead keep u3
   \ multiplied by (shifted) 2B throughout, i.e u3 is 2Bu3.  This is simple,
   \ as it allows us to use B² to add to 2Bu3.  One divide by 2 per iteration
   \ is needed to keep that equality.
   \
   \ Has to scan through all u3² bits from bits/cell-2 through 0.
   0						( s: u2=u1² 2Bu3=0 )
   0 #bits/cell 2 - ?DO
      2DUP 1 I LSHIFT OR  TUCK U>= IF		\ u1²-u3² >= 2Bu3+B² ?
	 					( s: u2 2Bu3 2Bu3+B²  )
	 ROT SWAP -				\ -> adjust u2:=u2-2Bu3-B²
	 SWAP 2/ 1 I LSHIFT OR			\ -> set bit B in u3,
      ELSE DROP 2/ THEN				\ else, just 2Bu3 shifted
   -2 +LOOP ;

: disqrt  ( ud1 -- u2 u3 )  \ double precision square root 
   \ See comments above.  Same implementation, just don't have shifts or OR
   \ for double-precision type.
   0.						( s: u2=ud1²  2Bu3=0 )
   [ 0 1 #bits/cell 2 - LSHIFT ] 2LITERAL	( s: u2=ud1²  2Bu3=0  B )
   BEGIN
      2>R					( s: u2=ud1² 2Bu3=0   r: B )
      2OVER 2OVER 2R@ D+ 2TUCK DU>= IF		\ ud1²-u3² >= 2Bu3+B² ?
	 2ROT 2SWAP D-				\ -> adjust u2:=u2-2Bu3-B²
	 2SWAP D2/ 2R@ D+			\ -> set bit B in u3,
      ELSE 2DROP D2/ THEN			\ else, just 2Bu3 shifted
      2R> D2/ D2/
   2DUP D0= UNTIL
   2DROP
   DROP NIP ;					\ convert to single precision

: count1bits  ( x -- n )   \ n is number of 1-bits in x
   0 BEGIN
      >R DUP 1 RSHIFT  SWAP 1 AND  R> +
   OVER 0= UNTIL NIP ;

: err-type  ( c-addr u -- )  \ print to stderr
   ['] type stderr outfile-execute ;

: -cbounds  ( c-addr u --  c-addr-1 c-addr+u-1 )
   \ like BOUNDS but for reverse loop over char-array using -DO..-LOOP
   SWAP 1-  TUCK + ;
: -bounds  ( a-addr u --  a-addr-cell a-addr+u-cell )
   \ like BOUNDS but for reverse loop over cell-array using -DO..-LOOP
   SWAP CELL -  TUCK + ;
: #msb  ( x -- u )  \ give index of highest non-zero bit
   \ For x=$80 return 7.  For x=1 return 0.  For x=0 return 0 as well.
   0 ( s: x u )
   [ #bits/cell 64 >= ] [IF]
      OVER $FFFFFFFF00000000 AND 0<> 32 AND ( s: x u 32|0)
      TUCK + -ROT RSHIFT SWAP ( s: x u )
   [THEN]
   [ #bits/cell 32 >= ] [IF]
      OVER $FFFF0000 AND 0<> 16 AND ( s: x u 16|0)
      TUCK + -ROT RSHIFT SWAP ( s: x u )
   [THEN]
   OVER $FF00 AND 0<> 8 AND ( s: x u 8|0)
   TUCK + -ROT RSHIFT SWAP ( s: x u )
   OVER $F0 AND 0<> 4 AND ( s: x u 4|0)
   TUCK + -ROT RSHIFT SWAP ( s: x u )
   OVER $C AND 0<> 2 AND ( s: x u 2|0)
   TUCK + -ROT RSHIFT SWAP ( s: x u )
   SWAP $2 AND 0<> 1 AND ( s: u 1|0)
   + ;
: parity  ( u -- 1|0 )  \ result is the XOR of all bits in u
   [ #bits/cell 64 >= ] [IF]
      DUP 32 RSHIFT XOR
   [THEN]
   [ #bits/cell 32 >= ] [IF]
      DUP 16 RSHIFT XOR
   [THEN]
   DUP 8 RSHIFT XOR   DUP 4 RSHIFT XOR  DUP 2 RSHIFT XOR  DUP 2/ XOR
   1 AND ;
\ inline the above function for performance
' parity
:noname
   [ #bits/cell 64 >= ] [IF]
      ]] DUP 32 RSHIFT XOR [[
   [THEN]
   [ #bits/cell 32 >= ] [IF]
      ]] DUP 16 RSHIFT XOR [[
   [THEN]
   ]] DUP 8 RSHIFT XOR   DUP 4 RSHIFT XOR  DUP 2 RSHIFT XOR  DUP 2/ XOR
   1 AND [[ ;
interpret/compile: parity


\ this nowadays exists in Gforth!
\ : dlshift  ( ud1 u -- ud2 )  \ double precision lshift by u>0
\    TUCK LSHIFT -ROT             \ shift high
\    2DUP LSHIFT -ROT             \ shift low
\    #bits/cell SWAP - RSHIFT     \ compute carry shifted from low to high
\    ROT OR ;                     \ add carry to high
\ this nowadays exists in Gforth!
\ : drshift  ( ud1 u -- ud2 )  \ double precision rshift by U>0
\    2DUP RSHIFT >R               \ shift high
\    TUCK #bits/cell SWAP - LSHIFT -ROT    \ carry from high to low
\    RSHIFT OR  R> ;         \  add carry to low
: mlshift ( u1 u2 -- du )   \ mixed-precision lshift by U2>0
   0 SWAP DLSHIFT ;   \ if DLSHIFT natively exists, it is more efficient to use
   \ 2DUP LSHIFT -ROT \ that one instead!
   \ #bits/cell SWAP - RSHIFT ;
: mrshift  ( du u1 -- u2 )  \ mixed-precision rshift by u1>0
   TUCK #bits/cell SWAP - LSHIFT -ROT
   RSHIFT OR ;
\ todo: we actually need a different mixed-precision shift that does:
\ 0 X drshift  ( i.e. a drshift with lower half zero )

: n**  ( n1 +n2 -- n3 )  \ signed integer exponentiation
   >r 1 swap
   BEGIN  r@ 0> WHILE
	 r@ 1 and if
	    tuck * swap
	 then
	 dup *
	 r> 2/ >r
   REPEAT
   r> 2drop ;
: 2**  ( +n -- )   \ return 2^n
   1 SWAP LSHIFT ;
' 2**  :noname  ]] 1 SWAP LSHIFT [[ ;  interpret/compile: 2**

-1 CELLS CONSTANT -CELL

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth misc-test.fs -e bye"
   End:
[THEN]
