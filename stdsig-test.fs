\ -*-forth-*-
\ Holographic standard signatures -- unit test
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA posteo TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: July 2021
3 assert-level !

require ./tester2.fs
T{ require  ./stdsig.fs -> }T

CR .( std1n:)
T{ std1n{ 1 }.sig  mvp-depth -> 0 }T

CR .( std2n:)
0 INTEGER MARRAY std2n{
mvp1 mvp,  mvp0 mvp,
T{
std2n{ 1 std-recursion  mvp-depth -> 0 }T
std2n{ 2 }.sig

CR .( std3n:)
\ T{
std2n{ 2 std-recursion mvp-depth -> 0 }T
std2n{ 3 }.sig

CR .( 0-std2e:) 
T{  0 2 stdsig-e 2 }.sig mvp-depth -> 0 }T

CR .( 1-std2e:) 
T{  1 2 stdsig-e 2 }.sig mvp-depth -> 0 }T

CR .( 0-std2o:) 
T{  0 2 stdsig-o 2 }.sig mvp-depth -> 0 }T

CR .( 1-std2o:) 
T{  1 2 stdsig-o 2 }.sig mvp-depth -> 0 }T

CR .( 0-std3e:) 
T{  0 3 stdsig-e 3 }.sig mvp-depth -> 0 }T

CR .( 1-std3e:) 
T{  1 3 stdsig-e 3 }.sig mvp-depth -> 0 }T

CR .( 2-std3e:) 
T{  2 3 stdsig-e 3 }.sig mvp-depth -> 0 }T

CR .( 3-std3e:) 
T{  3 3 stdsig-e 3 }.sig mvp-depth -> 0 }T

CR .( 0-std3o:) 
T{  0 3 stdsig-o 3 }.sig mvp-depth -> 0 }T

CR .( 1-std3o:) 
T{  1 3 stdsig-o 3 }.sig mvp-depth -> 0 }T

CR .( 2-std3o:) 
T{  2 3 stdsig-o 3 }.sig mvp-depth -> 0 }T

CR .( 3-std3o:) 
T{  3 3 stdsig-o 3 }.sig mvp-depth -> 0 }T


\ CR .( std4n:)
\ T{ std3n{ 3 std4n{ std-recursion mvp-depth -> 0 }T
\ std4n{ 4 }.sig

CR .( std5n:)
T{ 5 stdsig-n CONSTANT std5n{ mvp-depth -> 0 }T
std5n{ 5 }.sig

CR .( std6n:)
T{ 6 stdsig-n CONSTANT std6n{ mvp-depth -> 0 }T
std6n{ 6 }.sig

CR .( 0-std6o:)
T{ 0 6 stdsig-o 6 }.sig mvp-depth -> 0 }T

CR .( 1-std6e:)
T{ 1 6 stdsig-e 6 }.sig mvp-depth -> 0 }T

\ compare against Bradley paper 

\ Define lambda_x,y coefficients as used by Bradley
: λ_2,1 x0 ;
: λ_3,1 x1 ;  : λ_3,2 x2 ;
: λ_4,1 x3 ;  : λ_4,2 x4 ;  : λ_4,3 x5 ;
: λ_5,1 x6 ;  : λ_5,2 x7 ;  : λ_5,3 x8 ;  : λ_5,4 x9 ;
: λ_6,1 x10 ; : λ_6,2 x11 ; : λ_6,3 x12 ; : λ_6,4 x13 ;  : λ_6,5 x14 ;

\ Compare all entries of the normalized standard arity-6 signature with
\ values transcribed from the Bradley paper.

T{  std6n{ %000000 } mvp@  mvp1                                 mvp= -> true }T
T{  std6n{ %000011 } mvp@  λ_2,1                                mvp= -> true }T
T{  std6n{ %000101 } mvp@  λ_3,1                                mvp= -> true }T
T{  std6n{ %001001 } mvp@  λ_4,1                                mvp= -> true }T
T{  std6n{ %001010 } mvp@  λ_4,2                                mvp= -> true }T
T{  std6n{ %001100 } mvp@  λ_4,3                                mvp= -> true }T
T{  std6n{ %001111 } mvp@  λ_4,1 λ_3,2 mvp*
                        -1 λ_4,2 λ_3,1 mvp* mvp*n mvp+
                           λ_4,3 λ_2,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %010001 } mvp@  λ_5,1                                mvp= -> true }T
T{  std6n{ %010010 } mvp@  λ_5,2                                mvp= -> true }T
T{  std6n{ %010100 } mvp@  λ_5,3                                mvp= -> true }T
T{  std6n{ %010111 } mvp@  λ_5,1 λ_3,2 mvp*
                        -1 λ_5,2 λ_3,1 mvp* mvp*n mvp+
                           λ_5,3 λ_2,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %011000 } mvp@  λ_5,4                                mvp= -> true }T
T{  std6n{ %011011 } mvp@  λ_5,1 λ_4,2 mvp*
                        -1 λ_5,2 λ_4,1 mvp* mvp*n mvp+
                           λ_5,4 λ_2,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %011101 } mvp@  λ_5,1 λ_4,3 mvp*
                        -1 λ_5,3 λ_4,1 mvp* mvp*n mvp+
                           λ_5,4 λ_3,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %011110 } mvp@  λ_5,2 λ_4,3 mvp*
                        -1 λ_5,3 λ_4,2 mvp* mvp*n mvp+
                           λ_5,4 λ_3,2 mvp* mvp+                mvp= -> true }T
T{  std6n{ %100001 } mvp@  λ_6,1                                mvp= -> true }T
T{  std6n{ %100010 } mvp@  λ_6,2                                mvp= -> true }T
T{  std6n{ %100100 } mvp@  λ_6,3                                mvp= -> true }T
T{  std6n{ %100111 } mvp@  λ_6,1 λ_3,2 mvp*
                        -1 λ_6,2 λ_3,1 mvp* mvp*n mvp+
                           λ_6,3 λ_2,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %101000 } mvp@  λ_6,4                                mvp= -> true }T
T{  std6n{ %101011 } mvp@  λ_6,1 λ_4,2 mvp*
                        -1 λ_6,2 λ_4,1 mvp* mvp*n mvp+
                           λ_6,4 λ_2,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %101101 } mvp@  λ_6,1 λ_4,3 mvp*
                        -1 λ_6,3 λ_4,1 mvp* mvp*n mvp+
                           λ_6,4 λ_3,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %101110 } mvp@  λ_6,2 λ_4,3 mvp*
                        -1 λ_6,3 λ_4,2 mvp* mvp*n mvp+
                           λ_6,4 λ_3,2 mvp* mvp+                mvp= -> true }T
T{  std6n{ %110000 } mvp@  λ_6,5                                mvp= -> true }T

T{  std6n{ %110011 } mvp@  λ_6,1 λ_5,2 mvp*
                        -1 λ_6,2 λ_5,1 mvp* mvp*n mvp+
                           λ_6,5 λ_2,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %110101 } mvp@  λ_6,1 λ_5,3 mvp*
                        -1 λ_6,3 λ_5,1 mvp* mvp*n mvp+
                           λ_6,5 λ_3,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %110110 } mvp@  λ_6,2 λ_5,3 mvp*
                        -1 λ_6,3 λ_5,2 mvp* mvp*n mvp+
                           λ_6,5 λ_3,2 mvp* mvp+                mvp= -> true }T
T{  std6n{ %111001 } mvp@  λ_6,1 λ_5,4 mvp*
                        -1 λ_6,4 λ_5,1 mvp* mvp*n mvp+
                           λ_6,5 λ_4,1 mvp* mvp+                mvp= -> true }T
T{  std6n{ %111010 } mvp@  λ_6,2 λ_5,4 mvp*
                        -1 λ_6,4 λ_5,2 mvp* mvp*n mvp+
                           λ_6,5 λ_4,2 mvp* mvp+                mvp= -> true }T
T{  std6n{ %111100 } mvp@  λ_6,3 λ_5,4 mvp*
                        -1 λ_6,4 λ_5,3 mvp* mvp*n mvp+
                           λ_6,5 λ_4,3 mvp* mvp+                mvp= -> true }T
T{  std6n{ %111111 } mvp@

                           λ_6,1 λ_5,2 λ_4,3 mvp* mvp*
                           -1 λ_6,1 λ_5,3 λ_4,2 mvp* mvp* mvp*n mvp+
                           λ_6,1 λ_5,4 λ_3,2 mvp* mvp* mvp+
                           -1 λ_6,2 λ_5,1 λ_4,3 mvp* mvp* mvp*n mvp+
                           λ_6,2 λ_5,3 λ_4,1 mvp* mvp* mvp+
                           -1 λ_6,2 λ_5,4 λ_3,1 mvp* mvp* mvp*n mvp+
                           λ_6,3 λ_5,1 λ_4,2 mvp* mvp* mvp+
                           -1 λ_6,3 λ_5,2 λ_4,1 mvp* mvp* mvp*n mvp+
                           λ_6,3 λ_5,4 λ_2,1 mvp* mvp* mvp+
                           -1 λ_6,4 λ_5,1 λ_3,2 mvp* mvp* mvp*n mvp+
                           λ_6,4 λ_5,2 λ_3,1 mvp* mvp* mvp+
                           -1 λ_6,4 λ_5,3 λ_2,1 mvp* mvp* mvp*n mvp+
                           λ_6,5 λ_4,1 λ_3,2 mvp* mvp* mvp+
                           -1 λ_6,5 λ_4,2 λ_3,1 mvp* mvp* mvp*n mvp+
                           λ_6,5 λ_4,3 λ_2,1 mvp* mvp* mvp+

                                                                mvp= -> true }T


\ CR .( std7n:)
\ T{ std6n{ 6 }std-recursion{ CONSTANT std7n{   mvp-depth -> 0 }T
\ std7n{ 7 }.sig

CR .( std8n:) 
T{  8 stdsig-n DROP mvp-depth -> 0 }T
CR .( std9n:) 
T{  9 stdsig-n DROP mvp-depth -> 0 }T
CR .( std10e:) 
T{  10 stdsig-n DROP mvp-depth -> 0 }T
CR .( std11e:) 
T{  11 stdsig-n DROP mvp-depth -> 0 }T
\ CR .( std12e:) 
\ T{  12 stdsig-n 4095 } mvp@ mvp-maxidx mvp-depth -> 65 0 }T
\ T{  16 stdsig 65535 } mvp@ mvp-maxidx mvp-depth -> 119 0 }T

CR .( stdsig-eval:)
64 INTEGER MARRAY sig6r{
15 INTEGER MARRAY xval{   15 0 [DO]  [I] xval{ [I] } ! [LOOP]

T{ 6 xval{ sig6r{ stdsig-n-eval -> }T
64 0 [DO]
   T{ sig6r{ [I] } @  ->  xval{ std6n{ [I] } mvp@  mvp-eval }T
[LOOP]

T{ 6 xval{ sig6r{ (stdsig-n-eval) -> }T
64 0 [DO]
   T{ sig6r{ [I] } @  ->  xval{ std6n{ [I] } mvp@  mvp-eval }T
[LOOP]

CR .( stdsig-eval {bv}:)
15 0 [DO]  2 [I] n**  4 [I] n** + xval{ [I] } ! [LOOP]
T{ 6 xval{ sig6r{ stdsig-n-eval -> }T
CR 64 sig6r{ HEX }iprint DECIMAL
64 0 [DO]
   T{ sig6r{ [I] } @  ->  xval{ std6n{ [I] } mvp@  mvp-eval }T
[LOOP]

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth-fast -m 1G stdsig-test.fs -e bye"
   End:
[THEN]

