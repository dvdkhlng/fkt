\ -*-forth-*-
\ Benchmark our FKT Algorithm implementation
\
\ Copyright (C) 2012 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Nov 2012

require ./fkt.fs

50 50 make-square-lattice }#matchings 

BYE


\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "time gforth-fast -r 1M -d 1M -l 1M -m 128M -- benchmark.fs  -e bye"
   End:
[THEN]

