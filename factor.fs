\ -*-forth-*-
\ Factoring of small integers
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./matrix.fs
require ./misc.fs

: (prime?)  ( n -- flag )  \ primitive check for whether prime number
   DUP 2 ?DO
      DUP I MOD 0= IF  DROP FALSE UNLOOP EXIT THEN
   LOOP
   DROP TRUE ;

6 CONSTANT #primes    \ number of small primes to check against
#primes INTEGER MARRAY primes{

: fill-primes{}  ( -- )  \ fill array of small primes
   0    ( s: count )
   999999 2 DO
      I (prime?) IF
	 primes{ OVER }  I SWAP !
	 1+  DUP #primes = IF DROP UNLOOP EXIT THEN
      THEN
   LOOP
   DROP ;

fill-primes{}
CR .( Small primes used for small integer factorization: #=) #primes .
CR #primes primes{ }iprint
   

0 VALUE #divisors		\ number of divisors not divided by primes{}
0 VALUE divisors-product	\ product of those divisors

: make-divisors  ( -- a-addr )
   \ make array of divisors that are not multiples of primes{}, with maximum
   \ divisor equaling product of primes{} minus one.  Set #divisors and
   \ divisor-product
   1  primes{ #primes }bounds ?DO  I @ * CELL +LOOP  TO divisors-product
   ALIGN INTEGER , HERE
   divisors-product 0 DO
      TRUE
      #primes 0 DO  J primes{ I } @ MOD 0<> AND  LOOP   ( s: not-multiple? )
      IF
	 I ,
	 #divisors 1+ TO #divisors
      THEN
   LOOP ;

make-divisors CONSTANT divisors{
CR .( Divisor array #=) #divisors .  .( product=) divisors-product .
\ CR #divisors divisors{ }iprint

: sort-factors  ( ud1 u1 -- ud2 u2 )
   \ correct for division by number with itself, happening to small numbers to
   \ optimized division loops that blindly test against array of divisor, not
   \ aborting if square root of number reached: if ud1 is 1., set u1=1 and
   \ ud1=u1
   >R 2DUP 1. D= IF 2DROP R> 0 1 EXIT THEN
   R> ;

: (small-factor)  ( ud1 -- ud2 u)
   \ Attempt to factor ud by dividing it against the primes in primes{},
   \ returing smallest dividing prime in u and ud2=quotient.  If ud is not
   \ divided by a prime, return ud1 and u=1.
   primes{ #primes }bounds DO 
      2DUP I @ UD/MOD  ROT 0=  IF
	 2NIP I @   sort-factors UNLOOP EXIT
      ELSE 2DROP THEN
   CELL +LOOP
   1 ;
   
: (divisors-factor)  ( ud1 u1 -- ud2 u2)
   \ check for whether ud is divided by the divisors in divisors{} plus the
   \ value u.  U will usually be chosen to be a multiple of divisors-product
   \ Return divisor, if any is found, else 0.  Return smallest found divisor
   \ in u2, quotient in ud2.  If no divisor found, ud2=ud1 and u=1
   DUP >R
   divisors{ #divisors }bounds	( s: ud1 u1 a-addr1 a-addr2  r:  u1 )
   R> 0= IF  CELL+ THEN		\ skip first divisor '1' if u1==0
   DO 
      DUP I @ +		( s: ud u divisor )
      2OVER ROT UD/MOD ROT 0= IF 
	 ROT I @ +  sort-factors  >R 2NIP R>
	 UNLOOP EXIT
      ELSE 2DROP THEN
   CELL +LOOP
   DROP 1 ;

: (factor)  ( ud1 -- ud2 u )
   \ attempt to factor ud, returning the smallest factor found, in u2, and the
   \ quotient ud/u2 in u1.  By definition u2 is prime.
   \ If prime, return ud in u1 (might overflow!) and u2=1 .
   \ todo: change semantics: return u1=prime number and u2=quotient.
   \ quotient may be 1, if equal to prime.
   2DUP 4. DU< IF  1 EXIT THEN
   (small-factor) DUP 1 <> IF  EXIT ELSE DROP THEN
   2DUP disqrt NIP 1+ 0 ?DO 
      I (divisors-factor) DUP 1 <> IF  UNLOOP EXIT ELSE DROP THEN
   divisors-product +LOOP
   1 ;
   
: factor  ( ud1 -- n v{ )
   \ attempt to factor ud1, returning its factors in ascending order in vector
   \ v{ of length 'n'.  For primes, 'n' is 1.  Use }unallot to free v{}.
   ALIGN DOUBLE , HERE   { v{ }
   BEGIN (factor) DUP 1 <> WHILE   0  2,  REPEAT
   DROP 2,
   HERE v{ - DOUBLE / v{ ;
   


\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth-fast factor-test.fs -e bye"
   End:
[THEN]
