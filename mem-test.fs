\ -*-forth-*-
\ Tests for mem.fs
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./mem.fs -> }T


VARIABLE mem1  0 mem1 !
VARIABLE mem2  1234567 mem2 !
T{ mem1 mem2 CELL exchange-mem -> }T
T{ mem1 @  mem2 @ -> 1234567 0 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth mem-test.fs -e bye"
   End:
[THEN]
