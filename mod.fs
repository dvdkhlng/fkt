\ -*-forth-*-
\ Modular arithmetic routines
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./gcd.fs

variable modulus

: drn  ( d -- n )  modulus @ fm/mod drop ;
: rn  ( n1 -- n2 )  modulus @ modf ;
: rok?  ( n -- flag )  0 modulus @ WITHIN ;
: r+  ( n1 n2 -- n3 )
   assert3( 2DUP rok? SWAP rok? AND   )
   +  DUP modulus @ - UMIN
   assert3( DUP rok? ) ;   
assert-level @ 3 < [IF]
   ' r+
   :noname  ]]    +  DUP modulus @ - UMIN [[ ;
   interpret/compile: r+
[THEN]

: r-  ( n1 n2 -- n3 )
   assert3( 2DUP rok? SWAP rok? AND   )
   -  DUP modulus @ + UMIN
   assert3( DUP rok? ) ;
\ : r+  ( n1 n2 -- n3 )  s>d rot m+  drn ;
\ : r-  ( n1 n2 -- n3 )  s>d rot s>d 2SWAP D-  drn ;
: r+!  ( n1 a-addr -- )  dup @ rot r+ swap ! ;
: r-!  ( n1 a-addr -- )  dup @ rot r- swap ! ;
: rnegate  ( n1 -- n2 )  negate DUP modulus @ + UMIN  assert3( DUP rok? ) ;
: r*  ( n1 n2 -- n3 )
   assert3( 2DUP rok? SWAP rok? AND   )
   modulus @ */modf DROP ;
assert-level @ 3 < [IF]
   ' r*
   :noname  ]] modulus @ */modf DROP [[ ;
   interpret/compile: r* 
[THEN]   
: rsq  ( n1 -- n2 )  dup r* ;
: r**  ( n1 n2 -- n3 )
   >r 1 swap
   BEGIN  r@ 0> WHILE
	 r@ 1 and if
	    tuck r* swap
	 then
	 rsq
	 r> 2/ >r
   REPEAT
   r> 2drop ;
: rinv  ( n1 -- n3 )  \ this only works for prime modulus
   DUP 0= ABORT" rinv by zero"
   modulus @ 2 - r** ;

\ using extended euclidean algorithm is faster (in theory), and works for
\ non-prime modulus:
: rinv  ( n1 -- n3 )
   DUP 0= ABORT" rinv by zero"   
   modulus @ bezout  DROP
   DUP modulus @ + UMIN   \ make sure result is positive
   assert3( DUP rok? ) ;

: rinv-nonrec ( n1 -- n2 )
   \ Non-recursive version.  In gForth, with dynamic superinstructions
   \ disabled, this is actually slower than the recursive bezout-based rinv
   \ above.  Recursion in Gforth *is* fast.
   \ The algorithm corresponds to pseudo-code from
   \ https://en.wikipedia.org/w/index.php?title=Extended_Euclidean_algorithm&oldid=600472905#Modular_integers
   0 1  ROT modulus @ SWAP        ( s: t newt r newr )
   DUP IF	\ initial comparison pulled out (is faster than BEGIN..WHILE)
      BEGIN
        TUCK /modf	( s: t newt newr rem quot )
        >R 2SWAP TUCK R> * -
        2SWAP
      DUP 0= UNTIL
   THEN
   DROP  1 >   ABORT" not invertible "
   DROP
   DUP modulus @ + UMIN ;   \ make sure result is positive 

\ think twice before using this!
: r0<  ( n1 -- flag )  modulus @ 2/ U> ;
: r>signed  ( n1 -- n2 )  DUP r0< IF modulus @ - THEN ;
: r.s  ( n1 -- ) r>signed . ;

\ define maximum prime-number modulus supported by forth system
cell 4 = [IF]
   1 31 LSHIFT 1- CONSTANT max-modulus
[THEN]
cell 8 = [IF]
   ( 2^63-25) 9223372036854775783 CONSTANT max-modulus
[THEN]

max-modulus modulus !

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth mod-test.fs -e bye"
   End:
[THEN]
