\ -*-forth-*-
\ Elementary matrix transforms for matrices with elments from finite field
\
\ Copyright (C) 2014 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Mar 2014

require ./matrix.fs
require ./mod.fs

: (}}rrowadd) { alpha n m{{ r1 c r2 -- }
   \ In matrix M add ALPHA times the values in columns c..c+n-1 of row r1 to
   \ row r2
   m{{ r1 c }}   
   m{{ r2 c }}
   n 0 ?DO			( s: addr1 addr2 )
      2DUP @ SWAP @  alpha r* r+  OVER !
      CELL+ SWAP CELL+ SWAP
   LOOP
   2DROP ;
: }}rrowadd  ( alpha n m{{ r1 r2 -- )
   0 SWAP (}}rrowadd) ;

: (}}rcoladd) { alpha n m{{ r c1 c2 -- }
   \ In matrix M add ALPHA times the values in rows r..r+n-1 of column c1 to
   \ column c2
   m{{ r c1 }}   
   m{{ r c2 }}
   m{{ }}rowsize		{ stride }
   n 0 ?DO			( s: addr1 addr2 )
      2DUP @ SWAP @  alpha r* r+  OVER !
      stride + SWAP stride + SWAP
   LOOP
   2DROP ;
: }}rcoladd  ( alpha n m{{ c1 c2 -- )
   0 -ROT (}}rcoladd) ;

: (}}rrowmul) ( alpha n m{{ r c -- )
   \ In matrix M multiply elements in row R from column C..C+N-1 by ALPHA
   }} SWAP CELLS bounds ?DO  ( s: alpha )
      DUP I @ r* I !
   CELL +LOOP
   DROP ;
: }}rrowmul ( alpha n m{{ r -- )
   0 (}}rrowmul) ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth remt-test.fs -e bye"
   End:
[THEN]
