\ -*-forth-*-
\ Holographic standard signatures
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA posteo TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: July 2021
\
\ See also William F. Bradley, "A Recursive Definition of the Holographic
\ Standard Signature"

require ./matrix.fs 
require ./mvpoly.fs

\ arity-one normalized standard even signature
ALIGN INTEGER , HERE 
mvp1 mvp, 
mvp0 mvp,
CONSTANT std1n{

\ arity-one standard even signature
ALIGN INTEGER , HERE 
mvp1 mvp,  \ ? 
mvp0 mvp,
CONSTANT std1e{

\ arity-one standard odd signature
ALIGN INTEGER , HERE 
mvp0 mvp, 
mvp1 mvp,  \ ?
CONSTANT std1o{

: }.sig  { sig{ n -- }  \ print signature
   1 n LSHIFT 0 ?DO
      sig{ I } mvp@  mvp-size 0> IF \ only show non-zero entries
         CR base @  2 base !  I 16 .R SPACE   base !
         .mvp
      ELSE  mvp-drop THEN
   LOOP ;

: signadjust  { sigidx ifaceidx -- -1|1 }
   \ return sign change due to cross-over gadgets when routing a new input
   \ (n+1) to input ifaceidx, when assuming the inputs 1..n have value
   \ according to sigidx.
   \
   \ Explanation: the ifaceidx-1 lowest bits of sigidx correspond to the
   \ values at the 1x1 crossings!  Note that we need to shift twice to limited
   \ LSHIFT range!
   \
   \ alternative interpretation this corresponds to the sign adjustment in
   \ section 2.5 "Standard Signatures via Recursion".
   -1 1
   sigidx   [ #bits/cell 1- ]L ifaceidx - LSHIFT 2*
   parity select ;
   
: std-recursion  { sig{ n --  }
   \ n : arity of standard signature sig{
   \
   \ Append the shift-set corresponding to sig{} with base point xhat at the
   \ current position in data space (HERE), thereby forming a arity n+1
   \ signature (assuming that sig{ is currently being constructed in data
   \ space)
   \
   \ See bradley paper section 2.5 "standard Signatures via Recursion"
   
   -1   \ find xhat: i.e. the smallest x so that f(x) <> 0
   1 n LSHIFT 0 ?DO
      sig{ I } mvp@  mvp0<> IF  DROP I LEAVE THEN
   LOOP  DUP 0< ABORT" no shift-set for all-zero signature!"
   { xhat }
   
   n dup 1- * 2/   { voffs } \ start offset for variables x_n added here
   
   1 n LSHIFT 0 ?DO
      mvp0    ( o: accumulator )
      n 0 ?DO  \ iterate over all possible connections of new input "n+1"
         J xhat XOR  1 I LSHIFT AND IF  \ x_i != xhat_io
            sig{ J  1 I LSHIFT XOR } mvp@
            \ todo: the ideom "mvp-x_n mvp*" can be factored out into something much faster than the currently O(n²) mvp* 
            voffs I + mvp-x_n
            J xhat XOR I signadjust mvp*n
            mvp*  \ add new coefficient (in paper: lambda)
            mvp+
         THEN
      LOOP      
      ( accumulator) mvp,
   LOOP ;

: stdsig-n  { n -- sig{ ) }
   \ return arity-n normalized standard signature.
   \ You may free the returned memory using }unallot
   \ Returned polynomials use coefficients x_0 ... x_{n*(n-1)/2-1}.
   \ Ownership of polynomials referenced in sig{} goes to the caller.
   ALIGN INTEGER ,  HERE   { sig{ }
   \ seed the arity-one standard signature
   mvp1 mvp,  mvp0 mvp,
   n 1 ?DO
      \ extend signature by one input per iteration, using the matchgate
      \ recursion from the bradley paper
      sig{ I std-recursion
   LOOP
   sig{ ;

DEFER stdsig-e 

: stdsig-o  { u n -- sig{ ) }
   \ Return sig{: arity-n standard odd signature
   \
   \ You may free the returned memory using }unallot Returned polynomials use
   \ coefficients x_0 ... x_{n*(n-1)/2-1}.  Depending on the value of u, not
   \ all coefficients are used.  Ownership of polynomials referenced in sig{}
   \ goes to the caller.
   \
   \ Bit-mask u defines which inputs in the recursion are fixed to one.  It
   \ has (n-1) bits, as lowest input is never fixed.
   
   n 1 = IF
      ALIGN INTEGER ,  HERE
      mvp0 mvp,   mvp1 mvp,
      EXIT
   THEN
   1 n 2 - LSHIFT u AND IF
      u n 1- stdsig-e  { sige{ }
      1 n LSHIFT 2/ 0 ?DO         \ move sige{ to f_1, set f_0 zero
         sige{ I } mvp@  mvp,
         mvp0 sige{ I } mvp!
      LOOP
      sige{
   ELSE
      u n 1- RECURSE  { sigo{ }
      sigo{ n 1-  std-recursion   \ append the shift-set of sigo{}
      sigo{
   THEN ;
      

: (stdsig-e)  { u n -- sig{ ) }
   \ Return sig{: arity-n standard even signature
   \
   \ You may free the returned memory using }unallot Returned polynomials use
   \ coefficients x_0 ... x_{n*(n-1)/2-1}.  Depending on the value of u, not
   \ all coefficients are used.  Ownership of polynomials referenced in sig{}
   \ goes to the caller.
   \
   \ Bit-mask u defines which inputs in the recursion are fixed to one.  It
   \ has (n-1) bits, as lowest input is never fixed.
   \
   \ If u=0, returned signature corresponds to result of stdsig-n .
   n 1 = IF
      ALIGN INTEGER ,  HERE
      mvp1 mvp,   mvp0 mvp,
      EXIT
   THEN
   1 n 2 - LSHIFT u AND IF
      u n 1- stdsig-o  { sigo{ }
      1 n LSHIFT 2/ 0 ?DO         \ move sigo{ to f_1, set f_0 zero
         sigo{ I } mvp@  mvp,
         mvp0 sigo{ I } mvp!
      LOOP
      sigo{
   ELSE
      u n 1- RECURSE  { sige{ }
      sige{ n 1-  std-recursion   \ append the shift-set of sige
      sige{
   THEN ;
' (stdsig-e) IS stdsig-e

: stdsig-eval-lift  { n values{ sig{ -- }
   \ given the standard signature sig{} of arity N for weights values{},
   \ extend sig{} to the corresponding N+1 standard signature.
   \ Make sure that values{} and sig{} are of sufficient size.
   n DUP 1- * 2/   { voffs } \ start offset for variables x_n added here
   1 n LSHIFT 0 ?DO
      0   ( S: accumulator )
      n 0 ?DO  \ iterate over all possible connections of new input "n+1"
         J  1 I LSHIFT AND IF
            sig{ J  1 I LSHIFT XOR } @
            values{ voffs I + } @ *
            J I signadjust *
            +
         THEN
      LOOP      
      ( accumulator) sig{ I  1 n LSHIFT +  } !
   LOOP ;

: (stdsig-eval-lift)  { n values{ sig{ -- }
   \ same as above, but different loop structure and using +! operator
   \ Due to +! use, make sure part of sig{} to be populated is zero on entry.
   n DUP 1- * 2/   { voffs } \ start offset for variables x_n added here
   n 0 ?DO  \ iterate over all possible connections of new input "n+1"
      values{ voffs I + } @   ( s: coeff )
      n 2** 0 ?DO
         I J 2** AND IF
            DUP
            sig{ I  J 2** INVERT AND } @ *
            I J signadjust *
            sig{ I  n 2** + } +!
         THEN
      LOOP
      DROP
   LOOP ;
   
: stdsig-n-eval { n values{ sig{ -- }
   \ Compute arity-n standard-signature's values given defined values for the
   \ polynomial coefficients x_0..x_{n*(n-1)/2-1}.   Supply a sufficiently
   \ allocated array in sig{ and the coeffients in values{ .
   \ Note how stdsig-n-eval may be faster than using stdsig-n together
   \ with mvpoly-eval!
   1 sig{ 0 } !
   0 sig{ 1 } !  \ pre-seed with arity N standard signature
   n 1 ?DO   I values{ sig{ stdsig-eval-lift   LOOP ;

   
: (stdsig-n-eval) { n values{ sig{ -- }
   \ same as above, but different loop structure and using +! operators
   sig{  n 2** CELLS ERASE
   1 sig{ 0 } !
   0 sig{ 1 } !    \ pre-seed with arity N standard signature
   n 1 ?DO   I values{ sig{ (stdsig-eval-lift)   LOOP  ;

0 VALUE [J]
: stdsig-n-eval' { n values{ sig{ -- }
   \ (same as (stdsig-n-eval) just rolled out / inlined for performance)
   1 sig{ 0 } !
   0 sig{ 1 } !
   n 6 > ABORT" n too large"
   [ 6 1 ] [?DO] 
      values{
      [ [I] DUP 1- * 2/ CELLS ]L  \ start offset for variables x_n used here
      + >R    ( r: values{ voffs CELLS +)
      [ [I] TO [J] ]
      [ 1 [I] LSHIFT 0 ] [?DO] 
         sig{  0   ( S: sig{ accumulator )
         [ [J] DUP 0 ]   ( params for [?DO] and saved value of [J]])
         [ [I] TO [J] ] 
         [?DO] [ ] \ iterate over all possible connections of new input "n+1"
            [ [J]  1 [I] LSHIFT AND ] [IF]
               ( sig{)OVER
               [ [J]  1 [I] LSHIFT INVERT AND CELLS ]L ( }) + @
               ( values{voffs CELLS+) R@ [ [I] CELLS ]L ( }) + @ *
               [ [J] [I] signadjust 0< ] [IF] - [ELSE] + [THEN]
            [THEN]
         [LOOP]
         [ TO [J] ]
         ( accumulator) ( sig{)SWAP [ [I]  1 [J] LSHIFT + ]L  ( }) CELLS + !
      [LOOP]
      rdrop
      [ [I] 2 + ]L n > IF EXIT THEN
   [LOOP] ;
\ see stdsig-n-eval

: .bin  ( u -- )    [char] % emit   base @ swap 2 base ! u. base ! ; 


  

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth -m 1G stdsig-test.fs -e bye"
   End:
[THEN]

