\ -*-forth-*-
\ Holographic standard signature fastest evaluation in the west -- unit test.
\
\ Copyright (C) 2021 David Kühling <dvdkhlng TA posteo TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: July 2021
3 assert-level !

require ./tester2.fs
T{ require  ./stdsig-few.fs -> }T

CR .( sig4: )
16 INTEGER MARRAY sig4r{
16 INTEGER MARRAY sig4f{
6 INTEGER MARRAY xval4{   
6 0 [DO]  1 [I] LSHIFT xval4{ [I] } ! [LOOP]

T{ 4 xval4{ sig4r{ stdsig-n-eval -> }T
T{ 4 xval4{ sig4f{ stdsig-few -> }T

T{ sig4f{  16 CELLS  sig4r{  16 CELLS COMPARE -> 0 }T

CR .( sig5: )
32 INTEGER MARRAY sig5r{
32 INTEGER MARRAY sig5rr{
32 INTEGER MARRAY sig5f{
10 INTEGER MARRAY xval5{   
10 0 [DO]  1 [I] LSHIFT xval5{ [I] } ! [LOOP]

T{ 5 xval5{ sig5r{ stdsig-n-eval -> }T
T{ 5 xval5{ sig5rr{ stdsig-n-eval -> }T
T{ 5 xval5{ sig5f{ stdsig-few -> }T

T{ sig5r{  32 CELLS  sig5rr{  32 CELLS COMPARE -> 0 }T
T{ sig5f{  32 CELLS  sig5r{  32 CELLS COMPARE -> 0 }T
\ CR 32 sig5r{ }iprint
\ CR 32 sig5f{ }iprint
\ CR

CR .( sig6: )
64 INTEGER MARRAY sig6r{
64 INTEGER MARRAY sig6f{
15 INTEGER MARRAY xval6{   
15 0 [DO]  [I] [I] * xval6{ [I] } ! [LOOP]

T{ 6 xval6{ sig6r{ stdsig-n-eval -> }T
T{ 6 xval6{ sig6f{ stdsig-few -> }T

T{ sig6f{  64 CELLS  sig6r{  64 CELLS COMPARE -> 0 }T

CR .( sig7: )
128 INTEGER MARRAY sig7r{
128 INTEGER MARRAY sig7f{
21 INTEGER MARRAY xval7{   
21 0 [DO]  1 [I] LSHIFT xval7{ [I] } ! [LOOP]

T{ 7 xval7{ sig7r{ stdsig-n-eval -> }T
T{ 7 xval7{ sig7f{ stdsig-few -> }T

T{ sig7f{  128 CELLS  sig7r{  128 CELLS COMPARE -> 0 }T

CR .( sig8: )
256 INTEGER MARRAY sig8r{
256 INTEGER MARRAY sig8f{
28 INTEGER MARRAY xval8{   
28 0 [DO]  1 [I] LSHIFT xval8{ [I] } ! [LOOP]

T{ 8 xval8{ sig8r{ stdsig-n-eval -> }T
T{ 8 xval8{ sig8f{ stdsig-few -> }T

T{ sig8f{  256 CELLS  sig8r{  256 CELLS COMPARE -> 0 }T
\ CR 256 sig8r{ }iprint
\ CR 256 sig8f{ }iprint
\ CR

CR .( sig9: )
512 INTEGER MARRAY sig9r{
512 INTEGER MARRAY sig9f{
36 INTEGER MARRAY xval9{   
36 0 [DO]  1 [I] LSHIFT [I] + xval9{ [I] } ! [LOOP]

T{ 9 xval9{ sig9r{ stdsig-n-eval -> }T
T{ 9 xval9{ sig9f{ stdsig-few -> }T

T{ sig9f{  512 CELLS  sig9r{  512 CELLS COMPARE -> 0 }T

CR .( sig17: )
131072 INTEGER MARRAY sig17r{
131072 INTEGER MARRAY sig17f{
136 INTEGER MARRAY xval17{   
136 0 [DO]  [I] [I] * 123 +  xval17{ [I] } ! [LOOP]

T{ 17 xval17{ sig17r{ stdsig-n-eval -> }T
T{ 17 xval17{ sig17f{ stdsig-few -> }T

T{ sig17f{  131072 CELLS  sig17r{  131072 CELLS COMPARE -> 0 }T



\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth-fast -m 1G stdsig-few-test.fs -e bye"
   End:
[THEN]