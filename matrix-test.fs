\ -*-forth-*-
\ Matrix routines test-case
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs

T{ require ./matrix.fs -> }T

T{ HERE  INTEGER , HERE 1 , 2 , }unallot  HERE - -> 0 }T

0 VALUE m{{
3 , INTEGER , HERE  TO m{{
0 , 1 , 2 ,
3 , 4 , 5 ,
6 , 7 , 8 ,
T{ 3 m{{ 0 2 }}swap-rows -> }T
T{ m{{ 0 0 }} @+ @+ @+ DROP -> 6 7 8 }T
T{ m{{ 2 0 }} @+ @+ @+ DROP -> 0 1 2 }T

3 , INTEGER , HERE  TO m{{
0 , 1 , 2 ,
3 , 4 , 5 ,
6 , 7 , 8 ,
T{ 3 m{{ 0 2 }}swap-cols -> }T
T{ m{{ 0 0 }} @+ @+ @+ DROP -> 2 1 0 }T
T{ m{{ 2 0 }} @+ @+ @+ DROP -> 8 7 6 }T

3 , DOUBLE , HERE  TO m{{
0. 2, -1. 2, 2. 2,
-3. 2, 4. 2, 5. 2,
6. 2, -7. 2, -8. 2,
T{ 3 m{{ 0 2 }}2swap-cols -> }T
T{ m{{ 0 0 }} 2@+ 2@+ 2@+ DROP -> 2. -1. 0. }T
T{ m{{ 2 0 }} 2@+ 2@+ 2@+ DROP -> -8. -7. 6. }T

4 , INTEGER , HERE  TO m{{
0 , 1 , 2 , 4 ,
3 , 4 , 5 , 7 ,
6 , 7 , 8 , 9 ,
3 , 2 , 6 , 1 ,
1 , 0 , 3 , 4 ,

0 VALUE s{{
2 , INTEGER , HERE TO s{{  3 2 * cells ALLOT
T{ m{{ 2 1 s{{ 3 }}extract -> }T
T{ s{{ 0 0 }} @+ @+ @+ @+ @+ @+ DROP ->
7 8
2 6
0 3 }T

5 5 INTEGER MMATRIX I{{
T{ 5 I{{ }}I! -> }T
T{ I{{ 0 0 }} @+ @+ @+ @+ @+ DROP -> 1 0 0 0 0 }T
T{ I{{ 4 0 }} @+ @+ @+ @+ @+ DROP -> 0 0 0 0 1 }T

exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth matrix-test.fs -e bye"
   End:
[THEN]
