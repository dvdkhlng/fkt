\ -*-forth-*-
\ Inverse of the FKT: create planar graph, given number of matchings
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

require ./drawing.fs
require ./fibonacci.fs

: xline  ( n1 n2 -- )
   2DUP MIN -ROT MAX line ;

: inverse-fkt ( ud -- #points #edges xy{ edges{ )
   \ for debugging, you can afterwards use 'snaphsot file.ps' get see what's
   \ going on.
   fibonacci-encode   { n fc{ }
   draw
   n 2* n point  { joint }
   n 1- 0 ?DO
      I I point  I  n 2* I - point  2DUP xline
      I IF
	 2DUP 2>R
	 ROT xline xline
	 2R>
	 fc{ I 1- } C@ IF
	    I n + odd? IF OVER ELSE DUP THEN
	    joint xline
	 THEN
      THEN
   LOOP
   DROP
   \ connect bottom side of fibonacci with an "odd" point
   n n point TUCK xline
   \ connect that "odd" point to joint
   joint xline
   end-draw   ( colors{ ) }unallot ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth inverse-fkt-test.fs -e bye"
   End:
[THEN]

