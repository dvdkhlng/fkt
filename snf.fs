\ -*-forth-*-
\ Compute Smith Normal Form of Matrix
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Sep 2011

\ for the algorithm, see
\
\  http://en.wikipedia.org/wiki/Smith_normal_form
\  http://planetmath.org/?method=l2h&from=objects&name=GausssAlgorithmForPrincipalIdealDomains&op=getobj

\ For complexity of the algorithm, and proof that length of numbers doesn't
\ grow exponentially, we might have to look into Ravi Kannan's PhD
\ thesis/publications.

require ./matrix.fs
require ./gcd.fs
require ./module.fs

module snf

<private>

0 VALUE M{{		\ mxn input/output matrix
0 VALUE m		\ dimensions of matrix : mxn
0 VALUE n
0 VALUE t		\ current row
0 VALUE col		\ pivot column index

: (pivot)  ( n1 n2 -- n3 )
   \ dead code!
   \ search next pivot column in row n1.  n2 is the first column to consider
   \ if for j>=n2 : M{{t,j}} == 0, return -1 (and caller should permute rows)
   n SWAP ?DO
      M{{ OVER I }} @ IF DROP I UNLOOP EXIT THEN
   LOOP
   DROP -1 ;

: find-pivot  ( n1 -- n2 )
   \ Search next pivot column in rows t and below.  n1 is the first column to
   \ consider.  perform row permutations as necessary.  If no pivot found,
   \ return -1, then we should be done.
   n SWAP ?DO	\ outer loop over columns (need to find smallest column)	
      m t ?DO	   \ and _inner_ loop over rows
	 M{{ I J }} @ IF
	    n M{{ t I }}swap-rows
	    J UNLOOP UNLOOP EXIT
	 THEN
      LOOP
   LOOP
   -1 ;

: row-addition  { row1 row2 factor -- }
   \ add row1, multiplied by factor to row2
   \ todo: use M* DROP?
   n 0 ?DO  \ todo: only need to consider columns >= pivot!?
      I DROP
      M{{ row1 I }} @ factor *  M{{ row2 I }} +!
   LOOP ;

: double-row-addition  { row1 row2 f11 f12 f21 f22  -- }
   \ perform a doubled row multiplication transformation:
   \ row1 becomes row1*f11 + row2*f12, row2 becomes row1*f21+row2+f22.
   \ This only makes sense in a gaussion elimination if 2x2 matrix
   \ [f11,f12;f21,f22] has determinant 1 and is invertible.
   \ note: products computed via M* DROP so we're sure that overflow
   \ is treated properly (bezout identity intermidiates will be larger
   \ than resulting gcd)
   n 0 ?DO  \ todo: only need to consider columns >= pivot!?
      M{{ row1 I }} @  DUP f21 M* DROP  SWAP f11 M* DROP
      M{{ row2 I }} @  DUP f22 M* DROP  SWAP f12 M* DROP
      ROT + M{{ row1 I }} !
      + M{{ row2 I }} !
   LOOP ;

: bezout-step  ( -- )
   \ improve pivot by applying applying row-multiplying transformations based
   \ on the bezout identity, enforcing that pivot divides numbers in the same
   \ column and all rows below it.
   m t 1+ ?DO
      M{{ t col }} @   { pivot }
      M{{ I col }} @   { a }
      a pivot MOD IF    \ pivot doesn't divide row I
	 t I
	 pivot a bezout-gcd   { gcd }
	 a gcd / NEGATE  pivot gcd /
	 double-row-addition
	 assert0( M{{ I col }} @  0= )
	 assert( M{{ t col }} @  gcd = )
      THEN
   LOOP ;
: eliminate-step  ( -- )
   \ zero elements in column col below element in row t by doing row-additions
   M{{ t col }} @   { pivot }
   m t 1+ ?DO
      t I
      M{{ I col }} @  pivot  assert( 2DUP MOD 0= )  / NEGATE
      row-addition
   LOOP ;

<public>

: }}left-snf  ( m n M{{ -- )
   \ compute an intermediate result towards the Smith Normal Form: eliminate
   \ entries in mxn matrix M{{}} on the lower-left side.
   TO M{{ TO n TO m  -1 TO col
   m 0 ?DO
      I TO t
      col 1+ find-pivot TO col
      col 0< IF  ( no more pivots) LEAVE THEN
      bezout-step
      eliminate-step
   LOOP ;

end-module

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth snf-test.fs -e bye"
   End:
[THEN]
