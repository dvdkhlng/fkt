\ -*-forth-*-
\ test misc.fs routines
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./tester2.fs
T{ require ./misc.fs -> }T

T{ 1 isqrt -> 0 1 }T
T{ 1. disqrt -> 0 1 }T
T{ 196 isqrt  -> 0 14 }T
T{ 1234567890123. 10 ud/mod -> 3 123456789012. }T
T{ 1234567890123. 1000000000 ud/mod -> 567890123 1234. }T

\ the following tests won't work on 32-bit forths
CELL 4 > [IF]
   T{ 12345678 dup * isqrt  -> 0 12345678 }T
   T{ 1234567890123456 dup M* disqrt  -> 0 1234567890123456 }T
[THEN]

T{ $1137 count1bits -> 1 1 + 2 + 3 + }T
T{ 0 count1bits -> 0 }T
T{ -1 count1bits -> #bits/cell }T

#bits/cell 0 [DO]
   T{ 1 [I] LSHIFT #msb ->  [I] }T
   T{ -1 #bits/cell 1- [I] - RSHIFT #msb ->  [I] }T
[LOOP]
T{ 0 #msb -> 0 }T

T{ 1. 1 dlshift -> 2. }T
T{ 1. 32 dlshift -> $100000000. }T
T{ $100010001000. 32 dlshift -> $1000.1000100000000000 }T
T{ $7777.1000100010001000. 16 dlshift -> $77771000.1000100010000000 }T

T{ 2. 1 drshift -> 1. }T
T{ $100000000. #32 drshift -> 1. }T
T{ $1000.1000100000000000 32 drshift -> $100010001000. }T
T{ $77771000.1000100010000000 16 drshift -> $7777.1000100010001000. }T

T{ $1000.1000100000000000 32 mrshift -> $100010001000 }T
T{ $100010001000 32 mlshift -> $1000.1000100000000000 }T

T{ 0 parity -> 0 }T
T{ $8000000000000000 parity -> 1 }T
T{ $8000000000000001 parity -> 0 }T
T{ $8000050000000001 parity -> 0 }T
T{ $8000050000007001 parity -> 1 }T
T{ 16 0 [DO] [I] parity [LOOP] -> 0 1 1 0  1 0 0 1  1 0 0 1  0 1 1 0  }T

T{ 123 0 n** -> 1 }T
T{ 123 1 n** -> 123 }T
T{ 123 2 n** -> 123 123 * }T
T{ 123 3 n** -> 123 123 * 123 * }T
exit-test

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth misc-test.fs -e bye"
   End:
[THEN]

