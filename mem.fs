\ -*-forth-*-
\ Miscellanous raw memory handling routines
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

1024 CONSTANT /exchange-chunk		\ chunksize used by 'exchange-mem'

: (exchange-mem)  ( addr1 addr2 u -- )
   \ exchange contents of two memory regions.  Uses memory at HERE to hold a
   \ full copy of the 'u' bytes data.
   >R
   DUP HERE R@ MOVE	\ addr2 to HERE
   OVER SWAP R@ MOVE	\ addr1 to addr2
   HERE SWAP R> MOVE ;	\ HERE to addr1
: exchange-mem  ( addr1 addr2 u -- )
   \ like (exchange-mem), just copies in chunks to not use too much temporary
   \ memory.
   1 SWAP ?DO
      2DUP /exchange-chunk I MIN (exchange-mem)
      /exchange-chunk + SWAP /exchange-chunk + SWAP
   /exchange-chunk NEGATE +LOOP
   2DROP ;

\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth mem-test.fs -e bye"
   End:
[THEN]
