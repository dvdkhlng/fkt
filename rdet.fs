\ -*-forth-*-
\ Matrix Determinant
\
\ Copyright (C) 2011 David Kühling <dvdkhlng TA gmx TOD de>
\
\ License: GPLv3 or later, NO WARRANTY
\
\ Created: Aug 2011

require ./imatrix.fs
require ./mod.fs

require ./module.fs

: }}rdet  { n M{{ -- result }
   \ note: destroys the contents of M{{ during computation
   1 { result }
   n 0 ?DO

      \ search pivot (i.e. swap rows to ensure m(i,i)>0)
      n I ?DO
	 M{{ I J }} @ IF
	    I J <> IF
	       n M{{ I J }}swap-rows
	       result rnegate TO result
	    THEN
	    LEAVE
	 THEN
      LOOP

      \ read pivot, adjust result, precompute inverse
      M{{ I I }} @   dup result r* TO result	( s: pivot)
      dup 0= IF			\ zero pivot 
	 UNLOOP EXIT		\ -> abort with determinant zero
      THEN
      rinv { p^-1 }

      \ zero column I in rows below.  todo: optimize loop/factor
      n I 1+ ?DO
	 M{{ I J }} @ ?dup IF
	    p^-1 r*
	    n J ?DO
	       DUP M{{ K  I }} @ r*  M{{ J I }} r-!
	    LOOP
	    drop	    
	 THEN
      LOOP
   LOOP
   result ;


\ Customize Emacs
0 [IF]
   Local Variables:
   compile-command: "gforth rdet-test.fs -e bye"
   End:
[THEN]
